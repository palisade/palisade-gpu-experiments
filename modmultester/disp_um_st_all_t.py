#!/usr/bin/env python3
#Script to plot outputs of doruns_um_st (modmul) for all towers. 
# call like this:
#   ./displsy_um_st.py  outdirpath foo
# to plot outdirpath/foo* and outdirpath/fastfoo*
# note for example to plot all towers in out-um-st-NTL-64-8192-*.dat
# enter out-um-st-NTL-64-8192-1.dat
# and this program will open all the towers in that group 

import numpy as np
import sys
import filereaders as fr
import matplotlib.pyplot as plt
import plot_time as plttime

if len(sys.argv) < 3:
    print('Usage: displsy_um_st,py [path] [filenameroot]')
    sys.exit()

file_path = sys.argv[1]
time_data_file = sys.argv[2]

fastest_ave_time_usec =  1000000;

towers = [1,2,4,8, 16, 32]
for tower in towers:

    ## parse out the tower part of time_data_file number just before ".dat"

    splitstring = time_data_file.split('.')
    #print(splitstring)
    this_file = splitstring[0].rstrip('0123456789')+ str(tower) + '.'+ splitstring[1]
    filename_segments = splitstring[0].split('-')
    #print(filename_segments)
    alg_name = filename_segments[3]
    width_name = filename_segments[4]
    n_name = filename_segments[5]
    
    fastest_data_file = "fast"+this_file

    full_this_file = file_path +"/"+ this_file
    full_fastest_data_file = file_path + "/" + fastest_data_file

    #load in the runtimes from the file
    colnames, n, n_towers, \
        ave_time_usec, sd_time_usec, th, bal, nvalid\
        = fr.readUmStModMulTimeFiles(full_this_file)

    #todo make a div|ntl column
    #and a 32|64bit column
    nin = len(n_towers)
    dummy=[0]*nin  #until them use dummy
    
    title_str = alg_name+'-'+width_name+'-'+n_name
    
    #load in the fastest run time
    fcolnames, fn, fn_towers, \
        fave_time_usec, fsd_time_usec, fth, fbal, fnvalid\
        = fr.readUmStModMulTimeFiles(full_fastest_data_file)

    #save the fastest
    #print(ave_time_usec)
    #print(th)
    #print(fave_time_usec)
    #print(fth)
    # pull out the fastest in this tower
    min_ix = ave_time_usec.index(min(ave_time_usec))
    this_fastest = ave_time_usec[min_ix]

    if (this_fastest < fastest_ave_time_usec):
        fastest_ave_time_usec = this_fastest
        fastest_sd_time_usec = sd_time_usec[min_ix]
        fastest_threads = th[min_ix]
        fastest_tower = tower

    
    print("tower ",tower, "fastest threads ", fastest_threads, " time ", fastest_ave_time_usec)
    
    #   plttime.vsThreadBalance(th, bal, n, inverse_flag, ave_time_usec, fth, fbal, fave_time_usec, fsd_time_usec)
    if (tower ==1):
        [fig, ax]=plttime.all_threads(th, bal, n, ave_time_usec, sd_time_usec, title_str=title_str, plotID=tower)
    else:
        plttime.all_threads(th, bal, n, ave_time_usec, sd_time_usec, fig, ax, title_str=title_str, plotID=tower)

    ax.set_title('Fastest Run Time tower: '+str(fastest_tower) +' th:'+str(fastest_threads)+
                 " is "+str(fastest_ave_time_usec) +'\u00b1'+ '{:.0f}'.format(fastest_sd_time_usec) +' uS' )

        
figname = 'fig-'+filename_segments[3]+'-'+filename_segments[4]+'-'+filename_segments[5]+'.png'
#ax.set_ylim(bottom= 40,top= 600 )
ax.set_ylim(bottom= 40,top= 80 )
ax.legend()

fig.savefig(figname)
#plt.show()
