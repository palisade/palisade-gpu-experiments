// mod mul tester using  unified memory parallel streams for towers.  
// D.Cousins NJIT 2020
// combines features of the cuda-um-st.cpp benchmark code for NTT and
// CGBN based benchmarks of modulo multiply.
// code to generate or read in various sized towers of data and benchmark
// Hadamard vector products on attached GPU.
// stream version using unified memory.
// loops over # threads to use, and number of towers to send to a block (balance)



//#define NTL32B //this did not work. 
#define NTL32A 
#if __has_include ("cuda.h") // if not on cuda system 
                             // does not trigger compile errors when made on
                             // non gpu system accidentally

#include <bits/stdc++.h> 
#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>
#include <random>
#include <vector>
#include <cmath>
#include <cstring>
#include <debug.h>
#include <getopt.h>
#include <gpu_support.h>
#include <gpu_properties.h>
#include <exception>
#include <modmulmem.h>
#include <mod_math.h>
#include <ntl_stuff.h>

using namespace std;

// Test Enums //////////////////////////////////////////////
//define to_underlying() for allowing enum class to be used as index
template <typename E>
constexpr typename std::underlying_type<E>::type to_underlying(E e) noexcept {
    return static_cast<typename std::underlying_type<E>::type>(e);
}

//define the various tests in an enum class
enum class TestsEnum: unsigned int {
  NATIVE_BAR32,
  NATIVE_BAR64,
  NATIVE_DIV32,
  NATIVE_DIV64,
  NATIVE_NTL32,
  NATIVE_NTL64,
  NUM_TESTS
};

//define the names of the tests
std::string TestName[to_underlying(TestsEnum::NUM_TESTS)] = {
  "NATIVE_BAR32",
  "NATIVE_BAR64",
  "NATIVE_DIV32",
  "NATIVE_DIV64",
  "NATIVE_NTL32",
  "NATIVE_NTL64",
};




//struct to save fastest time during trials. 
typedef struct {
  uint64_t n;
  uint64_t towers;
  uint64_t th;
  uint64_t bal;
  double ave_time;
  double sd_time;  
  uint64_t nvalid;
} fastest_struct;

typedef struct {
  size_t threads;
  size_t balance;
} GPUwisdom;

#if 0 //this hasn't been used yet
class GPUCRT{
  GPUCRT(){};
  
  void setup(){
  };
  void fwd(){
  };
  void inv(){
  };
  void teardown(){
  };

private:
  
  uint64_t *input; //cpu
  size_t n;
  uint64_t modulus;
  GPUwisdom wisdom;
};
#endif
//////////////////////////////////////////////////////////////////////
// inplace vector vector hadamard modmultiplication,
// multiply inout by vec put result in inout
// n length of vectors
// nthr total # threads
// mod uint64_t modulus

__global__ void inplace_vmul_div_64(uint64_t* __restrict__ inout,
							 const uint64_t* __restrict__ vec,
							 const int n,
							 const size_t nthr,
							 const uint64_t mod) {
  int id = blockIdx.x * nthr + threadIdx.x;
  if (id < n){
    inout[id] = modmul_div_64_kernel(inout[id], vec[id], mod);
  }
}
//////////////////////////////////////////////////////////////////////
// outofplace vector vector hadamard modmultiplication,
// used to multiply a by b with result in result
// n length of vectors
// nthr total # threads
// mod uint64_t modulus

__global__ void outofplace_vmul_div_64(uint64_t* __restrict__ result,
								const uint64_t* __restrict__ a,
								const uint64_t* __restrict__ b,
								const int n,
								const size_t nthr,
								const uint64_t mod) {
  int id = blockIdx.x * nthr + threadIdx.x;
  if (id < n){
    result[id] = modmul_div_64_kernel(a[id], b[id], mod);
  }
}
//////////////////////////////////////////////////////////////////////
// inplace vector vector hadamard modmultiplication,
// multiply inout by vec put result in inout
// n length of vectors
// nthr total # threads
// mod uint32_t modulus

__global__ void inplace_vmul_div_32(uint32_t* __restrict__ inout,
							 const uint32_t* __restrict__ vec,
							 const int n,
							 const size_t nthr,
							 const uint32_t mod) {
  int id = blockIdx.x * nthr + threadIdx.x;
  if (id < n){
    inout[id] = modmul_div_32_kernel(inout[id], vec[id], mod);
  }
}
//////////////////////////////////////////////////////////////////////
// outofplace vector vector hadamard modmultiplication,
// used to multiply a by b with result in result
// n length of vectors
// nthr total # threads
// mod uint32_t modulus

__global__ void outofplace_vmul_div_32(uint32_t* __restrict__ result,
								const uint32_t* __restrict__ a,
								const uint32_t* __restrict__ b,
								const int n,
								const size_t nthr,
								const uint32_t mod) {
  int id = blockIdx.x * nthr + threadIdx.x;
  if (id < n){
    result[id] = modmul_div_32_kernel(a[id], b[id], mod);
  }
}



//////////////////////////////////////////////////////////////////////
// outofplace vector vector hadamard modmultiplication,
// used to multiply a by b with result in result
// n length of vectors
// nthr total # threads
// mod uint64_t modulus

__global__ void outofplace_vmul_NTL_64(uint64_t* __restrict__ result,
									   const uint64_t* __restrict__ a,
									   const uint64_t* __restrict__ b,
									   const uint64_t* __restrict__ yminv,
									   const int n,
									   const size_t nthr,
									   const uint64_t mod) {
  int id = blockIdx.x * nthr + threadIdx.x;
  if (id < n){
    result[id] = modmul_NTL_64_kernel(a[id], b[id], yminv[id], mod);
  }
}


//////////////////////////////////////////////////////////////////////
// outofplace vector vector hadamard modmultiplication,
// used to multiply a by b with result in result
// n length of vectors
// nthr total # threads
// mod uint32_t modulus

__global__ void outofplace_vmul_NTL_32A(uint32_t* __restrict__ result,
									   const uint32_t* __restrict__ a,
									   const uint32_t* __restrict__ b,
									   const int n,
									   const size_t nthr,
									   const uint32_t mod,
									   const double invmod) {
  int id = blockIdx.x * nthr + threadIdx.x;
  if (id < n){
    result[id] = modmul_NTL_32A_kernel(a[id], b[id], mod, invmod);
  }
}

//////////////////////////////////////////////////////////////////////
// outofplace vector vector hadamard modmultiplication,
// used to multiply a by b with result in result
// n length of vectors
// nthr total # threads
// mod uint32_t modulus

__global__ void outofplace_vmul_NTL_32B(uint32_t* __restrict__ result,
									   const uint32_t* __restrict__ a,
									   const uint32_t* __restrict__ b,
									   const uint32_t* __restrict__ yminv,
									   const int n,
									   const size_t nthr,
									   const uint32_t mod) {
  int id = blockIdx.x * nthr + threadIdx.x;
  if (id < n){
    result[id] = modmul_NTL_32B_kernel(a[id], b[id], yminv[id], mod);
  }
}

//////////////////////////////////////////////////////////////////////
// outofplace vector vector hadamard modmultiplication,
// used to multiply a by b with result in result
// n length of vectors
// nthr total # threads
// mod uint64_t modulus

__global__ void outofplace_vmul_bar_64(uint64_t* __restrict__ result,
									   const uint64_t* __restrict__ a,
									   const uint64_t* __restrict__ b,
									   const int n,
									   const size_t nthr,
									   const uint64_t mod,
									   const uint64_t mu,
									   const uint64_t msb) {
  int id = blockIdx.x * nthr + threadIdx.x;
  if (id < n){
    result[id] = modmul_bar_64_kernel(a[id], b[id], mod, mu,  msb);
  }
}


//////////////////////////////////////////////////////////////////////
// outofplace vector vector hadamard modmultiplication,
// used to multiply a by b with result in result
// n length of vectors
// nthr total # threads
// mod uint64_t modulus

__global__ void outofplace_vmul_bar_32(uint32_t* __restrict__ result,
									   const uint32_t* __restrict__ a,
									   const uint32_t* __restrict__ b,
									   const int n,
									   const size_t nthr,
									   const uint32_t mod,
									   const uint32_t mu,
									   const uint64_t msb) {
  int id = blockIdx.x * nthr + threadIdx.x;
  if (id < n){
    result[id] = modmul_bar_32_kernel(a[id], b[id], mod, mu, msb);
  }
}


////////////////////////////////////////////////////////////
// ModMulSetup Initializes the ModMul code
// allocates cuda and cpu memory 
// n length of vectors
// modulus:  modulus for all operations

void CRTsetup(vector<MODMULMEM> gvec) {
  for (uint twr = 0; twr< gvec.size(); twr++){
	MODMULMEM *gp = &gvec[twr];
	// does nothing. 
  }
  
}

// ModMul performs the vector Modulo Multiplication
// fwd_buffer: input vector of uint64_t
// result output vector of uint64_t
// twiddle vector of uint64_t twiddles forward
// n length of vectors
// modulus: uint64_t modulus for all operations
// w.threads: power of two # threads to use
// w.balance: power of two: stage at which we shift from single inplace_modmul_outer
//          kernel calls to loop over inplace_modmul_inner kernel calls 

void ModMul(vector<MODMULMEM>gvec, GPUwisdom &w, TestsEnum testId) {
  
  DEBUG_FLAG(false);
  bool kernel_error(false);
  DEBUGEXP(gvec.size());
  for (auto tix = 0; tix < gvec.size(); tix++){ // loop over towers
	DEBUGEXP(tix);
	MODMULMEM *gp = &gvec[tix];

	if (dbg_flag) print_modmulmem(gp);
	DEBUGEXP(w.threads);
	DEBUGEXP(w.balance);
	// prefetch data to GPU
	if (gp->a_buffer != NULL) {
	  CUDA_CHECK(cudaStreamAttachMemAsync(gp->SID, gp->a_buffer,
										  0, cudaMemAttachGlobal));
	  CUDA_CHECK(cudaStreamAttachMemAsync(gp->SID, gp->b_buffer,
										  0, cudaMemAttachGlobal));
	  if (testId == TestsEnum::NATIVE_NTL64) {
		CUDA_CHECK(cudaStreamAttachMemAsync(gp->SID, gp->yminv_buffer,
											0, cudaMemAttachGlobal));
	  }
	} else {
	  CUDA_CHECK(cudaStreamAttachMemAsync(gp->SID, gp->a_buffer32,
										  0, cudaMemAttachGlobal));
	  CUDA_CHECK(cudaStreamAttachMemAsync(gp->SID, gp->b_buffer32,
										  0, cudaMemAttachGlobal));
#ifdef NTL32B
	  if (testId == TestsEnum::NATIVE_NTL32) {
		CUDA_CHECK(cudaStreamAttachMemAsync(gp->SID, gp->yminv_buffer32,
											0, cudaMemAttachGlobal));
	  }
#endif
	}
  }

  for (auto tix = 0; tix < gvec.size(); tix++){ //loop over towers
	DEBUGEXP(tix);
	MODMULMEM *gp = &gvec[tix];

	if (dbg_flag) {
	  cout <<"input a tower "<<tix<<":"<<endl;
	  for (auto ix = 0; ix< gp->n; ix++){
		cout << gp->a_buffer[ix]<<endl;		
	  }
	  cout <<"input b tower "<<tix<<":"<<endl;
	  for (auto ix = 0; ix< gp->n; ix++){
		cout << gp->b_buffer[ix]<<endl;		
	  }
	}
	// compute product 
	DEBUG("  multiply start "<<TestName[to_underlying(testId)]);
	size_t nblock = ceil(gp->n / w.threads);
	switch (testId) {

	case TestsEnum::NATIVE_BAR32:
	  outofplace_vmul_bar_32<<<nblock, w.threads, 0, gp->SID>>>(gp->result32, gp->a_buffer32, gp->b_buffer32, gp->n, w.threads, gp->modulus32, gp->mu32, gp->msb);
	  break;
	  
	case TestsEnum::NATIVE_BAR64:
	  outofplace_vmul_bar_64<<<nblock, w.threads, 0, gp->SID>>>(gp->result, gp->a_buffer, gp->b_buffer, gp->n, w.threads, gp->modulus, gp->mu, gp->msb);
	  break;

	case TestsEnum::NATIVE_DIV32:
	  outofplace_vmul_div_32<<<nblock, w.threads, 0, gp->SID>>>(gp->result32, gp->a_buffer32, gp->b_buffer32, gp->n, w.threads, gp->modulus32);
	  break;
	  
	case TestsEnum::NATIVE_DIV64:
	  outofplace_vmul_div_64<<<nblock, w.threads, 0, gp->SID>>>(gp->result, gp->a_buffer, gp->b_buffer, gp->n, w.threads, gp->modulus);
	  break;

	case TestsEnum::NATIVE_NTL32:
#ifdef NTL32A
	  outofplace_vmul_NTL_32A<<<nblock, w.threads, 0, gp->SID>>>(gp->result32, gp->a_buffer32, gp->b_buffer32,  gp->n, w.threads, gp->modulus32, gp->invmod32);	
#endif
#ifdef NTL32B
	  outofplace_vmul_NTL_32B<<<nblock, w.threads, 0, gp->SID>>>(gp->result32, gp->a_buffer32, gp->b_buffer32, gp->yminv_buffer32, gp->n, w.threads, gp->modulus32);
#endif

	  break;

	  case TestsEnum::NATIVE_NTL64:
	  outofplace_vmul_NTL_64<<<nblock, w.threads, 0, gp->SID>>>(gp->result, gp->a_buffer, gp->b_buffer, gp->yminv_buffer, gp->n, w.threads, gp->modulus);
	  break;
	}
	//CUDA_ERR_CHECK(dbg_flag);
	  
	//kernel_error |= CUDA_ERR_CHECK(dbg_flag);
  } //for tix
  DEBUG("done with launches");
  
  if (kernel_error) {
	cout <<" ERROR "<<endl;
	throw "BAD KERNEL";
  } else {
	for (auto tix = 0; tix < gvec.size(); tix++){
	  DEBUG("attaching to tower "<<tix<<" stream "<< gvec[tix].SID );
	  //prefetch r to cpu and sync
	  if (gvec[tix].a_buffer != NULL) {
		CUDA_CHECK(cudaStreamAttachMemAsync(gvec[tix].SID, gvec[tix].result, 0, cudaMemAttachHost));
	  }else {
		CUDA_CHECK(cudaStreamAttachMemAsync(gvec[tix].SID, gvec[tix].result32, 0, cudaMemAttachHost));
	  }
	}	
  }

  for (auto tix = 0; tix < gvec.size(); tix++){
	DEBUG("synching to tower "<<tix<<" stream "<< gvec[tix].SID );
	// Synchronize
	CUDA_CHECK(cudaStreamSynchronize(gvec[tix].SID));
  }
  DEBUG("returning from CRTfwd()");

}

// ModMul teardown
// deallocates cuda and cpu memory 

void ModMulteardown(vector<MODMULMEM> gvec) {
  for (auto tix = 0; tix < gvec.size(); tix++){
	MODMULMEM* gp = &gvec[tix];
	//free the allocated memory
	if (gp->a_buffer != NULL) {
	  CUDA_CHECK(cudaFree(gp->result));
	  CUDA_CHECK(cudaFree(gp->a_buffer));
	  CUDA_CHECK(cudaFree(gp->b_buffer));
	  CUDA_CHECK(cudaFree(gp->yminv_buffer));
	  delete [] gp->correct;
	} else {
	  CUDA_CHECK(cudaFree(gp->result32));
	  CUDA_CHECK(cudaFree(gp->a_buffer32));
	  CUDA_CHECK(cudaFree(gp->b_buffer32));
#ifdef NTL32B
	  CUDA_CHECK(cudaFree(gp->yminv_buffer32));
#endif
	  delete [] gp->correct32;
	}
  }
}

//function to compute and time the ModMul
// gvec vector of argument and storage structures, one per tower
// w wisdom
// returns execution time in microseconds. 

uint64_t compute_ModMul(vector<MODMULMEM> &gvec, GPUwisdom &w, TestsEnum testId) {
  // Start the stopwatch
  auto start = chrono::high_resolution_clock::now();
 
  // Run CRT algorithm with loaded data
  ModMul(gvec, w, testId);
  
  // Log the elapsed time
  auto finish = chrono::high_resolution_clock::now();
  auto microseconds = chrono::duration_cast<std::chrono::microseconds>(finish-start);
  
  //return the average time for all towers
  return (microseconds.count()/gvec.size());
}

// function to clear the input buffers (to see if there are cache effects). 
void clear_input_buffers(bool cache_overwrt, vector<MODMULMEM> gvec){			  
  if (cache_overwrt){
	//overwrite the input buffer to flush the cache
	for (size_t tix = 0; tix < gvec.size(); tix++){
	  MODMULMEM *gp = &gvec[tix];
	  auto n = gp->n;
	  if (gp->a_buffer != NULL) { //Use 64 bit storage
		
		vector<uint64_t> temp_a;
		vector<uint64_t> temp_b;
		for (size_t i = 0; i < n; i++){ //copy buffer
		  temp_a.push_back(gp->a_buffer[i]);
		  temp_b.push_back(gp->b_buffer[i]);
		}
		for (size_t i = 0; i < n; i++){ //clear buffer
		  gp->a_buffer[i] = 0;
		  gp->b_buffer[i] = 0;
		  asm("");  //should keep compiler from optimizing out this code

		}
		for (size_t i = 0; i < n; i++){ //copy buffer
		  gp->a_buffer[i]=temp_a[i];
		  gp->b_buffer[i]=temp_b[i];
		}
	  } else { //use 32 bit storage
		vector<uint32_t> temp_a;
		vector<uint32_t> temp_b;
		for (size_t i = 0; i < n; i++){ //copy buffer
		  temp_a.push_back(gp->a_buffer32[i]);
		  temp_b.push_back(gp->b_buffer32[i]);
		}
		for (size_t i = 0; i < n; i++){ //copy buffer
		  gp->a_buffer32[i] = 0;
		  gp->b_buffer32[i] = 0;
		  asm("");  //should keep compiler from optimizing out this code
		}
		for (size_t i = 0; i < n; i++){ //copy buffer
		  gp->a_buffer32[i]=temp_a[i];
		  gp->b_buffer32[i]=temp_b[i];
		}
	  }
	}
  }
}
// compares all towers result vs correct, returns true if fails.
bool compare_all_tower_results(std::vector<MODMULMEM> &gvec, bool verbose) {

  bool fail(false);

  for (auto tix = 0; tix < gvec.size(); tix++){
	auto gp = &gvec[tix];
	auto n = gp->n;
	if (verbose) {
	  cout <<endl;
	}
	if (gp->a_buffer != NULL) { //Use 64 bit storage
	  for (int i = 0; i < n; i++) {
		if (gp->result[i]!=gp->correct[i]) {
		  fail |=true;
		  if (verbose) {
			cout<<"error result tower "<<tix<<" ["<<i<<"]: "<<gp->result[i]<<" != "<<gp->correct[i]<<endl;
		  }
		}
	  }
	  if (verbose) {
		if (fail) {
		  cout << "Verify failure in tower ix "<< tix <<endl;
		}else{
		  cout << "Verify success in tower ix "<< tix <<endl;
		}
	  }
	} else { //use 32 bit storage

	  for (int i = 0; i < n; i++) {
		if (gp->result32[i]!=gp->correct32[i]) {
		  fail |=true;
		  if (verbose) {
			cout<<"error result tower "<<tix<<" ["<<i<<"]: "<<gp->result32[i]<<" != "<<gp->correct32[i]<<endl;
		  }
		}
	  }
	  if (verbose) {
		if (fail) {
		  cout << "Verify failure in tower ix "<< tix <<endl;
		}else{
		  cout << "Verify success in tower ix "<< tix <<endl;
		}
	  }
	}

  }
  return (fail);
}
  


//main program parses input arguments
  
int main(int argc, char** argv) {
  DEBUG_FLAG(false);

  // manage the command line args
  int opt; //option from command line parsing

  //default values of inputs
  unsigned int n_repeat(1);
  unsigned int n_towers(1); 
  unsigned int n_selected(8);
  unsigned int n_balance_select(0);
  unsigned int n_threads_select(0);
  unsigned int select_word_size(64); //word size selected by user
  unsigned int word_size(60); //actual word size (smaller)

  string alg_select("div");
  string folder_select="";
  string out_fname="";
  bool verbose = false;
  bool cache_overwrt = false;

  string usage_string =
    string("run ")+ string(argv[0])+ string(" demo with settings (default value show in parenthesis):\n")+
    string("-a algorithm (div|ntl|bar) (div)\n")+	
    string("-b balance factor (loop over balance) power of two\n")+
    string("-c number of executions to average over (1)\n")+
    string("-n ring length (8-1024) powers of two\n")+
	string("-o [output filename]\n")+
    string("-p number of threads (loop over threads) power of two <= 1024 (512 on TX2)\n")+
    string("-t number of tower elements to do (1)\n")+
    string("-v verbose flag (false)\n")+
    string("-w word size 32|64 (actually 30|60 (64)\n")+	
    string("-x force cache overwrite on input  (false)\n")+
    string("\nh prints this message\n");
  
  while ((opt = getopt(argc, argv, "a:b:c:t:n:o:p:vw:xh")) != -1) {
    switch (opt)
      {
      case 'a':
        alg_select = optarg;
		break;
      case 'b':
        n_balance_select = atoi(optarg);
		break;
      case 'c':
        n_repeat = atoi(optarg);
		break;
      case 't':
        n_towers = atoi(optarg);
		break;
      case 'n':
        n_selected = atoi(optarg);
		break;
      case 'o':
        out_fname = optarg;
		break;
      case 'p':
        n_threads_select = atoi(optarg);
		break;
      case 'x':
        cache_overwrt = true;
		break;
      case 'v':
        verbose = true;
		break;
      case 'w':
        select_word_size = atoi(optarg);
		break;
		
      case 'h':
      default: /* '?' */
		cout<<usage_string<<endl;
		exit(0);
      }
  }
  
  if (verbose) {
    cout << "===========BENCHMARKING FOR ModMul CUDA ===============: " << endl;
  }
  
  // Initialize CUDA
  CUDA_CHECK(cudaSetDeviceFlags(cudaDeviceScheduleSpin));
  CUDA_CHECK(cudaFree(0)); //initializes the cuda context 

  vector<string> gpu_names;
  gpu_names = GPU_Properties(verbose);
  unsigned int maxthreadsperblock = 1024;
  if (gpu_names[0] == "NVIDIA Tegra X2"){
    maxthreadsperblock = 512;  //for some reason 1024 fails
  }
  cout<<"---------------------------------------------------"<<endl;
  cout << "Detected "<<gpu_names[0]<<", using max of "<<maxthreadsperblock<<" threads per block"<<endl;

  cout<<"---------------------------------------------------"<<endl;
  // Print out a header

  vector<MODMULMEM> gvec; //storage for gpu data
  gvec.resize(n_towers); //storage for n towers. 
  size_t size_of_word = 0;

  uint32_t mask30 = (1UL<<30) -1;  //mask for word_size bits.
  uint64_t mask60 = (1UL<<60) -1;  //mask for word_size bits.
  
  if (select_word_size == 64) {
	word_size = 60;
	size_of_word = sizeof(uint64_t);
  } else if (select_word_size == 32) {
	word_size = 30;	
	size_of_word = sizeof(uint32_t);
  } else {
	cout <<" error, word size must be 32 or 64"<<endl;
	exit(-1);
  }

  DEBUGEXP(word_size);
  DEBUGEXP(size_of_word);
  DEBUGEXP(mask30);
  DEBUGEXP(mask60);
  
  //std::random_device r; // a random seed
  //std::default_random_engine generator(r()); //random number generator seeded with random number
  std::default_random_engine generator; //random number generator seeded with random number
  std::uniform_int_distribution<uint32_t> dist30{0, mask30}; //distribution for 30 bit modulus
  std::uniform_int_distribution<uint64_t> dist60{0, mask60}; //same for 60 bit modulus

  size_t data_size = n_selected * size_of_word;


  // allocate appropriate sized memory, and
  // generate word-size bit random moduli 
  for (auto tower_ix=0; tower_ix< n_towers; tower_ix++){
	MODMULMEM *gp = &gvec[tower_ix];
	gp->n = n_selected;
	//create and attach the stream for this tower
	CUDA_CHECK(cudaStreamCreate(&gp->SID));
	
	if (word_size == 60) {
	  //use 64 bit storage
	  CUDA_CHECK(cudaMallocManaged(&gp->result, data_size)); //gpu
	  CUDA_CHECK(cudaMallocManaged(&gp->a_buffer, data_size, cudaMemAttachHost)); //cpu
	  CUDA_CHECK(cudaMallocManaged(&gp->b_buffer, data_size, cudaMemAttachHost)); //cpu
	  CUDA_CHECK(cudaMallocManaged(&gp->yminv_buffer, data_size, cudaMemAttachHost)); //cpu
	  gp->correct = new uint64_t[gp->n];
	  gp->modulus = dist60(generator);
	  gp->msb =  64 - (sizeof(unsigned long) == 8 ? __builtin_clzl(gp->modulus) : __builtin_clzll(gp->modulus));
	   //gp->msb =  60; //get_MSB(gp->modulus);
	  uint128_t temp(1);
	  temp <<= 2 * gp->msb + 3;
	  gp->mu = uint64_t(temp / gp->modulus);
	}else{
	  //use 32 bit storage
	  CUDA_CHECK(cudaMallocManaged(&gp->result32, data_size)); //gpu
	  CUDA_CHECK(cudaMallocManaged(&gp->a_buffer32, data_size, cudaMemAttachHost)); //cpu
	  CUDA_CHECK(cudaMallocManaged(&gp->b_buffer32, data_size, cudaMemAttachHost)); //cpu
#ifdef NTL32B
	  CUDA_CHECK(cudaMallocManaged(&gp->yminv_buffer32, data_size, cudaMemAttachHost)); //cpu
#endif
	  gp->correct32 = new uint32_t[gp->n];
	  gp->modulus32 = dist30(generator);
	  gp->invmod32 = double (1.0)/ double(gp->modulus32);
	  gp->msb = 30; // get_MSB(gp->modulus);
	  uint64_t temp(1);
	  temp <<= 2 * gp->msb + 3;
	  gp->mu32 = uint32_t(temp / gp->modulus);
	}
  }

  //generate entries for the input data and correct data
  for (auto tower_ix=0; tower_ix< n_towers; tower_ix++){
	MODMULMEM *gp = &gvec[tower_ix];
	if (word_size == 60) { // 64 bit arithmetic
	  for (auto ix = 0; ix< gp->n; ix++){

		uint64_t tmp_a;
		uint64_t tmp_b;
		std::uniform_int_distribution<uint64_t> dist_m{0, gp->modulus-1};  
		tmp_a = dist_m(generator);
		tmp_b = dist_m(generator);
		gp->a_buffer[ix] = tmp_a;
		gp->b_buffer[ix] = tmp_b;
		if (tmp_a> gp->modulus) {
		  cout <<"error in a generation"<<endl;
		  exit (-1);
		}
		  
		if (tmp_b> gp->modulus) {
		  cout <<"error in b generation"<<endl;
		  exit (-1);
		}
		  
		gp->yminv_buffer[ix] = PrepMulModPrecon((long)tmp_b, (long) gp->modulus);
	  
		unsigned __int128 x, y, m;
	  
		m = gp->modulus;
		x = gp->a_buffer[ix];
		y = gp->b_buffer[ix];
		gp->correct[ix] = (x*y)%m;      
	  }
	}else{ //32 bit arithmetic
	  for (auto ix = 0; ix< gp->n; ix++){
		std::uniform_int_distribution<uint32_t> dist_m{0, gp->modulus32-1};  
		gp->a_buffer32[ix] = dist_m(generator);
		gp->b_buffer32[ix] = dist_m(generator);
		if (gp->a_buffer32[ix]> gp->modulus32) {
		  cout <<"error in a32 generation: "<<endl;
		  exit (-1);
		}
		  
		if (gp->b_buffer32[ix]> gp->modulus32) {
		  cout <<"error in b generation"<<endl;
		  exit (-1);
		}
#ifdef NTL32B
		DEBUGEXP(PrepMulModPrecon((long)gp->b_buffer32[ix], (long) gp->modulus));
		gp->yminv_buffer32[ix] = PrepMulModPrecon((long)gp->b_buffer32[ix], (long) gp->modulus);
		DEBUGEXP(gp->yminv_buffer32[ix]);
#endif
		uint64_t x, y, m;
	  
		m = gp->modulus32;
		x = gp->a_buffer32[ix];
		y = gp->b_buffer32[ix];
		gp->correct32[ix] = (x*y)%m;
		//cout <<"(a*b)%m = ("<<x<<"*"<<y<<")%"<<m<<"="<<(x*y)%m<<" -->"<<gp->correct32[ix]<<endl;
		
	  }
	}
  }

  if (dbg_flag){
	
	for (auto tix = 0; tix < gvec.size(); tix++){
	  MODMULMEM *gp = &gvec[tix];
	  if (word_size == 60) {
		cout <<"modulus "<<gp->modulus<<":"<<endl;
	  }else{
		cout <<"modulus32 "<<gp->modulus32<<":"<<endl;
	  }
	  cout <<"input_a tower "<<tix<<":"<<endl;
	  for (auto ix = 0; ix< gp->n; ix++){
		if (word_size == 60) { // 64 bit arithmetic	
		  cout << gp->a_buffer[ix]<<endl;
		}else {
		  cout << gp->a_buffer32[ix]<<endl;
		}
	  }
	}
	for (auto tix = 0; tix < gvec.size(); tix++){
	  MODMULMEM *gp = &gvec[tix];
	  cout <<"input_b tower "<<tix<<":"<<endl;
	  for (auto ix = 0; ix< gp->n; ix++){
		if (word_size == 60) { // 64 bit arithmetic
		  cout << gp->b_buffer[ix]<<endl;
		}else{
		  cout << gp->b_buffer32[ix]<<endl;
		}			  
	  }
	}
  }
  

  DEBUGEXP(n_selected);
  if (verbose) print_modmulmem_vec(gvec);
  
  ofstream out_file;
  ofstream fastest_out_file;
  bool output_flag(false);
  if (out_fname.size()!= 0) {
	output_flag= true;
	out_file.open(out_fname);
	fastest_out_file.open("fast"+out_fname);
	//write out headers
	out_file<<"n, towers, ave_time_usec, sd_time_usec, th, bal, nvalid"<< endl;
	fastest_out_file<<"n, towers, ave_time_usec, sd_time_usec, th, bal, nvalid"<< endl;
  }
									  
  bool fail_flag; 
  uint64_t time_usec;
  double accume_time_usec;
  double accume_time_sq_usec;
  double sd_time_usec;
  double ave_time_usec;
	  
  fastest_struct fastest;
  fastest.ave_time = UINT64_MAX;
  fastest.sd_time = 0.0;

  CRTsetup(gvec);

  TestsEnum testId;
  
  if (alg_select.compare("div") == 0) {
	if (word_size == 30) {
	  testId = TestsEnum::NATIVE_DIV32;
	} else {
	  testId = TestsEnum::NATIVE_DIV64;
	}
  } else if (alg_select.compare("ntl") == 0) {
	if (word_size == 30) {
	  testId = TestsEnum::NATIVE_NTL32;
	} else {
	  testId = TestsEnum::NATIVE_NTL64;
	}
  } else if (alg_select.compare("bar") == 0) {
	cout <<"Warning, Barrett does not work !"<<endl;
	if (word_size == 30) {
	  testId = TestsEnum::NATIVE_BAR32;
	} else {
	  testId = TestsEnum::NATIVE_BAR64;
	}
  } else {
	cout <<"Error, wrong value for algorithm selected: "<<alg_select<<endl;

	exit(-1);
  }

  try { //big try block to catch bad kernel calls
	int maxthreads = min(n_selected, maxthreadsperblock);
	int start_th;
	int end_th;
	if (n_threads_select == 0){
	  start_th = 0; //check this should be 1. 
	  end_th = maxthreads;
	}else{
	  start_th = n_threads_select;
	  end_th = n_threads_select;
	}
	GPUwisdom w;

	bool first_run = true;
	for (int th = start_th; th <= end_th; th <<= 1) {
	  //for (int th = 4; th <= 4; th <<= 1) {
	  if (th == 0) th = 1;
			  
	  int start_bal;
	  int end_bal;
	  if (n_balance_select == 0){
		start_bal = 1;
		end_bal = 1;
	  }else{
		start_bal = n_balance_select;
		end_bal = n_balance_select;
	  }
	  for (int bal = start_bal; bal <= end_bal; bal <<= 1) { 

		w.balance = bal;
		w.threads = th;

		accume_time_sq_usec = 0.0;
		accume_time_usec = 0.0;

		uint nvalid = 0;
		for (int r = 0; r < n_repeat; r++) {
		  if (first_run) { //the first run is always slowest so repeat it to prime caches etc.
			for (auto ix = 0; ix < 10;  ix++){
			  clear_input_buffers(cache_overwrt, gvec);
			  time_usec = compute_ModMul(gvec, w, testId);
			  //cout << "throw away time "<< time_usec<<endl;
			}
			first_run = false;
		  }
		  clear_input_buffers(cache_overwrt, gvec);
		  time_usec = compute_ModMul(gvec, w, testId);
		  if (verbose) {
			cout<<"MM: "<< TestName[to_underlying(testId)] <<" n:"<<n_selected << " ntowers: "<<n_towers
				<<" th:"<<th<<" bal:"<<bal<<" "
				<< "time_usec:" << time_usec;
		  }
		  //todo move file io out of compare results. 
		  fail_flag = compare_all_tower_results(gvec, verbose);

		  // compare the computed data			  
		  if (fail_flag){
			if (verbose) cout <<" NOT verified"<<endl;
		  } else {
			if (verbose) cout <<" verified"<<endl;
			nvalid++;
			//divide the averages by the number of towers done
			accume_time_usec += (double)time_usec;
			//cout << (double)time_usec<<endl;
			accume_time_sq_usec += (double)time_usec*(double)time_usec;;
		  } //fail_flag
		} //n_repeat;
		  
		  //ModMulteardown(g);
		  
		  //compute statistics
		double variance;
		  
		ave_time_usec = accume_time_usec/(double)nvalid;
		variance = accume_time_sq_usec/(double)nvalid -
		  (ave_time_usec*ave_time_usec);
		if (nvalid>1) {
		  // compute Bessel's correction for unbiased estimate
		  variance = ((double)nvalid/(double)(nvalid-1))*variance;
		}
		sd_time_usec = sqrt(variance);
			
		cout <<"*** " <<  TestName[to_underlying(testId)]
             <<" n: "<<n_selected
			 <<" tow: "<<n_towers
			 <<" th:"<<th
			 <<" bal:"<<bal
			 <<" Average time usec: "<<ave_time_usec
			 <<" +-"<< sd_time_usec <<" usec"
			 << " "<<nvalid<<" valid runs"<< endl;
		if (output_flag){
		  out_file
			<< n_selected <<","
			<< n_towers <<","
			<<ave_time_usec<<","
			<< sd_time_usec <<","
			<<th<<","<<bal<<","
			<<nvalid<< endl;
		}

		if (fastest.ave_time > ave_time_usec) { //we have a new fastest configuration
		  fastest.ave_time = ave_time_usec;
		  fastest.sd_time = sd_time_usec;
		  fastest.n = n_selected;
		  fastest.towers = n_towers;
		  fastest.th = th;
		  fastest.bal = bal;
		  fastest.nvalid = nvalid;
		}
	  } //balance
	}//thread
		
  } catch (const char* msg) {
	cout << msg<<endl;
  }
  cout<<"### n:"<<fastest.n<<" tow: "<<fastest.towers
	  <<" Fastest averaged time usec:"<<fastest.ave_time
	  <<" +-"<<fastest.sd_time <<" usec"
	  <<" th:"<<fastest.th<<" bal:"<<fastest.bal<<" "
	  << fastest.nvalid<<" valid runs"<< endl;

  if (output_flag){
	fastest_out_file
	  <<fastest.n<<","
	  <<fastest.towers<<","
	  <<fastest.ave_time<<","
	  <<fastest.sd_time<<","
	  <<fastest.th<<","
	  <<fastest.bal<<","
	  <<fastest.nvalid<<endl;
  }

  if (output_flag){
	out_file.close();
	fastest_out_file.close();
  }
  return 0;
}

#endif //has_cuda
