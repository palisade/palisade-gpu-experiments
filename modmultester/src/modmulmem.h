#ifndef MODMUL_MEM
#define MODMUL_MEM

#include <vector>

typedef struct {
#if __has_include ("cuda.h")
  cudaStream_t SID; //cuda stream
  #else
  uint64_t SID; //dummy
#endif
  int n;						// vector length for this CRT
  //64 bit pointers
  uint64_t modulus = 0;			// modulus for this CRT
  uint64_t msb = 0;			// barrett mu 64 bit code
  uint64_t mu = 0;			// barrett mu 64 bit code
  uint64_t* a_buffer = NULL; //storage for a_input
  uint64_t* b_buffer = NULL; //storage for b_input
  uint64_t* yminv_buffer = NULL; //storage for yminv_input
  uint64_t* result = NULL; //storage for output
  uint64_t* correct = NULL; //storage for correct (for comarison)
  //32 bit pointers
  uint32_t modulus32 = 0;			// modulus for this CRT
  uint32_t mu32 = 0;			// barrett mu 32 bit code
  double invmod32 = 0;			// 1/mod 32 bit NTL code
  uint32_t* a_buffer32 = NULL; //storage for a_input
  uint32_t* b_buffer32 = NULL; //storage for b_input
#ifdef NTL32B
  uint32_t* yminv_buffer32 = NULL; //storage for yminv_input
#endif
  uint32_t* result32 = NULL; //storage for output
  uint32_t* correct32 = NULL; //storage for correct (for comarison)
} MODMULMEM;

void print_modmulmem(MODMULMEM *gp);
void print_modmulmem_vec(std::vector<MODMULMEM> &gvec);

#endif //MODMUL_MEM
