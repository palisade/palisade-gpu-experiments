// PALSIADE Library

#ifndef GPU_PROPERTIES_H
#define  GPU_PROPERTIES_H
#include <vector>
/////////////////////////////////////////
// checks for CUDA  errors if checkflag == true
// returns true if error has occured
bool CUDA_ERR_CHECK(bool checkflag){
  bool errflag = false;
  if (checkflag) {
	cudaError_t errSync  = cudaGetLastError();
	cudaError_t errAsync = cudaDeviceSynchronize();
	if (errSync != cudaSuccess){ 
	  printf("Sync kernel error: %s\n", cudaGetErrorString(errSync));
	  errflag |= true;
	}
	if (errAsync != cudaSuccess){
	  printf("Async kernel error: %s\n", cudaGetErrorString(cudaGetLastError()));
	  errflag |= true;
	}
  }
  return errflag;
}

/////////////////////////////////////////////////////
//  function to return vector of device name strings,
//  if verbose==true, also print out gpu properties 

std::vector<std::string> GPU_Properties(bool verbose){
  using namespace std;
  int nDevices;
  vector<string> devNames(0);

  cudaGetDeviceCount(&nDevices);
  for (int i = 0; i < nDevices; i++) {
    cudaDeviceProp prop;
    cudaGetDeviceProperties(&prop, i);
	if (verbose) {
	  cout<<"Device Number: "<< i<<endl;
	  cout<<"  Device name: "<< prop.name<<endl;
	  printf("  Memory Clock Rate (KHz): %d\n",
			 prop.memoryClockRate);
	  printf("  Memory Bus Width (bits): %d\n",
			 prop.memoryBusWidth);
	  printf("  Peak Memory Bandwidth (GB/s): %f\n\n",
			 2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6);
		
	  cout<<prop.totalGlobalMem<<" Global memory available on device in bytes."<<endl;
	  cout<<prop.sharedMemPerBlock<<" Shared memory available per block in bytes."<<endl;
		
		
	  cout<<"  CUDA Capability Major/Minor Version number: "
		  <<prop.major<<"."<<prop.minor<<endl;
	  cout<<  "Max dimension size of a thread block (x,y,z): ("
		  <<prop.maxThreadsDim[0]<< ", "
		  <<prop.maxThreadsDim[1]<< ", "
		  <<prop.maxThreadsDim[2]<<")"<<endl;
	  cout<<"  Max dimension size of a grid size    (x,y,z): ("
		  <<prop.maxGridSize[0]<<", "
		  <<prop.maxGridSize[1]<<", "
		  <<prop.maxGridSize[2]<<")"<<endl;
		
	  cout<<prop.canMapHostMemory<<" Device can map host memory with cudaHostAlloc/cudaHostGetDevicePointer."<<endl;
	  cout<<prop.clockRate<<" Clock frequency in kilohertz."<<endl;
	  cout<<prop.computeMode<<" Compute mode (See cudaComputeMode)."<<endl;
	  cout<<prop.deviceOverlap<<" Device can concurrently copy memory and execute a kernel."<<endl;
	  cout<<prop.integrated<<" Device is integrated as opposed to discrete."<<endl;
	  cout<<prop.kernelExecTimeoutEnabled<<" Specified whether there is a run time limit on kernels."<<endl;
		
	  cout<<prop.maxThreadsPerBlock<<" Maximum number of threads per block."<<endl;
	  cout<<prop.memPitch<<" Maximum pitch in bytes allowed by memory copies."<<endl;
	  cout<<prop.multiProcessorCount<<" Number of multiprocessors on device."<<endl;
		
	  cout<<prop.regsPerBlock<<" 32-bit registers available per block"<<endl;
	  cout<<prop.textureAlignment<<" Alignment requirement for textures."<<endl;
	  cout<<prop.totalConstMem<<" Constant memory available on device in bytes."<<endl;
	  cout<<prop.warpSize<<" Warp size in threads. "<<endl;
	}
	devNames.push_back(string(prop.name));
  }
  return devNames;
}
#endif // GPU_PROPERTIES_H
