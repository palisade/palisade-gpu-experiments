#include <cstdint>
#include <iostream>


#include <modmulmem.h>
void print_modmulmem(MODMULMEM *g){
  if (g->a_buffer!= NULL) {
	std::cout << " cudaStream_t "<< g->SID
			  << " n "<< g->n
			  << " modulus "<< g->modulus
			  << " mu "<< g->mu
			  << " a_buffer "<< g->a_buffer
			  << " b_buffer "<< g->b_buffer
			  << " yminv_buffer "<< g->yminv_buffer
			  << " result "<< g->result
			  << " correct "<< g->correct
			  << std::endl;
  } else {
	std::cout << " cudaStream_t "<< g->SID
			  << " n "<< g->n
			  << " modulus32 "<< g->modulus32
			  << " mu32 "<< g->mu32
			  << " a_buffer32 "<< g->a_buffer32
			  << " b_buffer32 "<< g->b_buffer32
	  #ifdef NTL32B
			  << " yminv_buffer32 "<< g->yminv_buffer32
	  #endif
			  << " result32 "<< g->result32
			  << " correct32 "<< g->correct32
			  << std::endl;
  }
}

void print_modmulmem_vec(std::vector<MODMULMEM> &gvec){
  for (uint twr = 0; twr< gvec.size(); twr++){
	MODMULMEM *g = &gvec[twr];
	std::cout<< " tower "<< twr;
	print_modmulmem(g);
  }
}
