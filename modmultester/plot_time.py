#!/usr/bin/env python3
# plotting functions for time vs various parameters
# for data generate by doruns_*

# plot 2d plot of a runtime vs #threads and balance for forward transforms only. 
def vsThreadBalance(th, bal, n, inverse_flag, ave_time_usec, fth, fbal, fave_time_usec, fsd_time_usec):
    import numpy as np
    from mpl_toolkits import mplot3d
    import matplotlib.pyplot as plt
    import matplotlib.cm as cm


    #compute the max dimensions (th vs bal)
    tind = np.unique(th); #gather all th values
    nt = tind.shape[0]    #determine dimension
    bind = np.unique(bal) #do same for bal values
    nb = bind.shape[0]

    time = np.zeros((nt, nb)) #construct array of times

    for i in np.arange(len(n)):
        type(inverse_flag[i])
        if not inverse_flag[i]: #this is a fwd plot
            itup = np.nonzero(tind==th[i]) #find approprate indicies
            jtup = np.nonzero(bind==bal[i])
            ix = itup[0][0] #converts to indicies. ugly
            jx = jtup[0][0]

            time[ix][jx] = ave_time_usec[i] #save this value

    #convert the indicies to log2
    tind = np.log2(tind)
    bind = np.log2(bind)

    #and data to log10
    time = np.log10(time);

    #make a 2D plotting grid
    tplot = np.outer(tind, np.ones(nb))
    bplot = np.outer(np.ones(nt),bind)

    # this is True for surface plot
    surfplot= False

    ix = 0 #forward fastest plot
    if surfplot:
        fig,ax=plt.subplots(1,1) #   fig = plt.figure()
        ax = plt.axes(projection='3d')
        cp = ax.plot_surface(tplot, bplot, time, cmap='hot')#, edgecolor='none')
        fig.colorbar(cp) # Add a colorbar to a plot
        ax.set_zlabel('log 10 runtime (uSec)')
    else:
        filled = False
        if filled:
            fig,ax=plt.subplots(1,1)
            cp = ax.contourf(tplot, bplot, time, 64, cmap='hot') #64 levels
            fig.colorbar(cp) # Add a colorbar to a plot

            #plot a white + at the location of the fastest run
            ftix = np.log2(fth[ix]) #get the indicies
            fbix = np.log2(fbal[ix])
            ax.plot([ftix], [fbix], '+', color='white')
        else:
            fig,ax=plt.subplots(1,1)
            ##im = ax.imshow(time, interpolation='bilinear', origin='lower',
            ##        cmap=cm.gray, extent=(tind[0], tind[-1], bind[0], bind[-1]))
            cp = ax.contour(tplot, bplot, time, 16, colors='k') #8 levels
            ax.clabel(cp) #label the contours
            #fig.colorbar(cp) # Add a colorbar to a plot

            #plot a black + at the location of the fastest run
            ftix = np.log2(fth[ix]) #get the indicies
            fbix = np.log2(fbal[ix])
            ax.plot([ftix], [fbix], '+', color='black')


    # label plot
    fig.suptitle('Log10 run time for n = '+str(n[ix]))
    ax.set_title('Fastest Run Time th:'+str(fth[ix])+' bal:'+str(fbal[ix])+
                 " is "+str(fave_time_usec[ix]) +'\u00b1'+ '{:.0f}'.format(fsd_time_usec[ix]) +' uS' )
    ax.set_xlabel('log2 # threads')
    ax.set_ylabel('log 2 balance')


###################################################
# plot 1d plots of  time for each thread
# if fig,ax are not present, use new figure and axis
def all_threads(th, bal, n,  ave_time_usec,  sd_time_usec, fig=0, ax=0, title_str="", plotID=0):

    import numpy as np
    from mpl_toolkits import mplot3d
    import matplotlib.pyplot as plt
    import matplotlib.cm as cm
    from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)

    #compute the max dimensions (over threads
    tind = np.unique(th); #gather all th values
    nt = tind.shape[0]    #determine dimension

    time = np.zeros(nt) #construct array of times
    sd_time = np.zeros(nt) #construct array of times

    # loop through all input and pull out the times for the fastest balance. 
    for i in np.arange(len(n)):
        itup = np.nonzero(tind==th[i]) #find approprate indicies
        ix = itup[0][0] #converts to indicies. ugly
        time[ix] = ave_time_usec[i] #save this value
        sd_time[ix] = sd_time_usec[i] 
               
    #convert the x indicies to log2
    tind = np.log2(tind)
    error = sd_time

    ix = 0 #forward fastest plot
    if (fig == 0):
        fig,ax=plt.subplots(1,1) #   fig = plt.figure()

    if plotID==0:
        ax.errorbar(tind, time, yerr=error) #, fmt ='none')
    else:
        ax.errorbar(tind, time, yerr=error, label='tower '+str(plotID)) #, fmt ='none')
    
    # label plot
    fig.suptitle('Normalized Run time for '+ title_str)

    ax.grid()
    ax.set_xlabel('log2 # threads')
    ax.set_ylabel('Runtime (uSec)')
    ax.set_yscale('log')
    #ax.yaxis.set_major_locator(MultipleLocator(100))
    #ax.yaxis.set_minor_locator(MultipleLocator(20))
    ax.grid(True)

    return fig, ax

###################################################
# plot 1d plot of omp output for forward only
def best_omp(th, n, inverse_flag, ave_time_usec,  sd_time_usec, fth, fave_time_usec, fsd_time_usec):

    import numpy as np
    from mpl_toolkits import mplot3d
    import matplotlib.pyplot as plt
    import matplotlib.cm as cm

    #compute the max dimensions (over threads
    tind = np.unique(th); #gather all th values
    nt = tind.shape[0]    #determine dimension

    time = np.zeros(nt) #construct array of times
    sd_time = np.zeros(nt) #construct array of times

    # loop through all input and pull out the times for the fastest balance. 
    for i in np.arange(len(n)):
        ##type(inverse_flag[i])
        if not inverse_flag[i]: #this is a fwd plot
            itup = np.nonzero(tind==th[i]) #find approprate indicies
            ix = itup[0][0] #converts to indicies. ugly
            time[ix] = ave_time_usec[i] #save this value
            sd_time[ix] = sd_time_usec[i] 
               
    #convert the indicies to log2
    tind = np.log2(tind)

    #and data to log10
    time_psd = np.log10(time+sd_time);
    time_msd = np.log10(time-sd_time);
    time = np.log10(time);

    #print(time_psd)
    #print(time_msd)

    ix = 0 #forward fastest plot
    fig,ax=plt.subplots(1,1) #   fig = plt.figure()
    plt.plot(tind, time, color='black')
    plt.plot(tind, time_psd, dashes=[6, 2], color='black')
    plt.plot(tind, time_msd, dashes=[6, 2], color='black')

    # label plot
    fig.suptitle('Log10 run time for n = '+str(n[ix]))
    ax.set_title('Fastest Run Time th:'+str(fth[ix])+
                 " is "+str(fave_time_usec[ix]) +'\u00b1'+ '{:.0f}'.format(fsd_time_usec[ix]) +' uS' )
    ax.grid()
    ax.set_xlabel('log2 # threads')
    ax.set_ylabel('log 10 runtime (uSec)')




###################################################
# plot 1d plots of  time for each tower
# if fig,ax are not present, use new figure and axis
def all_towers(towers,  time,  sd_time, fig=0, ax=0, title_str="", plotID=0):

    import numpy as np
    from mpl_toolkits import mplot3d
    import matplotlib.pyplot as plt
    import matplotlib.cm as cm
    from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)

    #compute the max dimensions (over threads
    towind = np.unique(towers); #gather all th values
    ntow = towind.shape[0]    #determine dimension

    #time = np.zeros(ntow) #construct array of times
    #sd_time = np.zeros(ntow) #construct array of times

    #convert the x indicies to log2
    towind = np.log2(towind)
    error = sd_time

    ix = 0 #forward fastest plot
    if (fig == 0):
        fig,ax=plt.subplots(1,1) #   fig = plt.figure()


    fmt = '.-'
    #fmt = '.--'
    if plotID==0:
        ax.errorbar(towind, time, yerr=error , fmt =fmt)
    else:
        ax.errorbar(towind, time, yerr=error, fmt = fmt, label='n '+str(plotID)) #, fmt ='none')
    

    ax.grid()
    ax.set_xlabel('log2 # towers')
    ax.set_ylabel('Runtime (uSec)')
    ax.set_yscale('log')
    #ax.yaxis.set_major_locator(MultipleLocator(100))
    #ax.yaxis.set_minor_locator(MultipleLocator(20))
    ax.grid(True)

    return fig, ax

###################################################
# plot 1d plots of  time all towers, compare widths
# if fig,ax are not present, use new figure and axis
def comp_width(towers,  time,  sd_time, this_n, this_width, fig=0, ax=0):

    import numpy as np
    from mpl_toolkits import mplot3d
    import matplotlib.pyplot as plt
    import matplotlib.cm as cm
    from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)

    #compute the max dimensions (over threads
    towind = np.unique(towers); #gather all th values
    ntow = towind.shape[0]    #determine dimension

    #time = np.zeros(ntow) #construct array of times
    #sd_time = np.zeros(ntow) #construct array of times

    #convert the x indicies to log2
    towind = np.log2(towind)
    error = sd_time

    ix = 0 #forward fastest plot
    if (fig == 0):
        fig,ax=plt.subplots(1,1) #   fig = plt.figure()

    if this_width == 32:
        fmt = '.-'
    else:
        fmt = '.--'
        
    ax.errorbar(towind, time, yerr=error, fmt = fmt, label='n '+str(this_n)+'-'+str(this_width)) #, fmt ='none')

    ax.grid()
    ax.set_xlabel('log2 # towers')
    ax.set_ylabel('Runtime (uSec)')
    ax.set_yscale('log')
    #ax.yaxis.set_major_locator(MultipleLocator(100))
    #ax.yaxis.set_minor_locator(MultipleLocator(20))
    ax.grid(True)

    return fig, ax
