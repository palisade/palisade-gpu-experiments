#!/usr/bin/env python3
#Script to plot outputs of doruns_um_st (modmul) for a given n 1024 threads and both algs. 
# call like this:
#   ./[thisfile].py  outdirpath foo
# to plot outdirpath/foo* and outdirpath/fastfoo*
# note for example to plot all towers in out-um-st-NTL-64-8192-*.dat
# enter out-um-st-NTL-64-8192-1.dat
# and this program will open all the towers in that group 

import numpy as np
import sys
import filereaders as fr
import matplotlib.pyplot as plt
import plot_time as plttime

if len(sys.argv) < 3:
    print('Usage: displsy_um_st,py [path] [filenameroot]')
    sys.exit()

file_path = sys.argv[1]
time_data_file = sys.argv[2]

fastest_ave_time_usec =  1000000;
n_size = [1024, 2048, 4096, 8192, 16384, 32768, 65536]
widths = [64]
width_ix = 0
for this_width in widths:
    n_ix = 0
    for this_n in n_size:
        towers = [1,2,4,8, 16, 32]
        fastest_time = [0, 0, 0, 0, 0, 0]
        sd_fastest_time = [0, 0, 0, 0, 0, 0]
        tower_ix = 0
        for tower in towers:

            ## parse out the tower part of time_data_file number just before ".dat"

            splitstring = time_data_file.split('.')
            #print(splitstring)

            filename_segments = splitstring[0].split('-')
            #print(filename_segments)
            alg_name = filename_segments[3]
            width_name = filename_segments[4]
            n_name = filename_segments[5]

            #this_file = splitstring[0].rstrip('0123456789')+ str(tower) + '.'+ splitstring[1]

            this_file = (filename_segments[0] + '-' + 
                         filename_segments[1] + '-' +
                         filename_segments[2] + '-' +
                         filename_segments[3] + '-' +
                         str(this_width) + '-' +
                         str(this_n) + '-' +
                         str(tower) + '.'+ splitstring[1])

            print(this_file)
            #exit()        
            full_this_file = file_path +"/"+ this_file

            #load in the runtimes from the file
            colnames, n, n_towers, \
                ave_time_usec, sd_time_usec, th, bal, nvalid\
                = fr.readUmStModMulTimeFiles(full_this_file)

            #todo make a div|ntl column
            #and a 32|64bit column
            nin = len(n_towers)


            title_str = alg_name+'-'+width_name

            # pull out the fastest in this tower and save it
            min_ix = ave_time_usec.index(min(ave_time_usec))
            this_fastest = ave_time_usec[min_ix]
            this_sd_fastest = sd_time_usec[min_ix]
            this_fastest_threads = th[min_ix]
            if (this_fastest < fastest_ave_time_usec):
                fastest_ave_time_usec = this_fastest
                fastest_sd_time_usec = this_sd_fastest
                fastest_threads = this_fastest_threads
                fastest_tower = tower
                fastest_n = this_n

            fastest_time[tower_ix] = this_fastest
            sd_fastest_time[tower_ix] = this_sd_fastest
            #print("tower ",tower, "fastest threads ",this_fastest_threads, " time ", this_fastest)
            tower_ix += 1

        #print('towers '), print(towers)

        #print(fastest_time)
        #print(sd_fastest_time)

        if (n_ix == 0 ) and (width_ix == 0):
            [fig, ax]=plttime.all_towers(towers, fastest_time, sd_fastest_time,  title_str=title_str, plotID=this_n)
        else:
            plttime.all_towers(towers, fastest_time, sd_fastest_time, fig=fig, ax=ax, title_str=title_str, plotID=this_n)

        n_ix += 1
    width_ix += 1
# label plot
fig.suptitle('Normalized Run time for '+ title_str)
    
ax.set_title('Fastest Run Time n: '+str(fastest_n)+' tower: '+str(fastest_tower) +' th:'+str(fastest_threads)+
             " is "+str(fastest_ave_time_usec) +'\u00b1'+ '{:.0f}'.format(fastest_sd_time_usec) +' uS' )

        
figname = 'fig-alltowers'+filename_segments[3]+'-'+filename_segments[4]+'.png'
print(figname)
#ax.set_ylim(bottom= 40,top= 600 )
ax.set_ylim(bottom= 40,top= 80 )
ax.legend()

fig.savefig(figname)
#plt.show()
