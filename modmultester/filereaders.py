#!/usr/bin/env python3
# functions to read files writen by doruns_ scripts

#================================
#function to read in the values from a um_st time file
# returns
# colnames which is a list of the column names
#

def readUmStModMulTimeFiles(filename):
    import csv
    n = []
    n_towers = []
    inverse_flag = []
    ave_time_usec = []
    sd_time_usec = []
    th = []
    bal = []
    nvalid = []
    colnames = []
    
    with open(filename) as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        first_row = True
        for row in csvReader:
            if (first_row):
                colnames = row
                first_row = False
            else:
                ix = 0
                n.append(int(row[ix])); ix +=1
                n_towers.append(int(row[ix])); ix +=1
                ave_time_usec.append(float(row[ix])); ix +=1
                sd_time_usec.append(float(row[ix])); ix +=1
                th.append(int(row[ix])); ix +=1
                bal.append(int(row[ix])); ix +=1
                nvalid.append(int(row[ix]))
    return colnames, n, n_towers, ave_time_usec, \
        sd_time_usec, th, bal, nvalid
#================================


#================================
#function to read in the values from a um_st time file
# returns
# colnames which is a list of the column names
#

def readUmStTimeFiles(filename):
    import csv
    n = []
    n_towers = []
    inverse_flag = []
    ave_time_usec = []
    sd_time_usec = []
    th = []
    bal = []
    nvalid = []
    colnames = []
    
    with open(filename) as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        first_row = True
        for row in csvReader:
            if (first_row):
                colnames = row
                first_row = False
            else:
                ix = 0
                n.append(int(row[ix])); ix +=1
                n_towers.append(int(row[ix])); ix +=1
                inverse_flag.append(bool(int(row[ix]))); ix +=1
                ave_time_usec.append(float(row[ix])); ix +=1
                sd_time_usec.append(float(row[ix])); ix +=1
                th.append(int(row[ix])); ix +=1
                bal.append(int(row[ix])); ix +=1
                nvalid.append(int(row[ix]))
    return colnames, n, n_towers, inverse_flag, ave_time_usec, \
        sd_time_usec, th, bal, nvalid
#================================



#================================
#function to read in the values from a omp time file
# returns
# colnames which is a list of the column names
#

def readOmpTimeFiles(filename):
    import csv
    n = []
    inverse_flag = []
    ave_time_usec = []
    sd_time_usec = []
    th = []
    nvalid = []
    colnames = []
    
    with open(filename) as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        first_row = True
        for row in csvReader:
            if (first_row):
                colnames = row
                first_row = False
            else:
                ix = 0
                n.append(int(row[ix])); ix +=1
                inverse_flag.append(bool(int(row[ix]))); ix +=1
                ave_time_usec.append(float(row[ix])); ix +=1
                sd_time_usec.append(float(row[ix])); ix +=1
                th.append(int(row[ix])); ix +=1
                nvalid.append(int(row[ix]))
    return colnames, n, inverse_flag, ave_time_usec, \
        sd_time_usec, th, nvalid
#================================

