#!/usr/bin/env python3

# need pip install seaborn
import os
import sys
import math
#import csv
import numpy as np
import matplotlib.pyplot as plt
#from matplotlib.offsetbox import (TextArea, DrawingArea, OffsetImage,
#                                  AnnotationBbox)
import timeit
import plottime

print ('Number of arguments:', len(sys.argv), 'arguments.')
print ('Argument List:', str(sys.argv))



bitList = [32, 64, 128, 256]
nList = [1024,         
         2048,
         4096,
         8192,
         16384,
         32768,
         65536,
         131072 ]

nLogList = []
for i in nList:
    nLogList.append(math.log2(i))

bitLength = len(bitList)
nLength = len(nList)
# generate a 2D array 
barrettCpuTimeTable = np.array([[0.0 for i in range(nLength)] for j in range(bitLength)])
montgomeryCpuTimeTable =  np.array([[0.0 for i in range(nLength)] for j in range(bitLength)])
remCpuTimeTable = np.array([[0.0 for i in range(nLength)] for j in range(bitLength)])
native32CpuTimeTable = np.array([[0.0 for i in range(nLength)] for j in range(bitLength)])
native64CpuTimeTable = np.array([[0.0 for i in range(nLength)] for j in range(bitLength)])

barrettGpuTimeTable =  np.array([[0.0 for i in range(nLength)] for j in range(bitLength)])
montgomeryGpuTimeTable =  np.array([[0.0 for i in range(nLength)] for j in range(bitLength)])
remGpuTimeTable = np.array([[0.0 for i in range(nLength)] for j in range(bitLength)])
native32GpuTimeTable = np.array([[0.0 for i in range(nLength)] for j in range(bitLength)])
native64GpuTimeTable = np.array([[0.0 for i in range(nLength)] for j in range(bitLength)])

for root, dirs, files in os.walk("./out"):
    #print(files)
    for name in files:
        print(name)
        thisfile = open("out/"+name, 'r')
        if name[0:8] == "out-gpu-":
            junk, type, bits, n = name.split("-")
            print(bits, n)
            i = bitList.index(int(bits))
            j = nList.index(int(n))
            func="none"
            
            for line in thisfile:  #for every line in the file
                print(line)
                # switch on the function name
                if "BARRETTMUL" in line:
                    func="Barrett"
                if "MONTMUL" in line:
                    func="Montgomery"
                if "REM" in line:
                    func="Rem"
                if "NATIVE_REM32" in line:
                    func="Native32"
                if "NATIVE_REM64" in line:
                    func="Native64"

                #save the cpu times    
                if ("cpu gmp time" in line) or ("cpu native time" in line):
                    words = line.split(" ");
                    time = math.log10(float(words[3]))
                    if func in "Barrett":
                        barrettCpuTimeTable[i][j] = time;
                    if func in "Montgomery":
                        montgomeryCpuTimeTable[i][j] = time;
                    if func in "Rem":
                        remCpuTimeTable[i][j] = time;
                    if func in "Native32":
                        native32CpuTimeTable[i][j] = time;
                    if func in "Native64":
                        native64CpuTimeTable[i][j] = time;

                # or save the gpu times
                if "gpu time" in line:
                    words = line.split(" ");
                    print("words: ", end="");
                    print(words)
                    time = math.log10(float(words[2]))
                    if func in "Barrett":
                        barrettGpuTimeTable[i][j] = time;
                    if func in "Montgomery":
                        montgomeryGpuTimeTable[i][j] = time;
                    if func in "Rem":
                        remGpuTimeTable[i][j] = time;
                    if func in "Native32":
                        native32GpuTimeTable[i][j] = time;
                    if func in "Native64":
                        native64GpuTimeTable[i][j] = time;

                        
            thisfile.close() #done parsing this file
        else:
            print("skipping "+name) #not a file we care about
        print("=====")
        
    break

#add the native's together. 
nativeGpuTimeTable = native64GpuTimeTable+native32GpuTimeTable
nativeCpuTimeTable = native64CpuTimeTable+native32CpuTimeTable

plottime.draw(barrettCpuTimeTable, barrettGpuTimeTable,
              bitList, nLogList, 'length',
              'log10(time usec)', 'Barrett time');
plottime.draw(montgomeryCpuTimeTable, montgomeryGpuTimeTable,
              bitList, nLogList, 'length',
              'log10(time usec)', 'Montgomery time');
plottime.draw(remCpuTimeTable, remGpuTimeTable,
              bitList, nLogList, 'length',
              'log10(time usec)', 'Rem time');
plottime.draw(nativeCpuTimeTable, nativeGpuTimeTable,
              bitList, nLogList, 'length',
              'log10(time usec)', 'Native time', [1,1,0,0]);

plottime.drawNative(nativeCpuTimeTable, nativeGpuTimeTable,
                    barrettGpuTimeTable, montgomeryGpuTimeTable,
                    remGpuTimeTable, remCpuTimeTable,
                    bitList, nLogList, 'length',
                    'log10(time usec)', '32 and 64 bit time', [1,1,0,0]);



plt.show(block=False)
foo = input('hit <cr> to continue')
plt.close('all')
