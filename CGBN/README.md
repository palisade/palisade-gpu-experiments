PALISADE Lattice Cryptography Library GPU Experimental Directory
================================================================

* CGBN: Experiments benchmarking NVIDIA's CGBN (Cuda GPU Big Number) library
Works on the following processors:
** MIT Supercloud volta (volta)
** MIT Supercloud tesla (testla)
** Jetson TX2 (tx2)
** Jetson Xavier (xavier)

The program "tester.cpp" is setup to run vector modulo multiply's of
various bitwidths and length.  Kernels for CGBN's Barrett as well as
Montgomery modmuls are included. Also various native kernels for 32
and 64 bit uints are provided.  The kernel launch parameters were
those originally chosen for use by Niall of NVIDIA, and may not be the
best for the non CGBN (native) kernels.

A version of tester must be compiled specifically for a given bitwidth
using the -DNBITS command line parameter passed to the make program.

All Makefiles *should* generate a version of the code that simply runs
on a cpu if the GPU is not present.

Makefiles try to detect the right version of the CUDA library. Note
CGBN needed at least version 9.2. However as more systems became
available, it was harder to distinguish between them, so we moved to
multiple makefiles and a naming convention.

use make -f Makefile_XXX to generate code on target system XXX.

In order to generate timings and generate plots the "doruns_XXX"
script will compile and run tester.cpp in a loop over different
bitwidths and lengths.  Often the limits of the ring lengths are
reduced for the smaller embedded GPUs (tx2, xavier). the output of
doruns_xxx is a bunch of out-* files.

The script parseruns_XXX.py is a python3 script to generate a csv
table called timings.csv from these outputs genrated by
doruns_XXX. This file can be imported into spreadsheet programs

plotit_XXX.py generates a set of output plots comparing the times for
the various kernels.

Note the jetson tx2 and xavier generate out-gpu-* files because they can do both gpu and cpu operations using the same program.

* Status:

MIT Volta: doruns_volta tester testergmp parseruns all work  plotit.py does
not(suspect a python version issue).

MIT Tesla: doruns_tesla tester parseruns.py all work plotit.py does
not(suspect a python version issue).

Xavier: doruns_tesla tester parseruns.py all work plotit.py does not
(numpy not installed) does run on my ubuntu though which has xavier's
file system attached


NOTE: Migration from NJIT student edition to Gitlab development edition
=======================================================================


