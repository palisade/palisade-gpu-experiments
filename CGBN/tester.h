/****

Copyright (c) 2018, NVIDIA CORPORATION.  All rights reserved.

modified by DBC to do montgomery and barrett multiplication
modified from powm.cu
changed power to y so we get result = (x*y)%modulus
****/

#ifndef TESTER_H
#define TESTER_H

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <iostream>
#include <getopt.h>
#include <gmp.h>


#if __has_include ("cuda.h")
  // enter whatever needs to defined for cuda build
  //#warning "has cuda.h"
#else
  // enter whatever needs to defined for a nocuda build
  //#warning "has no cuda.h"
  #define NOGPU
#endif



#ifndef NOGPU
#include <cuda.h>
#include "cgbn/cgbn.h"
#endif

#include "utility/support.h"
#include "debug.h"
#include "ntl_stuff.h"


// Test Enums //////////////////////////////////////////////
//define to_underlying() for allowing enum class to be used as index
template <typename E>
constexpr typename std::underlying_type<E>::type to_underlying(E e) noexcept {
    return static_cast<typename std::underlying_type<E>::type>(e);
}

//define the various tests in an enum class
enum class TestsEnum: unsigned int {
  BARRETTMUL,
  FAST_BARRETTMUL,
  MONTMUL,
  REM,
  NATIVE_REM32,
  NATIVE_REM64,
  NATIVE_NTL32,
  NATIVE_NTL64,
  NUM_TESTS
};

//define the names of the tests
std::string TestName[to_underlying(TestsEnum::NUM_TESTS)] = {
  "BARRETTMUL",
  "FAST_BARRETTMUL",
  "MONTMUL",
  "REM",
  "NATIVE_REM32",
  "NATIVE_REM64",
  "NATIVE_NTL32",
  "NATIVE_NTL64",
};


///////////////////////////////////////////////////////////
// For this tester, there are quite a few template parameters that are
// used to generate the actual code.  In order to simplify passing
// many parameters, we use the same approach as the CGBN library,
// which is to create a container class with static constants and then
// pass the class.

// The CGBN context uses the following three parameters:
//   TPB             - threads per block (zero means to use the blockDim.x)
//   MAX_ROTATION    - must be small power of 2, imperically, 4 works well
//   SHM_LIMIT       - number of bytes of dynamic shared memory available to the kernel
//   CONSTANT_TIME   - require constant time algorithms (currently, constant time algorithms are not available)

// Locally it will also be helpful to have several parameters:
//   TPI             - threads per instance
//   BITS            - number of bits per instance

template<uint32_t tpi, uint32_t bits>
class tester_params_t {
  public:
  // parameters used by the CGBN context
  static const uint32_t TPB=0;                     // get TPB from blockDim.x  
  static const uint32_t MAX_ROTATION=4;            // good default value
  static const uint32_t SHM_LIMIT=0;               // no shared mem available
  static const bool     CONSTANT_TIME=false;       // constant time implementations aren't available yet
  
  // parameters used locally in the application
  static const uint32_t TPI=tpi;                   // threads per instance
  static const uint32_t BITS=bits;                 // instance size
};


///////////////////////////////////////////////////////////
//define the main tester_t class
template<class params>
class tester_t {
  public:

  // define the instance structure for 3 vectors of bigint
  typedef struct {
    cgbn_mem_t<params::BITS> x;
    cgbn_mem_t<params::BITS> y;
    cgbn_mem_t<params::BITS> z; 
  } inst_vvv_t;

  // define the instance structure for 2 vectors of bigint
  typedef struct {
    cgbn_mem_t<params::BITS> x;
    cgbn_mem_t<params::BITS> y; 
  } inst_vv_t;

  // define the instance structure for 3 vectors of bigint
  typedef struct {
    cgbn_mem_t<params::BITS> x;
  } inst_v_t;

  // define the instance structure for 1 bigint
  typedef struct {
    cgbn_mem_t<params::BITS> a;
  } inst_i_t;

#ifndef NOGPU  

  //define ease of use typedefs
  typedef cgbn_context_t<params::TPI, params>    context_t;
  typedef cgbn_env_t<context_t, params::BITS>    env_t;
  typedef typename env_t::cgbn_t         bn_t;
  typedef typename env_t::cgbn_wide_t    bn_wide_t;
  typedef typename env_t::cgbn_local_t   bn_local_t;

  //main context, environment and instance
  context_t _context;
  env_t     _env;
  int32_t   _instance;


  //CTOR for our tester
  __device__ __forceinline__ tester_t(cgbn_monitor_t monitor, cgbn_error_report_t *report, int32_t instance) : _context(monitor, report, (uint32_t)instance), _env(_context), _instance(instance) {
  }
  

  // device code template for our montgomery multiply on the GPU
  __device__ __forceinline__ void montmul(bn_t &result,  bn_t &x,  bn_t &y, const bn_t &modulus) {

    uint32_t     mont_inv;
    // convert x into Montgomery space, save inverse
    mont_inv=cgbn_bn2mont(_env, x, x, modulus);
    // convert y into Montgomery space,       
    cgbn_bn2mont(_env, y, y, modulus);
    // compute t=x*y mod modulus
    cgbn_mont_mul(_env, result, x, y, modulus, mont_inv);      

    // convert result from Montgomery space
    cgbn_mont2bn(_env, result, result, modulus, mont_inv);
  }


  // device code template for our barrett multiply on the GPU
  __device__ __forceinline__ void barrettmul(bn_t &result,  bn_t &x,  bn_t &y, const bn_t &modulus) {

  bn_t approx;
  bn_wide_t w;
  uint32_t    clz_count;
  
  // assume modulus is a non-zero divisor, and x and y are less than modulus
  
  // compute the approximation of the inverse
  clz_count=cgbn_barrett_approximation(_env, approx, modulus);
  
  // compute the wide product of x*y
  cgbn_mul_wide(_env, w, x, y);
  
  // compute r=x*y mod modulus.  Pass the clz_count returned by the approx routine.
  cgbn_barrett_rem_wide(_env, result, w, modulus, approx, clz_count);

  }

  
  // device code template for our initializintg fast barrett multiply on the GPU
  __device__ __forceinline__ void init_fast_barrettmul(uint32_t &clz_count,  bn_t &approx, const bn_t &modulus) {
  
    // compute the approximation of the inverse
    clz_count=cgbn_barrett_approximation(_env, approx, modulus);

  }
  
  // device code template for our fast barrett multiply on the GPU
  __device__ __forceinline__ void fast_barrettmul(bn_t &result,  bn_t &x,  bn_t &y, const bn_t &modulus, bn_t& approx, uint32_t& clz_count) {

  bn_wide_t w;
  
  // assume modulus is a non-zero divisor, and x and y are less than modulus
  // compute the wide product of x*y
  cgbn_mul_wide(_env, w, x, y);
  
  // compute r=x*y mod modulus.  Pass the clz_count returned by the approx routine.
  cgbn_barrett_rem_wide(_env, result, w, modulus, approx, clz_count);

  }

  // device code template for remainder on the GPU
  __device__ __forceinline__ void rem(bn_t &result,  bn_t &x,  bn_t &y, const bn_t &modulus) {

  bn_wide_t w;
  
  // assume modulus is a non-zero divisor, and x and y are less than modulus
  // compute the wide product of x*y
  cgbn_mul_wide(_env, w, x, y);
  cgbn_rem_wide(_env, result, w, modulus);
  }

  
#endif
  //method to generate random vvv input instances that are < modulus
  __host__ static inst_vvv_t *generate_random_vvv_inInsts(uint32_t count, inst_i_t *modInst) {
    inst_vvv_t *inInsts=(inst_vvv_t *)malloc(sizeof(inst_vvv_t)*count);
    uint32_t index;
  
    for(index=0;index<count;index++) {
      random_words(inInsts[index].x._limbs, params::BITS/32);
      random_words(inInsts[index].y._limbs, params::BITS/32);
      random_words(inInsts[index].z._limbs, params::BITS/32);

      // ensure modulus is greater than x
      while(compare_words(inInsts[index].x._limbs, modInst->a._limbs, params::BITS/32)>0)
	      random_words(inInsts[index].x._limbs, params::BITS/32);

      // ensure modulus is greater than y
      while(compare_words(inInsts[index].y._limbs, modInst->a._limbs, params::BITS/32)>0)
	random_words(inInsts[index].y._limbs, params::BITS/32);

      
      // ensure modulus is greater than z
      while(compare_words(inInsts[index].z._limbs, modInst->a._limbs, params::BITS/32)>0)
	random_words(inInsts[index].z._limbs, params::BITS/32);

    }
    return inInsts;
  }

  
  //method to generate random vv input inInsts that are < modulus
  __host__ static inst_vv_t *generate_random_vv_inInsts(uint32_t count, inst_i_t *modInst) {
    inst_vv_t *inInsts=(inst_vv_t *)malloc(sizeof(inst_vv_t)*count);
    uint32_t index;
  
    for(index=0;index<count;index++) {
      random_words(inInsts[index].x._limbs, params::BITS/32);
      random_words(inInsts[index].y._limbs, params::BITS/32);

      // ensure modulus is greater than x
      while(compare_words(inInsts[index].x._limbs, modInst->a._limbs, params::BITS/32)>0)
	      random_words(inInsts[index].x._limbs, params::BITS/32);

      // ensure modulus is greater than y
      while(compare_words(inInsts[index].y._limbs, modInst->a._limbs, params::BITS/32)>0)
	random_words(inInsts[index].y._limbs, params::BITS/32);
    }
    return inInsts;
  }

  __host__ static inst_i_t conversion_function( unsigned long in){
	inst_i_t out;
	out.a._limbs[1] = (in>>32) & 0x00000000ffffffff;
	out.a._limbs[0] = in & 0x00000000ffffffff;
	return (out);
  }

#ifndef NOGPU    
  //method to generate vvv input in3Insts fro, in2Insts for ntl64
  //not needed for host only version
  __host__ static inst_vvv_t *generate_ntt_inInsts(inst_vv_t *inInsts,
												   uint32_t count,
												   inst_i_t *modInst) {
    inst_vvv_t *in3Insts=(inst_vvv_t *)malloc(sizeof(inst_vvv_t)*count);
    uint32_t index;

	uint64_t m;
	m = modInst->a._limbs[1];
	m <<=32;
	m |= modInst->a._limbs[0];

	inst_i_t tmp;
    for(index=0;index<count;index++) {
	  copy_words(inInsts[index].x._limbs, in3Insts[index].x._limbs, 2);
	  copy_words(inInsts[index].y._limbs, in3Insts[index].y._limbs, 2);
	  uint64_t y;
	  y = inInsts[index].y._limbs[1];
	  y <<=32;
	  y |= inInsts[index].y._limbs[0];
	  tmp = conversion_function(PrepMulModPrecon((long)y, (long)m));
	  copy_words(tmp.a._limbs, in3Insts[index].z._limbs, 2); 
	  
    }
    return in3Insts;
  }
#endif
  //method to generate random v outInsts that are zeroed
  __host__ static inst_v_t *generate_zero_v_outInsts(uint32_t count) {
    inst_v_t *outInsts=(inst_v_t *)malloc(sizeof(inst_v_t)*count);
    uint32_t index;
  
    for(index=0;index<count;index++) {
      zero_words(outInsts[index].x._limbs, params::BITS/32);
    }
    return outInsts;
  }
  
  //method to generate random i instance
  __host__ static inst_i_t *generate_random_i_modInst(void) {
    inst_i_t *modInst=(inst_i_t *)malloc(sizeof(inst_i_t));
    bool dbg_flag = false;
    random_words(modInst->a._limbs, params::BITS/32);
    // ensure modulus is odd
    modInst->a._limbs[0] |= 1;
    
    DEBUGEXP( modInst->a._limbs[0]);
    return modInst;
  }

  //method to generate zero i instance
  __host__ static inst_i_t *generate_zero_i_inst(void) {
    inst_i_t *inst=(inst_i_t *)malloc(sizeof(inst_i_t));
    bool dbg_flag = false;
    zero_words(inst->a._limbs, params::BITS/32);

    DEBUGEXP(inst->a._limbs[0]);
    return inst;
  }

  //method to verify the results 
  __host__ static float verify_results(inst_vv_t *inInsts,
				      inst_v_t *outInsts,
				      inst_i_t *modInst,
				      uint32_t count) {

    mpz_t x, y, m, computed, correct;
    
    bool dbg_flag(false);
    TimeVar t;
    float t_gmp;
    #ifdef __cplusplus
    DEBUG("gmp c++ support is defined");
    #else
    DEBUG("gmp c++ support is NOT defined");
    #endif
    DEBUG("gmp time " << count << " iterations");

    TIC(t);
    mpz_init(x);
    mpz_init(y);
    mpz_init(m);
    mpz_init(computed);
    mpz_init(correct);
    
    to_mpz(m, modInst->a._limbs, params::BITS/32);
    DEBUGEXP(m);
    
    for(uint32_t index=0;index<count;index++) {
      to_mpz(x, inInsts[index].x._limbs, params::BITS/32);
      to_mpz(y, inInsts[index].y._limbs, params::BITS/32);
      to_mpz(computed, outInsts[index].x._limbs, params::BITS/32);
      
      mpz_mul(correct, x, y);
      mpz_mod(correct, correct, m);

      if(mpz_cmp(correct, computed)!=0) {
#ifndef NOGPU      
        printf("gpu kernel failed on instance %d\n", index);
	std::cout << computed <<" != " << correct <<" = "
		  << x<< " * "<<y<<" % "<<m<<std::endl;
        break;
#endif
      }
    }
    mpz_clear(x);
    mpz_clear(y);
    mpz_clear(m);
    mpz_clear(computed);
    mpz_clear(correct);
    t_gmp = TOC_US(t);
 
    DEBUG("All results match");
    return t_gmp;
  }


  //method to verify the results using native arithmetic (for params::BITS<=64
  __host__ static float verify_results_native(inst_vv_t *inInsts,
				      inst_v_t *outInsts,
				      inst_i_t *modInst,
					      uint32_t count) {
    unsigned __int128 x, y, m;
    uint64_t computed, correct;

    TimeVar t;
    float t_cpu;
    bool dbg_flag(false);

    DEBUG("native time " << count << " iterations");  
    if (params::BITS == 32) {
      m = modInst->a._limbs[0];
      DEBUGEXP(modInst->a._limbs[0]);

    } else if (params::BITS == 64) {
      m = modInst->a._limbs[1];
      m <<=32;
      m |= modInst->a._limbs[0];
      DEBUGEXP(modInst->a._limbs[1]);
      DEBUGEXP(modInst->a._limbs[0]);
    }
    DEBUGEXP((uint64_t)m);

    TIC(t);
    for(uint32_t index=0;index<count;index++) {
      if (params::BITS == 32) {
		x = inInsts[index].x._limbs[0];
		y = inInsts[index].y._limbs[0];
		computed = outInsts[index].x._limbs[0];
		
      } else if (params::BITS == 64) {
		x = inInsts[index].x._limbs[1];
		x <<= 32;
		x |= inInsts[index].x._limbs[0];
		
		y = inInsts[index].y._limbs[1];
		y <<=32;
		y |= inInsts[index].y._limbs[0];
		
		computed = outInsts[index].x._limbs[1];
		computed <<=32;
		computed |= outInsts[index].x._limbs[0];
		
		
		
      } else {
		std::cout<<"bad bit size for native "<< params::BITS<<std::endl;
		return(0.0);
      }
      correct = (x*y)%m;      
	  
      if (correct!=computed) {
#ifndef NOGPU      
		printf("gpu kernel failed on instance %u\n", index);
		std::cout << computed <<" != " << correct <<" = "
				  << (uint64_t)x<< " * "<<(uint64_t)y<<" % "<<(uint64_t)m<<std::endl;
        break;
#endif
      }
    }
    t_cpu = TOC_US(t);
    
    DEBUG("All results match");
    return t_cpu;
  }



  //method to verify the results using native arithmetic (for params::BITS<=64
  __host__ static float verify_results_ntl32(inst_vv_t *inInsts,
				      inst_v_t *outInsts,
				      inst_i_t *modInst,
					      uint32_t count) {
    unsigned __int128 xo, yo, mo;

    uint64_t computed, correct;

    TimeVar t;
    float t_cpu;
    bool dbg_flag(false);

    DEBUG("native time " << count << " iterations");  

    TIC(t);
	mo = modInst->a._limbs[0];
	DEBUGEXP(modInst->a._limbs[0]);
	
	DEBUGEXP((uint64_t)mo);
	DEBUGEXP(sizeof(long));
	DEBUGEXP(sizeof(double));

	DEBUGEXP(sizeof(uint32_t));


    for(uint32_t index=0;index<count;index++) {
	  xo = inInsts[index].x._limbs[0];
	  yo = inInsts[index].y._limbs[0];
	  computed = outInsts[index].x._limbs[0];
	  
	  uint64_t x(0),y(0), m(0);
	  
	  x = inInsts[index].x._limbs[0];
	  y = inInsts[index].y._limbs[0];
	  m = modInst->a._limbs[0];
	  //sizeof long and double is 8
	  
	  long q = long(double(x) * double(y) / double(m));
	  DEBUGEXP(q);
	  DEBUGEXP(x*y);
	  DEBUGEXP(q*m);
	  DEBUGEXP(uint32_t(q*m));
	  
	  long r = x*y - q*m;
	  DEBUGEXP(r);
	  if (r >= m) 
		r -= m;
	  else if (r < 0)
		r += m;
	  
	  //r = (x*y)%m;
	  
	  //outInsts[instance].x._limbs[0] = uint32_t(r);
	  outInsts[index].x._limbs[0] = r;
	  
	  
	  computed = outInsts[index].x._limbs[0];
	  
	  correct = (xo*yo)%mo;      
	  
      if (correct!=computed) {
		printf("ntl32gpu kernel failed on instance %u\n", index);
		std::cout << computed <<" != " << correct <<" = "
				  << (uint64_t)x<< " * "<<(uint64_t)y<<" % "<<(uint64_t)m<<std::endl;
        break;
      }
    }
    t_cpu = TOC_US(t);
    
    DEBUG("All results match");
    return t_cpu;
  }



};  //class tester
#endif
