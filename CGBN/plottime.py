#!/usr/bin/env python3

import numpy as np
import matplotlib
import matplotlib.pyplot as plt

def draw(indata0, indata1, bitList, nLogList, x_label,
         y_label, title, flag = [], ax=None):

    #if not ax:
    #    ax = plt.gca()
    if not flag:
         flag = [1] * len(bitList)
    fig, axs = plt.subplots()
    plt.title(title, y = 1.0);
    for j in range (len(bitList)):
        if flag[j]:
            axs.plot(nLogList, indata0[j,:],'+-', label='C'+str(bitList[j]));
    axs.set_ylim(1, 6)
    #axs[0].set_xlabel('length')
    axs.set_ylabel(y_label)
    axs.grid(True)

    for j in range (len(bitList)):
        if flag[j]:
            axs.plot(nLogList, indata1[j,:],'x-', label='G'+str(bitList[j]));
    axs.legend()



def drawNative(indata0, indata1, indata2, indata3, indata4, indata5,
               bitList, nLogList, x_label,
               y_label, title, flag = [], ax=None):

    #if not ax:
    #    ax = plt.gca()
    if not flag:
         flag = [1] * len(bitList)
    fig, axs = plt.subplots()
    plt.title(title, y = 1.0);
    for j in range (len(bitList)):
        if flag[j]:
            axs.plot(nLogList, indata0[j,:],'+-', label='NC'+str(bitList[j]));
    axs.set_ylim(1, 5)
    #axs[0].set_xlabel('length')
    axs.set_ylabel(y_label)
    axs.grid(True)

    for j in range (len(bitList)):
        if flag[j]:
            axs.plot(nLogList, indata1[j,:],'x-', label='NG'+str(bitList[j]));

    for j in range (len(bitList)):
        if flag[j]:
            axs.plot(nLogList, indata2[j,:],'o-', label='BG'+str(bitList[j]));

    for j in range (len(bitList)):
        if flag[j]:
            axs.plot(nLogList, indata3[j,:],'s-', label='MG'+str(bitList[j]));
    for j in range (len(bitList)):
        if flag[j]:
            axs.plot(nLogList, indata4[j,:],'d-', label='RG'+str(bitList[j]));

    for j in range (len(bitList)):
        if flag[j]:
            axs.plot(nLogList, indata5[j,:],'*-', label='RC'+str(bitList[j]));
    axs.legend()
    
