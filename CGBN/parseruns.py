#!/usr/bin/env python3

import os
bitList = [32, 64, 128, 256]
nList = [1024,         
         2048,
         4096,
         8192,
         16384,
         32768,
         65536,
         131072,
         262144,
         524288,
         1048576 ]
bitLength = len(bitList)
nLength = len(nList)
# generate a 2D array 
barrettCpuTimeTable = [[0] * nLength for i in range(bitLength)]
montgomeryCpuTimeTable = [[0] * nLength for i in range(bitLength)]
barrettGpuTimeTable = [[0] * nLength for i in range(bitLength)]
montgomeryGpuTimeTable = [[0] * nLength for i in range(bitLength)]

for root, dirs, files in os.walk("."):
    #print(files)
    for name in files:
        print(name)
        thisfile = open(name, 'r')
        if name[0:4] == "out-":
            junk, bits, n = name.split("-")
            print(bits, n)
            i = bitList.index(int(bits))
            j = nList.index(int(n))
            func="none"
            
            for line in thisfile:
                print(line)
                if "BARRETTMUL" in line:
                    func="Barrett"
                if "MONTMUL" in line:
                    func="Montgomery"
                
                
                if ("cpu gmp time" in line)or("cpu native time" in line):
                    words = line.split(" ");
                    time = int(words[3])
                    if func in "Barrett":
                        barrettCpuTimeTable[i][j] = time;
                    if func in "Montgomery":
                        montgomeryCpuTimeTable[i][j] = time;

                if "gpu time" in line:
                    words = line.split(" ");
                    print("words: ", end="");
                    print(words)
                    time = int(words[2])
                    if func in "Barrett":
                        barrettGpuTimeTable[i][j] = time;
                    if func in "Montgomery":
                        montgomeryGpuTimeTable[i][j] = time;
            thisfile.close()
        else:
            print("skipping "+name)
        print("=====")
        
    break
#output the tables as a CSV
outfile = open("timings.csv",'w')
for i in range(len(barrettCpuTimeTable)):

    for j in range(len(barrettGpuTimeTable[i])):
        print(barrettGpuTimeTable[i][j], end=", ", file = outfile)
    print(", , ,", end="", file = outfile);

    for j in range(len(barrettCpuTimeTable[i])-1):
        print(barrettCpuTimeTable[i][j], end=", ", file = outfile)
    print(barrettCpuTimeTable[i][-1], file = outfile)

    
    for j in range(len(montgomeryGpuTimeTable[i])):
        print(montgomeryGpuTimeTable[i][j], end=", ", file = outfile)
    print(", , ,", end="", file = outfile);

    for j in range(len(montgomeryCpuTimeTable[i])-1):
        print(montgomeryCpuTimeTable[i][j], end=", ", file = outfile)
    print(montgomeryCpuTimeTable[i][-1], file = outfile)
    print('" "," "', file = outfile)
outfile.close()
