/****

Copyright (c) 2018, NVIDIA CORPORATION.  All rights reserved.

cuda malloc tester
****/
#define PROFILE //to turn on PROFILELOG

#include "tester.h"


// method to initialize the test

void init_malloc_test(cgbn_error_report_t * &report,
		      bool dbg_flag) {
  TimeVar t, t1;
  
  float t_devset, t_malloc, t_erralloc, t_tot_gpu;

#ifndef NOGPU

  std::cout << "init_malloc_test"<<std::endl;  

  TIC(t);
  TIC(t1);
  CUDA_CHECK(cudaSetDevice(0));
  t_devset = TOC_US(t1);

    
  TIC(t1);
  // create a cgbn_error_report for CGBN to report back errors
  CUDA_CHECK(cgbn_error_report_alloc(&report)); 
  t_erralloc=TOC_US(t1);
  t_tot_gpu = TOC_US(t);
  
  std::cout.precision(0);
  std::cout<<std::fixed;
  PROFILELOG("setdevice\t"<<t_devset<<" usec\t"
	     <<100.0*t_devset/t_tot_gpu<<"%");
  PROFILELOG("cudaErralloc\t"<<t_erralloc<<" usec\t"
	     <<100.0*t_erralloc/t_tot_gpu<<"%");  
  PROFILELOG("gpu setup time\t" << t_tot_gpu << " usec\t100%");
  PROFILELOG("=====");
  std::cout.precision(0);
#else
  t_devset = 0.0;
  t_malloc = 0.0;
  t_erralloc = 0.0;
  t_tot_gpu = t_devset+t_malloc+t_erralloc;
  PROFILELOG("gpu time\t" << t_tot_gpu << " usec\t100%");  
#endif

}

void teardown_malloc_test(cgbn_error_report_t * &report, bool dbg_flag) {
  TimeVar t, t1;
  float t_errfree, t_tot_gpu;

  #ifndef NOGPU
  TIC(t);

  TIC(t1);
  CUDA_CHECK(cgbn_error_report_free(report));
  t_errfree=TOC_US(t1);
  t_tot_gpu = TOC_US(t);
  
  std::cout.precision(2);
  PROFILELOG("cudaErrfree\t"<<t_errfree<<" usec\t"
	     <<100.0*t_errfree/t_tot_gpu<<"%");  
  PROFILELOG("gpu teardown\t" << t_tot_gpu << " usec\t100%");
  PROFILELOG("=====");
  std::cout.precision(0);
  #else
  t_errfree = 0.0;
  t_tot_gpu = t_errfree;
  PROFILELOG("gpu teardown\t" << t_tot_gpu << " usec\t100%");
  #endif

}


// 
template<uint32_t bits>
void test_mallocs( cgbn_error_report_t * &report,
	       uint32_t instance_count, bool dbg_flag) {
  TimeVar t, t1;
  
  float t_mallocvvvv, t_mallocvvv, t_mallocvv, t_mallocv1,t_mallocv2,t_mallocv3, t_malloci, t_tot_gpu;
  float t_mmovevvvv, t_mmovevvv, t_mmovevv, t_mmovev, t_mmovei;
  typedef typename tester_t<bits>::instance_vvvv_t instance_vvvv_t;
  typedef typename tester_t<bits>::instance_vvv_t instance_vvv_t;
  typedef typename tester_t<bits>::instance_vv_t instance_vv_t;
  typedef typename tester_t<bits>::instance_v_t instance_v_t;
  typedef typename tester_t<bits>::instance_i_t instance_i_t;

  instance_vvvv_t * g_vvvv;
  instance_vvv_t * g_vvv;
  instance_vv_t * g_vv;
  instance_v_t * g_v1, *g_v2, *g_v3;
  instance_i_t * g_i;

	      
#ifndef NOGPU
  std::cout << "malloc_test " << instance_count << " instances"<<std::endl;  

  std::cout<<"Allocating space for instances in the GPU ..."<<std::endl;

  instance_vvvv_t * i_vvvv = (instance_vvvv_t *)malloc(sizeof(instance_vvvv_t)*instance_count);
  instance_vvv_t *i_vvv = (instance_vvv_t *)malloc(sizeof(instance_vvv_t)*instance_count);
  instance_vv_t *i_vv = (instance_vv_t *)malloc(sizeof(instance_vv_t)*instance_count);
  instance_v_t * i_v = (instance_v_t *)malloc(sizeof(instance_v_t)*instance_count);
  instance_i_t *i_i = (instance_i_t *)malloc(sizeof(instance_i_t));

  TIC(t);
 
  TIC(t1);
  CUDA_CHECK(cudaMalloc((void **)&g_vvvv, sizeof(instance_vvvv_t)*instance_count));
  t_mallocvvvv=TOC_US(t1);
  TIC(t1);
  CUDA_CHECK(cudaMalloc((void **)&g_vvv, sizeof(instance_vvv_t)*instance_count));
  t_mallocvvv=TOC_US(t1);
  TIC(t1);
  CUDA_CHECK(cudaMalloc((void **)&g_vv, sizeof(instance_vv_t)*instance_count));
  t_mallocvv=TOC_US(t1);

  TIC(t1);
  CUDA_CHECK(cudaMalloc((void **)&g_v1, sizeof(instance_v_t)*instance_count));
  t_mallocv1=TOC_US(t1);

    CUDA_CHECK(cudaMalloc((void **)&g_v2, sizeof(instance_v_t)*instance_count));
  t_mallocv2=TOC_US(t1);

    CUDA_CHECK(cudaMalloc((void **)&g_v3, sizeof(instance_v_t)*instance_count));
  t_mallocv3=TOC_US(t1);

  TIC(t1);
  CUDA_CHECK(cudaMalloc((void **)&g_i, sizeof(instance_i_t)));
  t_malloci=TOC_US(t1);

  TIC(t1);
  t_tot_gpu = TOC_US(t);
  
  std::cout.precision(0);
  std::cout<<std::fixed;
  PROFILELOG("cudaMallocvvvv\t"<<t_mallocvvvv<<" usec\t"
	     <<100.0*t_mallocvvvv/t_tot_gpu<<"%");
  PROFILELOG("cudaMallocvvv\t"<<t_mallocvvv<<" usec\t"
	     <<100.0*t_mallocvvv/t_tot_gpu<<"%");
  PROFILELOG("cudaMallocvv\t"<<t_mallocvv<<" usec\t"
	     <<100.0*t_mallocvv/t_tot_gpu<<"%");
  PROFILELOG("cudaMallocv1\t"<<t_mallocv1<<" usec\t"
	     <<100.0*t_mallocv1/t_tot_gpu<<"%");
  PROFILELOG("cudaMallocv2\t"<<t_mallocv2<<" usec\t"
	     <<100.0*t_mallocv2/t_tot_gpu<<"%");
  PROFILELOG("cudaMallocv3\t"<<t_mallocv3<<" usec\t"
	     <<100.0*t_mallocv3/t_tot_gpu<<"%");
  PROFILELOG("cudaMalloci\t"<<t_malloci<<" usec\t"
	     <<100.0*t_malloci/t_tot_gpu<<"%");
  PROFILELOG("gpu setup time\t" << t_tot_gpu << " usec\t100%");

 TIC(t);
  TIC(t1);
  CUDA_CHECK(cudaMemcpy(g_vvvv, i_vvvv, sizeof(instance_vvvv_t)*instance_count,
			cudaMemcpyHostToDevice));

  t_mmovevvvv=TOC_US(t1);
  TIC(t1);
  CUDA_CHECK(cudaMemcpy(g_vvv, i_vvv, sizeof(instance_vvv_t)*instance_count,
			cudaMemcpyHostToDevice));
  t_mmovevvv=TOC_US(t1);
  TIC(t1);
  CUDA_CHECK(cudaMemcpy(g_vv, i_vv, sizeof(instance_vv_t)*instance_count,
			cudaMemcpyHostToDevice));
  t_mmovevv=TOC_US(t1);
  TIC(t1);
  //CUDA_CHECK(cudaMemcpy(g_v1, i_v, sizeof(instance_v_t)*instance_count,
  //			cudaMemcpyHostToDevice));
  t_mmovev=TOC_US(t1);
  TIC(t1);
  CUDA_CHECK(cudaMemcpy(g_i, i_i, sizeof(instance_i_t),
			cudaMemcpyHostToDevice));

  t_mmovei=TOC_US(t1);

  TIC(t1);
  t_tot_gpu = TOC_US(t);
  

  std::cout.precision(0);
  std::cout<<std::fixed;
  PROFILELOG("cudaMmovevvvv\t"<<t_mmovevvvv<<" usec\t"
	     <<100.0*t_mmovevvvv/t_tot_gpu<<"%");
  PROFILELOG("cudaMmovevvv\t"<<t_mmovevvv<<" usec\t"
	     <<100.0*t_mmovevvv/t_tot_gpu<<"%");
  PROFILELOG("cudaMmovevv\t"<<t_mmovevv<<" usec\t"
	     <<100.0*t_mmovevv/t_tot_gpu<<"%");
  PROFILELOG("cudaMmovev\t"<<t_mmovev<<" usec\t"
	     <<100.0*t_mmovev/t_tot_gpu<<"%");
  PROFILELOG("cudaMmovei\t"<<t_mmovei<<" usec\t"
	     <<100.0*t_mmovei/t_tot_gpu<<"%");
  PROFILELOG("gpu setup time\t" << t_tot_gpu << " usec\t100%");


  PROFILELOG("=====");
  std::cout.precision(0);


  free(i_vvvv);
  free(i_vvv);
  free(i_vv);
  free(i_v);
  free(i_i);
  
  CUDA_CHECK(cudaFree(g_vvvv));
  CUDA_CHECK(cudaFree(g_vvv));
  CUDA_CHECK(cudaFree(g_vv));
  CUDA_CHECK(cudaFree(g_v1));
  CUDA_CHECK(cudaFree(g_v2));
  CUDA_CHECK(cudaFree(g_v3));
  CUDA_CHECK(cudaFree(g_i));


#endif

}

int main(int argc, char* argv[]) {
  bool dbg_flag = false; //set to true to turn on DEBUG()
  int opt; //used in getting options
  
  uint32_t n_instances = 1024;
  //const uint32_t n_bits =1024;
  const uint32_t n_bits =64;

  while ((opt = getopt(argc, argv, "dn:h")) != -1) {
    switch (opt) {
    case 'd':
      dbg_flag = true;
      std::cout << "setting dbg_flag true" << std::endl;
      break;
    case 'n':
      n_instances = atoi(optarg);
      if (n_instances <1) {
	n_instances = 1;
      }
      std::cout << "Running "<< n_instances<< " instances" << std::endl;
      break;
    case 'h':
    default: /* '?' */
      std::cerr<< "Usage: "<<argv[0]<<" <arguments> " <<std::endl
	       << "required arguments:" <<std::endl
	       << "  -n num_instances (1024)"  <<std::endl
	       << "optional arguments:"<<std::endl
	       << "  -d  (false) sets debug flag true " <<std::endl
	       << "  -h  prints this message" <<std::endl;
      exit(EXIT_FAILURE);
    }
  }
 
  DEBUG ("Debugging statements on");
  std::cout << "Using "<< n_bits<< " bit arithmetic" << std::endl;
  // private pointers to data
  tester_t<n_bits>::instance_vvvv_t *gpu_vvvv_Instances(NULL);
  tester_t<n_bits>::instance_vvv_t *gpu_vvv_Instances(NULL);
  tester_t<n_bits>::instance_i_t *gpu_i_Instance(NULL);
  
  cgbn_error_report_t *report(0);
  init_malloc_test(report, dbg_flag);

  test_mallocs<n_bits>(report, n_instances, dbg_flag);

  test_mallocs<n_bits>(report, n_instances, dbg_flag);

  test_mallocs<n_bits>(report, n_instances, dbg_flag);

  teardown_malloc_test(report, dbg_flag);
