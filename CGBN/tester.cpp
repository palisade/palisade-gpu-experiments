/****

Copyright (c) 2018, NVIDIA CORPORATION.  All rights reserved.

modified by DBC to do montgomery and barrett multiplication
modified from powm.cu
changed power to y so we get result = (x*y)%modulus
****/
#define PROFILE //to turn on PROFILELOG

#ifndef NBITS
#define NBITS 128
#endif
#ifndef NTPI
#define NTPI 4
#endif

#include "tester.h"
#ifndef NOGPU
#include "utility/cuda_uint128.h"
#endif

#include "ntl_stuff.h"

#ifndef NOGPU  
///////////////////////////////////////////////////////
// montmul kernel implementation using cgbn
// Unfortunately, the kernel must be separate from the object code
// Require that x<modulus

template<class params>
__global__ void kernel_montmul(cgbn_error_report_t *report,
			       typename tester_t<params>::inst_vv_t *inInsts,
			       typename tester_t<params>::inst_v_t *outInsts,
			       typename tester_t<params>::inst_i_t *modInst,
			       uint32_t count) {
  int32_t instance;

  // decode an instance number from the blockIdx and threadIdx
  instance=(blockIdx.x*blockDim.x + threadIdx.x)/params::TPI;
  if(instance>=count)
    return;

  tester_t<params> po(cgbn_report_monitor, report, instance);
  typename tester_t<params>::bn_t  r, x, y, m;
  
  // the loads and stores can go in the class, but it seems more natural to have them
  // here and to pass in and out bignums
  cgbn_load(po._env, x, &(inInsts[instance].x));
  cgbn_load(po._env, y, &(inInsts[instance].y));
  cgbn_load(po._env, m, &(modInst->a));
  
  po.montmul(r, x, y, m);
  
  cgbn_store(po._env, &(outInsts[instance].x), r);
}

///////////////////////////////////////////////////////////
// barrett kernel implementation using cgbn
// Unfortunately, the kernel must be separate from the object code
// Require that x<modulus

template<class params>
__global__ void kernel_barrettmul(cgbn_error_report_t *report,
				  typename tester_t<params>::inst_vv_t *inInsts,
				  typename tester_t<params>::inst_v_t *outInsts,
				  typename tester_t<params>::inst_i_t *modInst,
				  uint32_t count) {
  int32_t instance;
  
  // decode an instance number from the blockIdx and threadIdx
  instance=(blockIdx.x*blockDim.x + threadIdx.x)/params::TPI;
  if(instance>=count)
    return;

  tester_t<params> po(cgbn_report_monitor, report, instance);
  typename tester_t<params>::bn_t  r, x, y, m;
  
  // the loads and stores can go in the class, but it seems more natural to have them
  // here and to pass in and out bignums
  cgbn_load(po._env, x, &(inInsts[instance].x));
  cgbn_load(po._env, y, &(inInsts[instance].y));
  cgbn_load(po._env, m, &(modInst->a));
  
  po.barrettmul(r, x, y, m);
  
  cgbn_store(po._env, &(outInsts[instance].x), r);
}

///////////////////////////////////////////////////////////
// remainder kernel implementation using cgbn
// Unfortunately, the kernel must be separate from the object code
// Require that x<modulus

template<class params>
__global__ void kernel_rem(cgbn_error_report_t *report,
				  typename tester_t<params>::inst_vv_t *inInsts,
				  typename tester_t<params>::inst_v_t *outInsts,
				  typename tester_t<params>::inst_i_t *modInst,
				  uint32_t count) {
  int32_t instance;
  
  // decode an instance number from the blockIdx and threadIdx
  instance=(blockIdx.x*blockDim.x + threadIdx.x)/params::TPI;
  if(instance>=count)
    return;

  tester_t<params> po(cgbn_report_monitor, report, instance);
  typename tester_t<params>::bn_t  r, x, y, m;
  
  // the loads and stores can go in the class, but it seems more natural to have them
  // here and to pass in and out bignums
  cgbn_load(po._env, x, &(inInsts[instance].x));
  cgbn_load(po._env, y, &(inInsts[instance].y));
  cgbn_load(po._env, m, &(modInst->a));
  
  po.rem(r, x, y, m);
  
  cgbn_store(po._env, &(outInsts[instance].x), r);
}
///////////////////////////////////////////////////////////
// remainder kernel implementation using cgbn
// Unfortunately, the kernel must be separate from the object code
// Require that x<modulus

template<class params>
__global__ void kernel_native_rem32(cgbn_error_report_t * __restrict__ report,
				  typename tester_t<params>::inst_vv_t * __restrict__ inInsts,
				  typename tester_t<params>::inst_v_t * __restrict__ outInsts,
				  typename tester_t<params>::inst_i_t * __restrict__ modInst,
				  uint32_t count) {
  int32_t instance;
  
  // decode an instance number from the blockIdx and threadIdx
  instance=(blockIdx.x*blockDim.x + threadIdx.x);
  if(instance>=count)
    return;

  uint32_t r;
  uint64_t x,y,m;

  x = inInsts[instance].x._limbs[0];
  y = inInsts[instance].y._limbs[0];
  m = modInst->a._limbs[0];
  r = (x*y)%m;
  //r = x%m;
 outInsts[instance].x._limbs[0] = r;
}

// divide and conquer algorithm from /https://www.codeproject.com/Tips/785014/UInt-Division-Modulus
// divde the 128 bit {u1,u0} by v.
// quotient in q, remainder in r
__device__ void divmod128by64(const uint64_t u1, const uint64_t u0, uint64_t v, uint64_t& q, uint64_t& r)
{
    const uint64_t b = 1ll << 32;
    uint64_t un1, un0, vn1, vn0, q1, q0, un32, un21, un10, rhat, left, right;
    size_t s;

    s = __clzll(v); //cuda count leading zeros
    v <<= s;
    vn1 = v >> 32;
    vn0 = v & 0xffffffff;

    if (s > 0)
    {
        un32 = (u1 << s) | (u0 >> (64 - s));
        un10 = u0 << s;
    }
    else
    {
        un32 = u1;
        un10 = u0;
    }

    un1 = un10 >> 32;
    un0 = un10 & 0xffffffff;

    q1 = un32 / vn1;
    rhat = un32 % vn1;

    left = q1 * vn0;
    right = (rhat << 32) + un1;
again1:
    if ((q1 >= b) || (left > right))
    {
        --q1;
        rhat += vn1;
        if (rhat < b)
        {
            left -= vn0;
            right = (rhat << 32) | un1;
            goto again1;
        }
    }

    un21 = (un32 << 32) + (un1 - (q1 * v));

    q0 = un21 / vn1;
    rhat = un21 % vn1;

    left = q0 * vn0;
    right = (rhat << 32) | un0;
again2:
    if ((q0 >= b) || (left > right))
    {
        --q0;
        rhat += vn1;
        if (rhat < b)
        {
            left -= vn0;
            right = (rhat << 32) | un0;
            goto again2;
        }
    }

    r = ((un21 << 32) + (un0 - (q0 * v))) >> s;
    q = (q1 << 32) | q0;
}

template<class params>
__global__ void kernel_native_rem64(cgbn_error_report_t * __restrict__ report,
				  typename tester_t<params>::inst_vv_t * __restrict__ inInsts,
				  typename tester_t<params>::inst_v_t *  __restrict__ outInsts,
				  typename tester_t<params>::inst_i_t *  __restrict__ modInst,
				  uint32_t count) {
  int32_t instance;
  
  // decode an instance number from the blockIdx and threadIdx
  instance=(blockIdx.x*blockDim.x + threadIdx.x);
  if(instance>=count)
    return;

  uint64_t r(0);
  uint64_t x(0),y(0),m(0);
  uint128_t ww(0);

  
  x = inInsts[instance].x._limbs[1];
  x<<=32;
  x|= inInsts[instance].x._limbs[0];
  
  y = inInsts[instance].y._limbs[1];
  y<<=32;
  y |= inInsts[instance].y._limbs[0];
  
  m = modInst->a._limbs[1];
  m<<=32;
  m|= modInst->a._limbs[0];

  ww.lo = x * y;
  ww.hi = __umul64hi(x, y);

  uint64_t q(0);

  divmod128by64(ww.hi, ww.lo, m, q, r);

  outInsts[instance].x._limbs[1] = (r>>32) & 0x00000000ffffffff;
  outInsts[instance].x._limbs[0] = r & 0x00000000ffffffff;
  //__syncthreads();
}

template<class params>
__global__ void kernel_native_NTL32(cgbn_error_report_t * __restrict__ report,
				  typename tester_t<params>::inst_vv_t * __restrict__ inInsts,
				  typename tester_t<params>::inst_v_t * __restrict__ outInsts,
				  typename tester_t<params>::inst_i_t * __restrict__ modInst,
				  uint32_t count) {
  int32_t instance;
  
  // decode an instance number from the blockIdx and threadIdx
  instance=(blockIdx.x*blockDim.x + threadIdx.x);
  if(instance>=count)
    return;
#if 1
  uint64_t x(0),y(0), m(0);

  x = inInsts[instance].x._limbs[0];
  y = inInsts[instance].y._limbs[0];
  m = modInst->a._limbs[0];
  //sizeof long and double is 8
  //note 1/double(m) should be precomputed
   long q = long(double(x) * double(y) / double(m));
   long r = x*y - q*m;
   if (r >= m) 
      r -= m;
   else if (r < 0)
      r += m;
  
  //r = (x*y)%m;

   //outInsts[instance].x._limbs[0] = uint32_t(r);
   outInsts[instance].x._limbs[0] = r;
#endif
}

template<class params>
__global__ void kernel_native_NTL64(cgbn_error_report_t * __restrict__ report,
				  typename tester_t<params>::inst_vvv_t * __restrict__ inInsts,
				  typename tester_t<params>::inst_v_t * __restrict__ outInsts,
				  typename tester_t<params>::inst_i_t * __restrict__ modInst,
				  uint32_t count) {
  int32_t instance;
  
  // decode an instance number from the blockIdx and threadIdx
  instance=(blockIdx.x*blockDim.x + threadIdx.x);
  if(instance>=count)
    return;


  uint64_t x(0),y(0), yninv(0), m(0);
  
  x = inInsts[instance].x._limbs[1];
  x<<=32;
  x|= inInsts[instance].x._limbs[0];
  
  y = inInsts[instance].y._limbs[1];
  y<<=32;
  y |= inInsts[instance].y._limbs[0];

  yninv = inInsts[instance].z._limbs[1];
  yninv<<=32;
  yninv|= inInsts[instance].z._limbs[0];

  m = modInst->a._limbs[1];
  m<<=32;
  m|= modInst->a._limbs[0];

  ////start here

  /////
   uint64_t qq = __umul64hi(x, yninv);
   unsigned long rr = x*y - qq*m;
   uint64_t r = uint64_t(sp_CorrectExcess(long(rr), long(m)));

  
  outInsts[instance].x._limbs[1] = (r>>32) & 0x00000000ffffffff;
  outInsts[instance].x._limbs[0] = r & 0x00000000ffffffff;
  //__syncthreads();
}

////////////////////////////////////////////////////////////////
// init_fast_barrettmul kernel implementation using cgbn
// Precompute inverse from modulus and save in clzInst and approxInst
// Unfortunately, the kernel must be separate from the object code
// Require that x<modulus

template<class params>
__global__ void kernel_init_fast_barrettmul(cgbn_error_report_t *report,
			  typename tester_t<params>::inst_i_t *modInst,
			  typename tester_t<params>::inst_i_t *approxInst,
    			  typename tester_t<params>::inst_i_t *clzInst,
		       	  uint32_t count) {
  int32_t instance;
  
  // decode an instance number from the blockIdx and threadIdx
  instance=(blockIdx.x*blockDim.x + threadIdx.x)/params::TPI;
  if(instance>=count)
    return;

  tester_t<params> po(cgbn_report_monitor, report, instance);
  typename tester_t<params>::bn_t  approx, m, bn_clz_count;
  uint32_t clz_count;
  
  // the loads and stores can go in the class,
  // but it seems more natural to have them
  // here and to pass in and out bignums

  cgbn_load(po._env, m, &(modInst->a));
  po.init_fast_barrettmul(clz_count, approx, m);

  //save the clz_count, convert from uint to bn
  cgbn_set_ui32(po._env, bn_clz_count, clz_count);

  // and store
  cgbn_store(po._env, &(clzInst->a), bn_clz_count);
  cgbn_store(po._env, &(approxInst->a), approx);
}

////////////////////////////////////////////////////////////////
// init_fast_barrettmul kernel implementation using cgbn
// Precompute inverse
// Unfortunately, the kernel must be separate from the object code
// Require that x<modulus


template<class params>
__global__ void kernel_fast_barrettmul(cgbn_error_report_t *report,
			typename tester_t<params>::inst_vv_t *inInsts,
			typename tester_t<params>::inst_v_t *outInsts,
			typename tester_t<params>::inst_i_t *modInst,
			typename tester_t<params>::inst_i_t *approxInst,
    			typename tester_t<params>::inst_i_t *clzInst,
			uint32_t count) {
  int32_t instance;
  
  // decode an instance number from the blockIdx and threadIdx
  instance=(blockIdx.x*blockDim.x + threadIdx.x)/params::TPI;
  if(instance>=count)
    return;

  tester_t<params> po(cgbn_report_monitor, report, instance);
  typename tester_t<params>::bn_t  r, x, y, m, bn_clz_count, approx;
  uint32_t clz_count;
  
  // the loads and stores can go in the class, but it seems more natural to have them
  // here and to pass in and out bignums
  cgbn_load(po._env, x, &(inInsts[instance].x));
  cgbn_load(po._env, y, &(inInsts[instance].y));
  cgbn_load(po._env, m, &(modInst->a));
  cgbn_load(po._env, approx, &(approxInst->a));
  cgbn_load(po._env, bn_clz_count, &(clzInst->a));

  //convert bn_clz_count to uint32
  clz_count = cgbn_get_ui32(po._env, bn_clz_count);
  po.fast_barrettmul(r, x, y, m, approx, clz_count);
  
  cgbn_store(po._env, &(outInsts[instance].x), r);
}
#endif




///////////////////////////////////////////////////////////////////////////
// method to initialize the test
template<class params>
void init_test(typename tester_t<params>::inst_vv_t * &gInInsts,
			   typename tester_t<params>::inst_vvv_t * &gIn3Insts,
	       typename tester_t<params>::inst_v_t * &gOutInsts,
	       typename tester_t<params>::inst_i_t * &gModInst,
	       typename tester_t<params>::inst_i_t * &gApproxInst,
	       typename tester_t<params>::inst_i_t * &gClzInst,
	       cgbn_error_report_t * &report,
	       uint32_t inst_count,
	       bool dbg_flag, bool memstat_flag) {

  TimeVar t, t1;

#ifndef NOGPU
  
  float t_devset, t_malloc1, t_malloc2, t_malloc3, t_malloc4,
	t_erralloc, t_tot_gpu;
  typedef typename tester_t<params>::inst_vv_t inst_vv_t;
  typedef typename tester_t<params>::inst_vvv_t inst_vvv_t;
  typedef typename tester_t<params>::inst_v_t inst_v_t;
  typedef typename tester_t<params>::inst_i_t inst_i_t;

  std::cout << "init_test " << inst_count << " instances"<<std::endl;  
  std::cout<<"Allocating space for I/O instances in the GPU ..."<<std::endl;

  TIC(t);
  TIC(t1);
  CUDA_CHECK(cudaSetDevice(0));
  t_devset = TOC_US(t1);

  TIC(t1);
  CUDA_CHECK(cudaMalloc((void **)&gInInsts, sizeof(inst_vv_t)*inst_count));
  t_malloc1=TOC_US(t1);
  TIC(t1);
  CUDA_CHECK(cudaMalloc((void **)&gIn3Insts, sizeof(inst_vvv_t)*inst_count));
  t_malloc2=TOC_US(t1);
TIC(t1);
  CUDA_CHECK(cudaMalloc((void **)&gOutInsts, sizeof(inst_v_t)*inst_count));
  t_malloc3=TOC_US(t1);

  CUDA_CHECK(cudaMalloc((void **)&gModInst, sizeof(inst_i_t)));
  CUDA_CHECK(cudaMalloc((void **)&gApproxInst, sizeof(inst_i_t)));
  CUDA_CHECK(cudaMalloc((void **)&gClzInst, sizeof(inst_i_t)));

  TIC(t1);
  t_malloc4=TOC_US(t1);    
  TIC(t1);
  // create a cgbn_error_report for CGBN to report back errors
  CUDA_CHECK(cgbn_error_report_alloc(&report)); 
  t_erralloc=TOC_US(t1);
  t_tot_gpu = TOC_US(t);
  
  if (memstat_flag) {
    std::cout.precision(0);
    std::cout<<std::fixed;
    PROFILELOG("setdevice\t"<<t_devset<<" usec\t"
	       <<100.0*t_devset/t_tot_gpu<<"%");
    PROFILELOG("cudaMalloc1\t"<<t_malloc1<<" usec\t"
	       <<100.0*t_malloc1/t_tot_gpu<<"%");
    PROFILELOG("cudaMalloc2\t"<<t_malloc2<<" usec\t"
	       <<100.0*t_malloc2/t_tot_gpu<<"%");
    PROFILELOG("cudaMalloc3\t"<<t_malloc3<<" usec\t"
	       <<100.0*t_malloc3/t_tot_gpu<<"%");
    PROFILELOG("cudaMalloc4\t"<<t_malloc4<<" usec\t"
	       <<100.0*t_malloc4/t_tot_gpu<<"%");
    PROFILELOG("cudaErralloc\t"<<t_erralloc<<" usec\t"
	       <<100.0*t_erralloc/t_tot_gpu<<"%");  
    PROFILELOG("gpu setup time\t" << t_tot_gpu << " usec\t100%");
    PROFILELOG("=====");
    std::cout.precision(0);
  }
#endif

}

  
/////////////////////////////////////////////////////////////////  
// method to run the test on the GPU
template<class params>
void run_test(TestsEnum test_num,
	      uint32_t inst_count,
	      uint32_t n_loop,
	      typename tester_t<params>::inst_vv_t *gInInsts,
	      typename tester_t<params>::inst_vvv_t *gIn3Insts,
	      typename tester_t<params>::inst_v_t *gOutInsts,
	      typename tester_t<params>::inst_i_t *gModInst,
	      typename tester_t<params>::inst_i_t * gApproxInst,
	      typename tester_t<params>::inst_i_t * gClzInst,
	      cgbn_error_report_t *report, bool dbg_flag) {

  TimeVar t, t1;
  float t_memcpy_in(0.0), t_run_sync(0.0), t_report(0.0),
    t_tot_gpu(0.0), t_memcpy_out(0.0), t_cpu(0.0);
#ifndef NOGPU  
  typedef typename tester_t<params>::inst_vvv_t inst_vvv_t;
#endif
  typedef typename tester_t<params>::inst_vv_t inst_vv_t;
  typedef typename tester_t<params>::inst_v_t inst_v_t;
  typedef typename tester_t<params>::inst_i_t inst_i_t;
  
  //local copies of the memory allocations
  inst_vv_t *inInsts(NULL);
#ifndef NOGPU
  inst_vvv_t *in3Insts(NULL);
#endif
  inst_v_t *outInsts(NULL);
  inst_i_t *modInst(NULL);
#if 0
  // the next two are for debug
  inst_i_t *approxInst(NULL);
  inst_i_t *clzInst(NULL);  
#endif
  
  std::string verifyType;
  
  int32_t TPB=(params::TPB==0) ? 128 : params::TPB; // default threads per block to 128
  int32_t TPI=params::TPI, IPB=TPB/TPI;	// IPB is instances per block
  
  std::cout << "gpu test " <<TestName[to_underlying(test_num)]<<" "
			<< inst_count << " iterations"<<std::endl;  
  std::cout<<"TPB: "<<TPB<<" TPI: "<<TPI<<" IPB: "<<IPB<<std::endl;
  
  for (uint32_t ix = 0; ix< n_loop+1; ix++) {
    DEBUG("Generating input_instances ...");
	modInst = tester_t<params>::generate_random_i_modInst();
	if (test_num == TestsEnum::NATIVE_NTL64) {
	  modInst->a._limbs[1]&=0x0fffffff; //limit this to 60 bits
	}
	if (test_num == TestsEnum::NATIVE_NTL32) {
	  //modInst->a._limbs[0]&=0x3fffffff; //limit this to 30 bits
	  modInst->a._limbs[0]&=0x3ffffff; //limit this to 30 bits
	}
	
	inInsts = tester_t<params>::generate_random_vv_inInsts(inst_count, modInst);
#ifndef NOGPU
	if (test_num == TestsEnum::NATIVE_NTL64) {	
	  in3Insts = tester_t<params>::generate_ntt_inInsts(inInsts, inst_count, modInst);
	}
#endif
    outInsts = tester_t<params>::generate_zero_v_outInsts(inst_count);
#if 0    
    //debug
    approxInst = tester_t<params>::generate_zero_i_inst();
    clzInst = tester_t<params>::generate_zero_i_inst();
#endif
    // note we do not need to allocate local copies of gApproxInst and gClzInst
    // as they are only used on the gpu
#ifndef NOGPU
    TIC(t);
    TIC(t1);
	
	if (test_num != TestsEnum::NATIVE_NTL64) {
	  CUDA_CHECK(cudaMemcpy(gInInsts, inInsts, sizeof(inst_vv_t)*inst_count,
							cudaMemcpyHostToDevice));
	  
	}else{
	  CUDA_CHECK(cudaMemcpy(gIn3Insts, in3Insts, sizeof(inst_vvv_t)*inst_count,
							cudaMemcpyHostToDevice));
	}
	
	CUDA_CHECK(cudaMemcpy(gModInst, modInst, sizeof(inst_i_t),
						  cudaMemcpyHostToDevice));
    DEBUGEXP(modInst->a._limbs[0]);
	
    if (ix >0){
      t_memcpy_in +=TOC_US(t1);
    }
	
    DEBUG("Running GPU kernel ...");
    TIC(t1);
	
    // launch kernel with blocks=ceil(inst_count/IPB) and threads=TPB
    switch(test_num) {
    case TestsEnum::MONTMUL:
      kernel_montmul<params><<<(inst_count+IPB-1)/IPB, TPB>>>(report,
															  gInInsts,
															  gOutInsts,
															  gModInst,
															  inst_count);
      break;
    case TestsEnum::BARRETTMUL:
      kernel_barrettmul<params><<<(inst_count+IPB-1)/IPB, TPB>>>(report,
																 gInInsts,
							     gOutInsts,
							     gModInst,
							     inst_count);
      break;
    case TestsEnum::REM:
      kernel_rem<params><<<(inst_count+IPB-1)/IPB, TPB>>>(report,
							     gInInsts,
							     gOutInsts,
							     gModInst,
							     inst_count);
      break;
    case TestsEnum::NATIVE_REM32:
      kernel_native_rem32<params><<<(inst_count+IPB-1)/IPB, TPB>>>(report,
							  gInInsts,
							  gOutInsts,
							  gModInst,
							  inst_count);
      break;
    case TestsEnum::NATIVE_REM64:
      kernel_native_rem64<params><<<(inst_count+IPB-1)/IPB, TPB>>>(report,
							  gInInsts,
							  gOutInsts,
							  gModInst,
							  inst_count);
      break;
    case TestsEnum::NATIVE_NTL32:
      kernel_native_NTL32<params><<<(inst_count+IPB-1)/IPB, TPB>>>(report,
							   gInInsts,
							   gOutInsts,
							   gModInst,
							   inst_count);
     break;
    case TestsEnum::NATIVE_NTL64:
      kernel_native_NTL64<params><<<(inst_count+IPB-1)/IPB, TPB>>>(report,
							   gIn3Insts,
							   gOutInsts,
							   gModInst,
							   inst_count);
     break;
    case TestsEnum::FAST_BARRETTMUL:
      //std::cerr<<"init_fast_barrettmul"<<std::endl;
      kernel_init_fast_barrettmul<params><<<1, TPB>>>(report,
						      gModInst,
						     gApproxInst,
						     gClzInst,
						      1);

					  //CUDA_CHECK(cudaDeviceSynchronize());
      kernel_fast_barrettmul<params><<<(inst_count+IPB-1)/IPB, TPB>>>(report,
							     gInInsts,
							     gOutInsts,
							     gModInst,
							     gApproxInst,
							     gClzInst,
							     inst_count);
      //std::cerr<<"done"<<std::endl;
      break;
    default:
      std::cerr<< "Bad test selected"<<std::endl;
    }
    // error report uses managed memory, so we sync the device (or stream) and check for cgbn errors
    CUDA_CHECK(cudaDeviceSynchronize());
    if (ix >0){
      t_run_sync += TOC_US(t1);
    }
    TIC(t1);
    CGBN_CHECK(report);
    if (ix >0){
      t_report += TOC_US(t1);
    }
	
    // copy the instances back from gpuMemory
    DEBUG("Copying results back to CPU ...");
    TIC(t1);
    CUDA_CHECK(cudaMemcpy(outInsts, gOutInsts, sizeof(inst_v_t)*inst_count,
						  cudaMemcpyDeviceToHost));
    if (ix >0){
      t_memcpy_out += TOC_US(t1);    
      t_tot_gpu += TOC_US(t);
    }
	
#endif  
    DEBUG("Verifying the results ...\n");
    float t_cpu_loop(0.0);
    
    if (params::BITS <= 64) {
      t_cpu_loop = tester_t<params>::verify_results_native(inInsts, outInsts,
														   modInst,  inst_count);
	  if (test_num == TestsEnum::NATIVE_NTL32){
		t_cpu_loop = tester_t<params>::verify_results_ntl32(inInsts, outInsts,
														   modInst,  inst_count);
	  }

      verifyType = "native";
    } else  {
      t_cpu_loop = tester_t<params>::verify_results(inInsts, outInsts,
							   modInst, inst_count);
      verifyType = "gmp";
    }
    if (ix>0) {
      t_cpu+=t_cpu_loop;
    }
  }// end n_loop
  
  
  //normalize by number of loops
  t_memcpy_in /= (float)n_loop;
  t_run_sync /= (float)n_loop;
  t_report /= (float)n_loop;
  t_memcpy_out /= (float)n_loop;
  t_tot_gpu  /= (float)n_loop;
  t_cpu /= (float)n_loop;
  
  //std::cout.precision(2);
  std::cout.precision(0);
  std::cout<<std::fixed;
  
#ifndef NOGPU
  PROFILELOG("cudaMemCPYin "<<t_memcpy_in<<" usec "<<100.0*t_memcpy_in/t_tot_gpu<<"%");
  PROFILELOG("run+sync "<<t_run_sync<<" usec "<<100.0*t_run_sync/t_tot_gpu<<"%");  
  PROFILELOG("report   "<<t_report<<" usec "<<100.0*t_report/t_tot_gpu<<"%");  
  PROFILELOG("cudaMemCPYout "<<t_memcpy_out<<" usec "<<100.0*t_memcpy_out/t_tot_gpu<<"%");
  PROFILELOG("gpu time " << t_tot_gpu << " usec 100%");
#endif
  PROFILELOG("cpu "<<verifyType<<" time " << t_cpu << " usec");  
  
  PROFILELOG("=====");
  
  free(inInsts);
  free(outInsts);
  free(modInst);
}

/////////////////////////////////////////////////////////////////////////
// method to teardown the test
template<class params>
void teardown_test(typename tester_t<params>::inst_vv_t * &gInInsts,
				   typename tester_t<params>::inst_vvv_t * &gIn3Insts,
		   typename tester_t<params>::inst_v_t * &gOutInsts,
		   typename tester_t<params>::inst_i_t * &gModInst,
		   typename tester_t<params>::inst_i_t * &gApproxInst,
		   typename tester_t<params>::inst_i_t * &gClzInst,
		   cgbn_error_report_t * &report,
		   bool dbg_flag, bool memstat_flag) {
  TimeVar t, t1;
  #ifndef NOGPU
  float t_free, t_errfree, t_tot_gpu;

  TIC(t);
  TIC(t1);
  CUDA_CHECK(cudaFree(gInInsts));
  CUDA_CHECK(cudaFree(gIn3Insts));
  CUDA_CHECK(cudaFree(gOutInsts));
  CUDA_CHECK(cudaFree(gModInst));
  CUDA_CHECK(cudaFree(gApproxInst));
  CUDA_CHECK(cudaFree(gClzInst));
  t_free=TOC_US(t1);

  TIC(t1);
  CUDA_CHECK(cgbn_error_report_free(report));
  t_errfree=TOC_US(t1);
  t_tot_gpu = TOC_US(t);
  if (memstat_flag){
    std::cout.precision(2);
    PROFILELOG("cudaFree "<<t_free<<" usec "
	       <<100.0*t_free/t_tot_gpu<<"%");
    PROFILELOG("cudaErrfree "<<t_errfree<<" usec "
	       <<100.0*t_errfree/t_tot_gpu<<"%");  
    PROFILELOG("gpu teardown " << t_tot_gpu << " usec 100%");
    PROFILELOG("=====");
    std::cout.precision(0);
  }
  #endif

}


///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
#ifdef LOCALMAIN    //compiled locally with make in same directory
int main(int argc, char* argv[]) 
#else
void  NOTmain(int argc, char* argv[]) //in case this is called by our luxurious Make system
#endif
{
  bool dbg_flag = false; //set to true to turn on DEBUG()
  int opt; //used in getting options
  bool memstat_flag = false; //if true print memory stats. 
  uint32_t n_instances = 1024;
  uint32_t n_loop = 10;
  const uint32_t tpi = NTPI;
  const uint32_t n_bits = NBITS;

  uint32_t rand_seed = 1;
  
  typedef tester_params_t<tpi, n_bits> params;

  
  while ((opt = getopt(argc, argv, "dml:n:r:h")) != -1) {
    switch (opt) {
    case 'd':
      dbg_flag = true;
      std::cout << "setting dbg_flag true" << std::endl;
      break;
    case 'm':
      memstat_flag = true;
      std::cout << "printing memory stats" << std::endl;
      break;
    case 'l':
      n_loop = atoi(optarg);
      if (n_loop <1) {
	n_loop = 1;
      }
      std::cout << "Running "<< n_loop<< " loops" << std::endl;
      break;
    case 'n':
      n_instances = atoi(optarg);
      if (n_instances <1) {
	n_instances = 1;
      }
      std::cout << "Running "<< n_instances<< " instances" << std::endl;
      break;
    case 'r':
      rand_seed = atoi(optarg);
      if (rand_seed <1) {
	rand_seed = 1;
      }
      srand(rand_seed); // seed the random number generator
      std::cout << "seeding RNG with  "<< rand_seed<< std::endl;
      break;
    case 'h':
    default: /* '?' */
      std::cerr<< "Usage: "<<argv[0]<<" <arguments> " <<std::endl
	       << "required arguments:" <<std::endl
	       << "  -l num_loops (10)"  <<std::endl
	       << "  -n num_instances (1024)"  <<std::endl
	       << "optional arguments:"<<std::endl
	       << "  -d  (false) sets debug flag true " <<std::endl
	       << "  -m  (false) sets malloc print timing true " <<std::endl
	       << "  -r random number seed (1)"  <<std::endl
	       << "  -h  prints this message" <<std::endl;
      exit(EXIT_FAILURE);
    }
  }

 
  DEBUG ("Debugging statements on");
  std::cout << "Using "<< n_bits<< " bit arithmetic "
	    << tpi <<" threads per instance"<< std::endl;
  // private pointers to data storage allocated on the GPU
  tester_t<params>::inst_vv_t *gInInsts(NULL);
  tester_t<params>::inst_vvv_t *gIn3Insts(NULL);
  tester_t<params>::inst_v_t *gOutInsts(NULL);
  tester_t<params>::inst_i_t *gModInst(NULL);
  tester_t<params>::inst_i_t *gApproxInst(NULL);
  tester_t<params>::inst_i_t *gClzInst(NULL);

  
  cgbn_error_report_t *report(0);
  
  init_test<params>(gInInsts, gIn3Insts,
					gOutInsts, gModInst,
					gApproxInst, gClzInst,
					report, n_instances,
					dbg_flag, memstat_flag);

  PROFILELOG("------");
  run_test<params>(TestsEnum::BARRETTMUL, n_instances, n_loop,
		   gInInsts, gIn3Insts, gOutInsts, gModInst, gApproxInst, gClzInst,
		   report, dbg_flag);
  PROFILELOG("------");
  if (n_bits > 64) {
    run_test<params>(TestsEnum::FAST_BARRETTMUL, n_instances, n_loop,
		     gInInsts, gIn3Insts, gOutInsts, gModInst, gApproxInst, gClzInst,
		     report, dbg_flag);
    PROFILELOG("------");
  }
  run_test<params>(TestsEnum::REM, n_instances, n_loop,
		   gInInsts, gIn3Insts, gOutInsts, gModInst, gApproxInst, gClzInst,
		   report, dbg_flag);
  PROFILELOG("------");
  if (n_bits == 32) {
    run_test<params>(TestsEnum::NATIVE_REM32, n_instances, n_loop,
		     gInInsts, gIn3Insts, gOutInsts, gModInst, gApproxInst, gClzInst,
		     report, dbg_flag);
    PROFILELOG("------");

    run_test<params>(TestsEnum::NATIVE_NTL32, n_instances, n_loop,
		     gInInsts, gIn3Insts, gOutInsts, gModInst, gApproxInst, gClzInst,
		     report, dbg_flag);
    PROFILELOG("------");
  }
  if (n_bits == 64) {
    run_test<params>(TestsEnum::NATIVE_REM64, n_instances, n_loop,
		     gInInsts, gIn3Insts, gOutInsts, gModInst, gApproxInst, gClzInst,
		     report, dbg_flag);
    PROFILELOG("------");
	
    run_test<params>(TestsEnum::NATIVE_NTL64, n_instances, n_loop,
		     gInInsts, gIn3Insts, gOutInsts, gModInst, gApproxInst, gClzInst,
		     report, dbg_flag);
    PROFILELOG("------");

	
  }
  run_test<params>(TestsEnum::MONTMUL, n_instances, n_loop,
		   gInInsts, gIn3Insts, gOutInsts, gModInst, gApproxInst, gClzInst,
		   report, dbg_flag);
  PROFILELOG("------");

  teardown_test<params>(gInInsts, gIn3Insts, gOutInsts, gModInst, gApproxInst, gClzInst,
			report, dbg_flag, memstat_flag);
}
