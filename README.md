PALISADE Lattice Cryptography Library GPU Experimental Directory
================================================================

This directory contains several experiments


* CGBN: Experiments benchmarking NVIDIA's CGBN (Cuda GPU Big Number)
  This library was an initial look at large number arithmetic on
  GPUs. Howeer with the advent of RNS we do not need to continue to
  use it, even though it does support smaller bitwidths. Note
  that this could contain useful code snippets for modulo
  multiplication on GPU

* CRT-cuda: Experiments benchmarking a native CRT implementation using
  cuda. This code was originally based on FFT-cuda located at
  https://github.com/mmajko/FFT-cuda and written by Marián Hlaváč
  (hlavam30), marian.hlavac@fit.cvut.cz.  Massively re-written for the
  Palisade CRT. It uses code generated from a special program
  src/pke/demo/demo_dcrt_files.cpp for input and output
  comparison. Currently only does forward and inverse CRT for ring
  sizes 8 to 8k in powers of two, for 64 bit only.
  
  
* modmultester: experiments to identify optimum kernel launch
  paramters for all the various versions of modulo multiply.

* setupsupercloud: this script should be sourced when running on the
  MIT supercloud

  