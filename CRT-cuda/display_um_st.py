#!/usr/bin/env python3
#Script to plot outputs of doruns_um_st
# call like this:
#   ./displsy_um_st.py  outdirpath foo
# to plot outdirpath/foo* and outdirpath/fastfoo*

import numpy as np
import sys
import filereaders as fr
import matplotlib.pyplot as plt
import plot_time as plttime

if len(sys.argv) < 3:
    print('Usage: displsy_um_st,py [path] [filenameroot]')
    sys.exit()

file_path = sys.argv[1]
time_data_file = sys.argv[2]
fastest_data_file = "fast"+time_data_file

full_time_data_file = file_path +"/"+ time_data_file
full_fastest_data_file = file_path + "/" + fastest_data_file


#load in the runtimes from the file
colnames, n, n_towers, inverse_flag, \
    ave_time_usec, sd_time_usec, th, bal, nvalid\
    = fr.readUmStTimeFiles(full_time_data_file)

#load in the fastest run time
fcolnames, fn, fn_towers, finverse_flag, \
    fave_time_usec, fsd_time_usec, fth, fbal, fnvalid\
    = fr.readUmStTimeFiles(full_fastest_data_file)

#select some data for plots

plttime.vsThreadBalance(th, bal, n, inverse_flag, ave_time_usec, fth, fbal, fave_time_usec, fsd_time_usec)

plttime.best_balance(th, bal, n, inverse_flag, ave_time_usec, sd_time_usec, fth, fbal, fave_time_usec, fsd_time_usec)

plt.show()
