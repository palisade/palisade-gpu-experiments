#!/usr/bin/env python3
#Script to plot outputs of doruns_omp
# call like this:
#   ./displsy_um_st.py  outdirpath foo
# to plot outdirpath/foo* and outdirpath/fastfoo*

import numpy as np
import sys
import filereaders as fr
import matplotlib.pyplot as plt
import plot_time as plttime

if len(sys.argv) < 3:
    print('Usage: displsy_um_st,py [path] [filenameroot]')
    sys.exit()

file_path = sys.argv[1]
time_data_file = sys.argv[2]
fastest_data_file = "fast"+time_data_file

full_time_data_file = file_path +"/"+ time_data_file
full_fastest_data_file = file_path + "/" + fastest_data_file

splitstring = time_data_file.split('.')
filename_segments = splitstring[0].split('-')
alg_name = filename_segments[1]
width_name = filename_segments[2]
n_name = filename_segments[3]


#load in the runtimes from the file
colnames, n, inverse_flag, \
    ave_time_usec, sd_time_usec, th, nvalid \
    = fr.readOmpTimeFiles(full_time_data_file)

#load in the fastest run time
fcolnames, fn, finverse_flag, \
    fave_time_usec, fsd_time_usec, fth, fnvalid \
    = fr.readOmpTimeFiles(full_fastest_data_file)

#select some data for plots

[fig, ax] = plttime.best_omp(th, n, inverse_flag, ave_time_usec, sd_time_usec, fth, fave_time_usec, fsd_time_usec)

fig.suptitle('Run time for OMP n = '+n_name+' width '+width_name)
ax.set_title('Fastest Run Time th:'+str(fth[0])+
                 " is "+str(fave_time_usec[0]) +'\u00b1'+ '{:.0f}'.format(fsd_time_usec[0]) +' uS' )

figname = 'fig-omp-'+width_name+'-'+n_name+'.png'
ax.set_ylim(bottom= 40,top= 8000 )
#ax.set_ylim(bottom= 40,top= 80 )
#ax.legend()

fig.savefig(figname)
#plt.show()
