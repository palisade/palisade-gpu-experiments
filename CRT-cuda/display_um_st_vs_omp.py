#!/usr/bin/env python3
#Script to plot outputs of doruns_um_stfor all towers. 
# call like this:
#   ./displsy_um_st.py  outdirpath foo
# to plot outdirpath/foo* and outdirpath/fastfoo*

import numpy as np
import sys
import filereaders as fr
import matplotlib.pyplot as plt
import plot_time as plttime

if len(sys.argv) < 3:
    print('Usage: displsy_um_st.py [path] [filenameroot]')
    sys.exit()

file_path = sys.argv[1]
time_data_file = sys.argv[2]

fastest_ave_time_usec =  1000000;

towers = [1,2,4,8]
for tower in towers:

    ## parse out the tower part of time_data_file number just before ".dat"
    splitstring = time_data_file.split('.')
    this_file = splitstring[0].rstrip('0123456789')+ str(tower) + '.'+ splitstring[1]

    filename_segments = splitstring[0].split('-')
    print(filename_segments)

    width_name = filename_segments[3]
    n_name = filename_segments[4]
    fastest_data_file = "fast"+this_file

    full_this_file = file_path +"/"+ this_file
    full_fastest_data_file = file_path + "/" + fastest_data_file

    #load in the runtimes from the file
    colnames, n, n_towers, inverse_flag, \
        ave_time_usec, sd_time_usec, th, bal, nvalid\
        = fr.readUmStTimeFiles(full_this_file)

    #load in the fastest run time
    fcolnames, fn, fn_towers, finverse_flag, \
        fave_time_usec, fsd_time_usec, fth, fbal, fnvalid\
        = fr.readUmStTimeFiles(full_fastest_data_file)

    # pull out the fastest in this tower
    min_ix = ave_time_usec.index(min(ave_time_usec))
    this_fastest = ave_time_usec[min_ix]

    if (this_fastest < fastest_ave_time_usec):
        fastest_ave_time_usec = this_fastest
        fastest_sd_time_usec = sd_time_usec[min_ix]
        fastest_threads = th[min_ix]
        fastest_tower = tower
        fastest_balance = bal[min_ix]
    
    print("tower ",tower, "fastest threads ", fastest_threads,
          "fastest_balance ",fastest_balance, " time ", fastest_ave_time_usec)    
    if (tower ==1):
        [fig, ax]=plttime.best_balance(th, bal, n, inverse_flag, ave_time_usec, sd_time_usec, fth, fbal, fave_time_usec, fsd_time_usec, plotID=tower)
    else:
        plttime.best_balance(th, bal, n, inverse_flag, ave_time_usec, sd_time_usec, fth, fbal, fave_time_usec, fsd_time_usec, fig, ax, plotID=tower)

    fig.suptitle('Run time for DIV n = '+n_name+' width '+width_name)
    ax.set_title('Fastest Run Time tower: '+str(fastest_tower) +' th:'+str(fastest_threads)+
                 " is "+str(fastest_ave_time_usec) +'\u00b1'+ '{:.0f}'.format(fastest_sd_time_usec) +' uS' )


#now  open and plot omp 32 results
filepath_segments = file_path.split('/')

file_path = filepath_segments[0]+'/omp32'

time_data_file_segments = time_data_file.split('-')
time_data_file = time_data_file_segments[0]+'-omp-32-'+time_data_file_segments[4]

fastest_data_file = "fast"+time_data_file

full_time_data_file = file_path +"/"+ time_data_file
full_fastest_data_file = file_path + "/" + fastest_data_file

splitstring = time_data_file.split('.')
filename_segments = splitstring[0].split('-')

print(filename_segments)
alg_name = filename_segments[1]
width_name = filename_segments[2]
n_name = filename_segments[3]

#load in the runtimes from the file
colnames, n, inverse_flag, \
    ave_time_usec, sd_time_usec, th, nvalid \
    = fr.readOmpTimeFiles(full_time_data_file)

#load in the fastest run time
fcolnames, fn, finverse_flag, \
    fave_time_usec, fsd_time_usec, fth, fnvalid \
    = fr.readOmpTimeFiles(full_fastest_data_file)

fastest_omp = fave_time_usec[0]
print('fastest gpu ',fastest_ave_time_usec, ' towers ', fastest_tower);
print("fastest omp ",fastest_omp)

speedup_pct = 100.0 *  fastest_omp/(fastest_ave_time_usec)
#select some data for plots

[fig, ax] = plttime.best_omp(th, n, inverse_flag, ave_time_usec, sd_time_usec, fth, fave_time_usec, fsd_time_usec, fig, ax)

fig.suptitle('Run time comparison n = '+n_name+' width '+width_name)
ax.set_title('GPU speedup '+ '{:.1f}'.format(speedup_pct) +'%' )

figname = 'fig-comp-'+width_name+'-'+n_name+'.png'
#ax.set_ylim(bottom= 40,top= 8000 )
ax.set_ylim(bottom= 100,top= 3000 )
ax.legend()
fig.savefig(figname)
#plt.show()

