// mod_math for crt

#ifndef MOD_MATH_H
#define MOD_MATH_H

#include <bits/stdc++.h> 
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>
#include <cstring>
#include <mod_math.h>

#include <debug.h>

//modulo multiplication using g++ extended precision
uint64_t modmul(uint64_t a, uint64_t b,
		uint64_t mod){
  
  uint64_t result;
  
  unsigned __int128 x, y, m;
  uint64_t computed, correct;
  
  x = a;
  y = b;
  m = mod;
  result = (x*y)%m;      
  return result;
}	  


//modulo multiplication using g++ extended precision
uint32_t modmul32(uint32_t a, uint32_t b,
		uint32_t mod){
  
  uint32_t result;
  uint64_t x, y, m;
  
  x = a;
  y = b;
  m = mod;
  result = (x*y)%m;      
  return result;
}	  

#endif //MOD_MATH_H
