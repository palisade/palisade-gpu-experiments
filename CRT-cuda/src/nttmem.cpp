
#include <nttmem.h>
void print_nttmem(NTTMEM *g){
	std::cout << " cudaStream_t "<< g->SID
			 << " n "<< g->n
			 << " modulus "<< g->modulus
			 << " inverse "<< g->inverse
	  
			 << " fwd_buffer "<< g->fwd_buffer
			 << " inv_buffer "<< g->inv_buffer
			 << " twiddle "<< g->twiddle
			 << " itwiddle "<< g->itwiddle
			 << " result "<< g->result
			 << " dbg "<< g->dbg
			 << std::endl;
}

void print_nttmem_vec(std::vector<NTTMEM> &gvec){
  for (uint twr = 0; twr< gvec.size(); twr++){
	NTTMEM *g = &gvec[twr];
	std::cout<< " tower "<< twr;
	print_nttmem(g);
  }
}

void print_nttmem(NTTMEM32 *g){
	std::cout << " cudaStream_t "<< g->SID
			 << " n "<< g->n
			 << " modulus "<< g->modulus
			 << " inverse "<< g->inverse
	  
			 << " fwd_buffer "<< g->fwd_buffer
			 << " inv_buffer "<< g->inv_buffer
			 << " twiddle "<< g->twiddle
			 << " itwiddle "<< g->itwiddle
			 << " result "<< g->result
			 << " dbg "<< g->dbg
			 << std::endl;
}

void print_nttmem_vec(std::vector<NTTMEM32> &gvec){
  for (uint twr = 0; twr< gvec.size(); twr++){
	NTTMEM32 *g = &gvec[twr];
	std::cout<< " tower "<< twr;
	print_nttmem(g);
  }
}
