// this is an accopen mp version of the Palisade CRT based on the NTT
// 32 bit version
// this code was converted from a published acc-based FFT located at
// https://github.com/mmajko/FFT-cuda
// That was a student project looking focusing on implementing FFTs in CUDA
// his report in czech is located at
// https://github.com/mmajko/FFT-cuda/blob/master/docs/MI-PRC%20Seminar%20Project%20Report.ipynb


// massively recoded for CRT/NTT by Dave Cousins for the PALISADE project .

//#define NO_MOD_FUNC   //if defined it uses inline code otherwise
// it uses forcedinline functions on jetson tx2 there
// was no discernable difference in run time

//#define DEBUG_KERNEL //if defined, save debugging info from kernel

#include <bits/stdc++.h> 
#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>
#include <vector>
#include <cmath>
#include <cstring>
#include <debug.h>
#include <getopt.h>
#include <exception>
#include <file_io.h>
#include <thread>
#include "omp.h"

#include <mod_math_acc.h>

using namespace std;

//struct to save fastest time during trials. 
typedef struct {
  uint64_t n;
  uint64_t nthread;
  double ave_time;
  double sd_time;  
  uint64_t nvalid;
} fastest_struct;

// open-acc version of forward CRT

template<int T>
void CRTfwd(uint32_t* __restrict__ data,
			uint32_t* __restrict__ twiddle,
			uint32_t* __restrict__ result,
			size_t n,
			uint32_t mod) {
  DEBUG_FLAG(false);

  int s = log2(n);

  uint logm(0), logn(s);
  DEBUGEXP(T);
  DEBUGEXP(n);
#pragma acc data copy(data[0:n]) copy(twiddle[0:n]) copyout(result[0:n])
  {
	DEBUGEXP(1);
    // Bit-reversal reordering and premultiply by twiddle 
#pragma omp parallel for num_threads(T)
    for (int i = 0; i <= n-1; i++) {
      int j = i, k = 0;
      
#pragma acc loop seq
      for (int l = 1; l <= s; l++) {
        k = k * 2 + (j & 1);
        j >>= 1;
      }
	  
      result[k]= modmul32(data[i], twiddle[i], mod);
    }
	DEBUGEXP(2);    
#pragma acc for seq
    for (int i = 1; i <= s; i++) {
	  DEBUGEXP(i);
      int m = 1 << i;
      logm = i;
	  
#pragma omp  parallel for num_threads(T)
      for (int j = 0; j < n; j += m) {
        for(int k = 0; k < m/2; k++) {
          if (j + k + m / 2 < n) { 
            uint32_t t, t2, u, omegaFactor;
            
			unsigned int x = k<<(1+logn-logm);
			t= twiddle[x];
            
            size_t ridx = (j + k);
			u = result[ridx];
            
            size_t ridx2 = ridx + m;
			omegaFactor = modmul(t, result[j + k + m / 2], mod);

			//r[j + k] = ModAdd(u, t);
			t2 = u + omegaFactor;
			if (t2 >= mod )
			  t2 -= mod;
			result[j + k] = t2;
			
			//r[j + k + m / 2] = ModSub(u, t);
			if (u < omegaFactor)
			  u += mod;
			u -= omegaFactor;
			result[j + k + m / 2] = u;
          }
		}
        
      }
      
    }
  }
  return ;
}

uint32_t compute_CRT(int nthreads,
					 uint32_t* buffer,
					 uint32_t* twiddle,
					 uint32_t* result,
					 uint32_t modulus,
					 size_t n) {
  DEBUG_FLAG(false);

  uint64_t time = 0;
  
  
  // Start the stopwatch
  auto start = chrono::high_resolution_clock::now();
  
  // Run FFT algorithm with loaded data
  DEBUGEXP(nthreads);
  switch (nthreads) {
  case 1:  CRTfwd<1>(buffer, twiddle, result,n, modulus); break;
  case 2:  CRTfwd<2>(buffer, twiddle, result,n, modulus); break;
  case 4:  CRTfwd<4>(buffer, twiddle, result,n, modulus); break;
  case 8:  CRTfwd<8>(buffer, twiddle, result,n, modulus); break;
  case 16:  CRTfwd<16>(buffer, twiddle, result,n, modulus); break;
  case 32:  CRTfwd<32>(buffer, twiddle, result,n, modulus); break;
  case 64:  CRTfwd<64>(buffer, twiddle, result,n, modulus); break;
  }

  
  // Log the elapsed time
  auto finish = chrono::high_resolution_clock::now();
  auto microseconds = chrono::duration_cast<std::chrono::microseconds>(finish-start);
  
  time += microseconds.count();
  return time;
}


//main program parses input arguments

int main(int argc, char** argv) {
  DEBUG_FLAG(false);

  srand (time(NULL));
  
  // manage the command line args
  int opt; //option from command line parsing

  //default values of inputs
  unsigned int n_repeat(1);
  unsigned int tower_select(0); //this select the tower 
  unsigned int n_selected(8);
  unsigned int nthreads_start(1);
  unsigned int max_threads =  omp_get_max_threads();
  unsigned int nthreads_end(max_threads);
  string folder_select="";
  string out_fname="";

  bool verbose = false;
  bool cache_overwrt = false;

  string usage_string =
    string("run ntt-cuda demo with settings (default value show in parenthesis):\n")+
    string("-s starting # threads (1) power of two\n")+
    string("-e ending # threads ("+to_string(max_threads)+") power of two\n")+
    string("-f folder containing input data\n")+
    string("-c number of executions to average over (1)\n")+
    string("-t tower element selector (0)\n")+
    string("-n ring length (8-1024) powers of two\n")+
	string("-o [output filename]\n")+
    string("-v verbose flag (false)\n")+
    string("-x force cache overwrite on input  (false)\n")+
    string("\nh prints this message\n");
  
  while ((opt = getopt(argc, argv, "s:c:e:f:t:n:o:vxh")) != -1) {
    switch (opt)
      {
      case 's':
        nthreads_start = atoi(optarg);
		break;
      case 'e':
        nthreads_end = atoi(optarg);
		break;
      case 'c':
        n_repeat = atoi(optarg);
		break;
      case 't':
        tower_select = atoi(optarg);
		break;
      case 'f':
        folder_select = optarg;
		if (folder_select.empty()) {
		  cerr << "Please enter a data folder using -f"<<endl;
		  exit(-1);
		}
		break;
      case 'n':
        n_selected = atoi(optarg);
		break;
      case 'o':
        out_fname = optarg;
		break;
      case 'x':
        cache_overwrt = true;
		break;
      case 'v':
        verbose = true;
		break;
		
      case 'h':
      default: /* '?' */
		cout<<usage_string<<endl;
		exit(0);
      }
  }
  if ( nthreads_end < nthreads_start) {
	cerr<< "Error nthreads_start: "<< nthreads_start
		<< " should be smaller than nthreads_end: "<<nthreads_end
		<<endl;
	exit(-1);
  }

  if ( nthreads_start > max_threads) {
	nthreads_start = max_threads;
	cout<< "nthreads_start set to Max # threads:"<< max_threads <<endl;
  }

  if ( nthreads_end > max_threads) {
	nthreads_end = max_threads;
	cout<< "nthreads_end set to Max # threads:"<< max_threads <<endl;
  }

  if (verbose) {
    cout << "===========BENCHMARKING FOR CRT_ACC ===============: " << endl;
  }
  

  // Print out the CSV header
  if (verbose) {
    cout << "folder: "<<folder_select<<" NTT size: "<<n_selected<<endl;
  }

  // storage for inputs, twiddles file
  vector<uint32_t> fwd_buffer(n_selected); //storage for fwd_input
  vector<uint32_t> inv_buffer(n_selected); //storage for fwd_input
  vector<uint32_t> twiddle(n_selected); //storage for twiddle
  vector<uint32_t> itwiddle(n_selected); //storage for twiddle
  
  uint32_t* result; //storage for output
  size_t data_size = n_selected * sizeof(uint32_t);
  result = (uint32_t*)malloc(data_size);
  
  //storage for modulus and inversemodulus
  uint32_t modulus(0);
  uint32_t inverse(0);

  //result filenames for verification
  string fwd_resultfname("");
  string inv_resultfname("");

  // storage for n (size of vectors)
  unsigned int n;

  //scan the selected folder for the run desired, and load all variables from files.
  
  scan_and_load_run32(folder_select, n_selected, tower_select, verbose,
					fwd_buffer, inv_buffer,
					twiddle, itwiddle, result,
					modulus, inverse,
					fwd_resultfname, inv_resultfname, n);

  if (n!=n_selected){
	cerr<<"error in file I/O. Data for n="<<n_selected<<" not found."<<endl;
	exit(-1);
  }

  ofstream out_file;
  ofstream fastest_out_file;
  bool output_flag(false);
  if (out_fname.size()!= 0) {
	output_flag= true;
	out_file.open(out_fname);
	fastest_out_file.open("fast"+out_fname);
	//write out headers
	out_file<<"n, inverse_flag, ave_time_usec, sd_time_usec, th, null, nvalid"<< endl;
	fastest_out_file<<"n, inverse_flag, ave_time_usec, sd_time_usec, th, null, nvalid"<< endl;
  }

  //initialize
  CRTfwd<1>(&fwd_buffer[0], &twiddle[0], result, n, modulus);

  //for (uint64_t loop_count = 0; loop_count<2; loop_count++){
  // only do fwd for now
  for (uint64_t loop_count = 0; loop_count<1; loop_count++){
	//loop through twice first time with inverse_flag false, then true
	bool fail_flag; 
	uint64_t time_usec;
	double accume_time_usec;
	double accume_time_sq_usec;
	double sd_time_usec;
	double ave_time_usec;
	  
	fastest_struct fastest;
	fastest.ave_time = UINT64_MAX;
	fastest.sd_time = 0.0;
		  
	bool inverse_flag = (loop_count == 1UL); 
	try { //big try block to catch bad kernel calls

	  // Run compute for various nthreads
	  for (int nthread = nthreads_start; nthread <= nthreads_end; nthread <<= 1) {

		accume_time_sq_usec = 0.0;
		accume_time_usec = 0.0;
		uint nvalid = 0;
		//CRTsetup(twiddle.data(), itwiddle.data(),
		//	 n, modulus, inverse, g);

		for (int r = 0; r < n_repeat; r++) {
		  if (inverse_flag) {
			if (cache_overwrt){
			  //overwrite the input buffer to flush the cache
			  vector<uint32_t> temp; 
			  for (size_t i = 0; i < n; i++){ //copy buffer
				temp.push_back(inv_buffer[i]);
			  }
			  for (size_t i = 0; i < n; i++){ //copy buffer
				inv_buffer[i] = 0;
			  }
			  for (size_t i = 0; i < n; i++){ //copy buffer
				inv_buffer[i]=temp[i];
			  }
			}
			//time_usec = compute_ICRT(nthread, inv_buffer, itwiddle, result,
			//modulus,n);
			//todo move file io out of compare results. 
			if (verbose) {
			  cout<<"ICRT: n:"<<n<< " tower:"<<tower_select
				  <<" nthreads:"<<nthread<<" "
				  << "time_usec:" << time_usec;
			}
			fail_flag = compare_results32(inv_resultfname.c_str(),
										result, tower_select, modulus, n,
										verbose);
		  }else{
			if (cache_overwrt){
			  //overwrite the input buffer to flush the cache
			  vector<uint32_t> temp; 
			  for (size_t i = 0; i < n; i++){ //copy buffer
				temp.push_back(fwd_buffer[i]);
			  }
			  for (size_t i = 0; i < n; i++){ //copy buffer
				fwd_buffer[i] = 0;
			  }
			  for (size_t i = 0; i < n; i++){ //copy buffer
				fwd_buffer[i] = temp[i];
			  }
			}

			time_usec = compute_CRT(nthread, &fwd_buffer[0], &twiddle[0], result,
									modulus, n);

			if (verbose) {
			  cout<<"CRT: n:"<<n<< " tower:"<<tower_select
				  <<" nthreads:"<<nthread<<" " << "time_usec:" << time_usec;
			}
			fail_flag = compare_results32(fwd_resultfname.c_str(),
										result, tower_select, modulus, n,
										verbose);
			
		  } //inverse flag

		  // compare the computed data			  
		  if (fail_flag){
			if (verbose)  cout <<" NOT verified"<<endl;
		  } else {
			if (verbose)  cout <<" verified"<<endl;
			nvalid++;
			accume_time_usec += (double)time_usec;
			accume_time_sq_usec += (double)time_usec*(double)time_usec;;
		  } //fail_flag
		} //n_repeat;
		  
		//compute statistics
		double variance;
		
		ave_time_usec = accume_time_usec/(double)nvalid;
		variance = accume_time_sq_usec/(double)nvalid -
		  (ave_time_usec*ave_time_usec);
		if (nvalid>1) {
		  // compute Bessel's correction for unbiased estimate
		  variance = ((double)nvalid/(double)(nvalid-1))*variance;
		}
		sd_time_usec = sqrt(variance);
		cout <<"*** n "<<n <<" th:"<<nthread<<(inverse_flag?" ICRT":" CRT")
			 <<" Average time usec: "<<ave_time_usec
			 <<" +-"<< sd_time_usec <<" usec"
			 << " "<<nvalid<<" valid runs"<< endl;
		if (output_flag){
		  out_file
			<< n <<","
			<<inverse_flag<<","
			<<ave_time_usec<<","
			<< sd_time_usec <<","
			<<nthread<<","<<0<<","
			<<nvalid<< endl;
		}
		if (fastest.ave_time > ave_time_usec) { //we have a new fastest configuration
		  fastest.ave_time = ave_time_usec;
		  fastest.sd_time = sd_time_usec;
		  fastest.n = n;
		  fastest.nthread = nthread;
		  fastest.nvalid = nvalid;
		}
	  } //nthread
			
	} catch (const char* msg) {
	  DEBUGEXP(msg);
	  cout << msg<<endl;
	}
	cout<<"### n:"<<fastest.n<<(inverse_flag?" ICRT":" CRT")
		<<" Fastest averaged time usec:"<<fastest.ave_time
		<<" +-"<<fastest.sd_time <<" usec"
		<<" n:"<<fastest.n
		<<" th:"<<fastest.nthread <<" "
		<< fastest.nvalid<<" valid runs"<< endl;

	if (output_flag){
	  fastest_out_file
		<<fastest.n<<","
		<<inverse_flag<<","
		<<fastest.ave_time<<","
		<<fastest.sd_time<<","
		<<fastest.nthread<<","
		<<0<<","
		<<fastest.nvalid<<endl;
	}
  } // loop_count


  if (output_flag){
	out_file.close();
	fastest_out_file.close();
  }
  return 0;
}

