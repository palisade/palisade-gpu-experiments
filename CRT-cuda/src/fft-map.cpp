#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>
#include <vector>
#include <cmath>
#include <dirent.h>
#include <cstring>

using namespace std;

#define N_REPEAT    3

// Complex numbers data type
typedef float Cplx;




/**
 * Runs in-place Iterative Fast Fourier Transformation.
 */
void fft(Cplx* __restrict__ d, size_t n, size_t threads, int balance) {
  size_t data_size = n * sizeof(Cplx);
  Cplx *r, *dn;
  

  // Bit-reversal reordering
  int s = log2(n);
  //bitrev_reorder<<<ceil(n / threads), threads>>>(r, dn, s, threads);
  
  // Synchronize

  cout <<" fft"<<endl;
  // Iterative FFT (with loop paralelism balancing)
  for (int i = 1; i <= s; i++) {
    int m = 1 << i;
	cout<<"i,m = "<<i<<","<<m<<endl;
    if (n/m > balance) {
	  cout << "inplace_fft_outer<<< "<< ceil((float)n / m / threads)
		   <<", "<< threads<< ">>>(r, "<<m<<","<<n<<","<<threads<<")"<<endl;
      //inplace_fft_outer<<<ceil((float)n / m / threads), threads>>>(r, m, n, threads);
	  cout<<"loops over "<<m / 2<<" calls to fft butterfly"<<endl;
    } else {
      for (int j = 0; j < n; j += m) {
        float repeats = m / 2;
		cout << "inplace_fft_butterfly<<< "<< ceil(repeats / threads)
			 <<", "<< threads<< ">>>(r,"<<j<<","<<m<<","<<n<<","<<threads<<")"<<endl;
        //inplace_fft<<<ceil(repeats / threads), threads>>>(r, j, m, n, threads);
      }
    }
  }
  
}


void compute_file(const char* filename, int count, size_t threads, int balance) {
  vector<Cplx> buffer;
  
  // Display active computation
  cout << count << "," << threads <<  "," << balance;
  cout.flush();
  // Run FFT algorithm with loaded data
  fft(&buffer[0], count, threads, balance);
  
  

}

int main(int argc, char** argv) {
  srand (time(NULL));
  
  // Deal with program arguments
  if (argc < 2) {
    cerr << "Usage: " << argv[0] << " nsamp"; return 2;
  }
  int smp = atoi(argv[1]);
  
  int maxthread = smp;
  // Compute for all set parameters

  for (int th = 0; th <= maxthread; th <<= 1) {
  //for (int th = 512; th <= 1024; th <<= 1) {
	if (th == 0) th = 1;
	cout <<"==============="<<endl
		 <<"loop over threads "<<th<<endl;
	for (int bal = 2; bal <= smp / 2; bal <<= 1){
	  const char *fname = "foo";
	  cout <<"  --------------"<<endl
		   <<"  loop over bal "<<bal<<endl;
	  compute_file(fname, smp, th, bal);
	}
  }

  
  return 0;
}
