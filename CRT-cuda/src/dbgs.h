// debugging info for crt

#ifndef DBGS_H
#define DBGS_H

#include <bits/stdc++.h> 
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>
#include <cstring>
#include <debug.h>

typedef struct {
  uint64_t j;
  uint64_t k;
  uint64_t m;
  uint64_t n;
  uint64_t x;
  uint64_t omega;
  uint64_t indexEven;
  uint64_t indexOdd;
  uint64_t inEven;
  uint64_t inOdd;
  uint64_t wlo;
  uint64_t whi;
  uint64_t omegaFactor;
  uint64_t butterflyPlus;
  uint64_t butterflyMinus;
} DBGS;

//forward declarations
std::string to_string_uint128(__int128 n);
void PrintDBGS(DBGS* dbgcpu, int rep, bool dbg_flag, uint64_t modulus);

#endif //DBGS_H
