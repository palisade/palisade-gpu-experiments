#if __has_include ("cuda.h")

// debugging structure block

#include <dbgs.h>

using namespace std;
string to_string_uint128(__int128 n) {
  string ret = "";
  if (n == 0)  return ret;

  char str[40] = {0}; // log10(1 << 128) + '\0'
  char *s = str + sizeof(str) - 1; // start at the end
  while (n != 0) {
    if (s == str) return ret; // never happens

    *--s = "0123456789"[n % 10]; // save last digit
    n /= 10;                     // drop it
  }
  ret = s;
  return ret;
}


void PrintDBGS(DBGS* dbgcpu, int rep, bool dbg_flag, uint64_t modulus) {
  
  DEBUG("block: "<<rep);
			
  DEBUG("  j: "<<dbgcpu[rep].j);
  DEBUG("  k: "<<dbgcpu[rep].k);
  DEBUG("  m: "<<dbgcpu[rep].m);
  DEBUG("  n: "<<dbgcpu[rep].n);
  DEBUG("  x: "<<dbgcpu[rep].x);
			
  DEBUG("  omega: "<<dbgcpu[rep].omega);
  DEBUG("  indexEven: "<<dbgcpu[rep].indexEven);
  DEBUG("  indexOdd: "<<dbgcpu[rep].indexOdd);
  DEBUG("  inOdd: "<<dbgcpu[rep].inOdd);
  DEBUG("  wlo: "<<dbgcpu[rep].wlo);
  DEBUG("  whi: "<<dbgcpu[rep].whi);
  unsigned __int128 x, y, m;
  uint64_t correct;
  m = modulus;
  x = dbgcpu[rep].inOdd;
  y = dbgcpu[rep].omega;
  DEBUG("  x: "<< to_string_uint128(x));
  DEBUG("  y: "<< to_string_uint128(y));
  DEBUG("  m: "<< to_string_uint128(m));
  correct = (x*y)%m;
  DEBUG("  correct OmegaFactor: "<<correct);
  DEBUG("  omegaFactor: "<<dbgcpu[rep].omegaFactor);
  DEBUG("  inEven: "<<dbgcpu[rep].inEven);
  DEBUG("  butterflyPlus: "<<dbgcpu[rep].butterflyPlus);
  DEBUG("  butterflyMinus: "<<dbgcpu[rep].butterflyMinus);
}

  


#endif //__has_include
