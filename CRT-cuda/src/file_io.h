#ifndef FILE_IO_H
#define FILE_IO_H

#include <dirent.h>
#include <nttmem.h>

//forward definions of file io functions. see CPP files for details

void scan_and_load_run(std::string folder_select, unsigned int n_selected, unsigned int tower_select,
					   bool verbose,
					   std::vector<uint64_t> &fwd_buffer, std::vector<uint64_t> &inv_buffer,
					   std::vector<uint64_t> &twiddle, std::vector<uint64_t> &itwiddle, uint64_t* result,
					   uint64_t &modulus, uint64_t &inverse,
					   std::string &fwd_resultfname, std::string &inv_resultfname,  unsigned int &n);
  
void scan_and_load_run32(std::string folder_select, unsigned int n_selected, unsigned int tower_select,
						 bool verbose,
						 std::vector<uint32_t> &fwd_buffer, std::vector<uint32_t> &inv_buffer,
						 std::vector<uint32_t> &twiddle, std::vector<uint32_t> &itwiddle,
						 uint32_t* result,
						 uint32_t &modulus, uint32_t &inverse,
						 std::string &fwd_resultfname,
						 std::string &inv_resultfname,
						 unsigned int &n);
  

//forward definions of file io functions. see CPP files for details

void scan_and_load_all_towers(std::string folder_select,
							  unsigned int n_selected,
							  unsigned int n_towers,
							  bool verbose,
							  std::vector<NTTMEM> &gvec,
							  std::string &fwd_resultfname,
							  std::string &inv_resultfname);
  

void scan_and_load_all_towers32(std::string folder_select,
							  unsigned int n_selected,
							  unsigned int n_towers,
							  bool verbose,
							  std::vector<NTTMEM32> &gvec,
							  std::string &fwd_resultfname,
							  std::string &inv_resultfname);
  
void read_input_file(const char* filename, std::vector<uint64_t>& in, unsigned int tower_select, unsigned int n, uint64_t *modp);

void read_twiddle_ans_file(const char* filename, std::vector<uint64_t>& in, unsigned int n, uint64_t modulus);

void read_inverse_file(const char* filename, uint64_t &inverse, size_t n, uint64_t modulus);

bool compare_results(const char* filename, uint64_t* result, unsigned int tower_select, uint64_t modulus, size_t n, bool verbose);

bool compare_results32(const char* filename, uint32_t* result, unsigned int tower_select, uint32_t modulus, size_t n, bool verbose);


bool compare_all_tower_results(const char* filename, std::vector<NTTMEM> &gvec, bool verbose);
bool compare_all_tower_results32(const char* filename, std::vector<NTTMEM32> &gvec, bool verbose);

#endif //FILE_IO_H
