#ifndef NTT_MEM
#define NTT_MEM
#include <dbgs.h>

typedef struct {
#if __has_include ("cuda.h")
  cudaStream_t SID; //cuda stream
  #else
  uint64_t SID; //dummy
#endif
  int n;						// vector length for this CRT
  uint64_t modulus = 0;			// modulus for this CRT
  uint64_t inverse = 0;			// inverse of cyclotomic order modulo
  uint64_t* fwd_buffer; //storage for fwd_input
  uint64_t* inv_buffer; //storage for inv_input
  uint64_t *twiddle; //cpu
  uint64_t *itwiddle; //cpu
  uint64_t* result; //storage for output
  DBGS *dbg = NULL;
} NTTMEM;

typedef struct {
#if __has_include ("cuda.h")
  cudaStream_t SID; //cuda stream
  #else
  uint64_t SID; //dummy
#endif
  int n;						// vector length for this CRT
  uint32_t modulus = 0;			// modulus for this CRT
  uint32_t inverse = 0;			// inverse of cyclotomic order modulo
  uint32_t* fwd_buffer; //storage for fwd_input
  uint32_t* inv_buffer; //storage for inv_input
  uint32_t *twiddle; //cpu
  uint32_t *itwiddle; //cpu
  uint32_t* result; //storage for output
  DBGS *dbg = NULL;
} NTTMEM32;


void print_nttmem(NTTMEM *gp);
void print_nttmem(NTTMEM32 *gp);
void print_nttmem_vec(std::vector<NTTMEM> &gvec);
void print_nttmem_vec(std::vector<NTTMEM32> &gvec);

#endif //NTT_MEM
