//version 3 unified memory parallel streams for towers.  
// this is a cuda version of the Palisade CRT based on the NTT
// this code was converted from a published cuda based FFT located at
// https://github.com/mmajko/FFT-cuda
// That was a student project looking focusing on implementing FFTs in CUDA
// his report in czech is located at
// https://github.com/mmajko/FFT-cuda/blob/master/docs/MI-PRC%20Seminar%20Project%20Report.ipynb

//TODO: add texture memory
// note tegra has different memory construction than discrete gpu
// see https://docs.nvidia.com/cuda/cuda-for-tegra-appnote/index.html for issues with xavier

// https://docs.nvidia.com/cuda/cuda-c-best-practices-guide/index.html#asynchronous-transfers-and-overlapping-transfers-with-computation

// massively recoded for CRT/NTT by Dave Cousins for the PALISADE project .

//#define NO_MOD_FUNC   //if defined it uses inline code otherwise
// it uses forcedinline functions on jetson tx2 there
// was no discernable difference in run time

//#define DEBUG_KERNEL //if defined, save debugging info from kernel

#if __has_include ("cuda.h") // if not on cuda system 
                             // does not trigger compile errors when made on
                             // non gpu system accidentally

#include <bits/stdc++.h> 
#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>
#include <vector>
#include <cmath>
#include <cstring>
#include <debug.h>
#include <getopt.h>
#include <gpu_support.h>
#include <gpu_properties.h>
#include <exception>
#include <file_io.h>
#include <dbgs.h>
#include <mod_math.h>

using namespace std;

//struct to save fastest time during trials. 
typedef struct {
  uint64_t n;
  uint64_t towers;
  uint64_t th;
  uint64_t bal;
  double ave_time;
  double sd_time;  
  uint64_t nvalid;
} fastest_struct;

typedef struct {
  size_t threads;
  size_t balance;
} GPUwisdom;

#if 0 //this hasn't been used yet
class GPUCRT{
  GPUCRT(){};
  
  void setup(){
  };
  void fwd(){
  };
  void inv(){
  };
  void teardown(){
  };

private:
  
  uint64_t *input; //cpu
  uint64_t *twiddle; //cpu
  uint64_t *itwiddle; //cpu
  size_t n;
  uint64_t modulus;
  uint64_t inverse;
  GPUwisdom wisdom;
};
#endif

//////////////////////////////////
void debug_result(bool dbg_flag, NTTMEM &g){
  //note dbg_flag is used by DEBUG as well
  if (dbg_flag) {
    CUDA_ERR_CHECK(true);
	//prefetch r to cpu and sync
	CUDA_CHECK(cudaStreamAttachMemAsync(g.SID, g.result, 0, cudaMemAttachHost));
    // Synchronize
    CUDA_CHECK(cudaStreamSynchronize(g.SID));

    DEBUG("result : ");
    for (auto ix = 0; ix< g.n; ix++){ DEBUG(g.result[ix]); }
    DEBUG("  done");
  }
}
#if DEBUG_KERNEL
/////////////////
void debug_reps(bool dbg_flag, NTTMEM &g){

  if (dbg_flag) {
	//prefetch dbg to cpu and sync
	CUDA_CHECK(cudaStreamAttachMemAsync(g.SID, g.dbg, 0, cudaMemAttachHost));
	CUDA_CHECK(cudaStreamSynchronize(g.SID));
	for (int rep = 0;rep<(repeats/w.threads); rep++){
	  PrintDBGS(g.dbg, rep, dbg_flag, g.modulus);
	} //for rep
  } // if dbg_flag
  
}
#else
void debug_reps(bool dbg_flag, NTTMEM &g){
  
}
#endif


//////////////////////////////////////////////////////////////////////
// inplace vector vector hadamard multiplication,
// used to premultiply d by twiddle table
// used to postmultiply d by inversetwiddle table
// d in place vector of uint64_t
// twiddle vector of uint64_t to mult by
// n length of vectors
// nthr total # threads
// mod uint64_t modulus
// dbg pointer to array of debug structures. 

__global__ void inplace_vmul(uint64_t* __restrict__ inout,
			     const uint64_t* __restrict__ vec,
			     const int n,
			     const size_t nthr,
			     const uint64_t mod,
			     DBGS *dbg) {
  int id = blockIdx.x * nthr + threadIdx.x;
  if (id < n){
    uint64_t omegaFactor;

#ifdef NO_MOD_FUNC
    uint64_t w_hi(0), w_lo(0), q(0);
    w_lo = inout[id] * vec[id];
    w_hi =  __umul64hi(inout[id], vec[id]);
#ifdef DEBUG_KERNEL
    dbg->wlo = w_lo;
    dbg->whi = w_hi;	
#endif
    divmod128by64(w_hi, w_lo, mod, q, omegaFactor);
#else
    omegaFactor = modmul(inout[id], vec[id], mod);
#endif
#ifdef DEBUG_KERNEL
    dbg->omegaFactor = omegaFactor;
#endif
    inout[id] = omegaFactor;
  }
}

///////////////////////////////////////////////////////
// inplace vector constant multiply
//  vector uint64_t inout by constant uint64_t c
// inout in place vector of uint64_t
// c uint64_t constant to mult by
// n length of vectors
// nthr total # threads
// mod uint64_t modulus
// dbg pointer to array of debug structures. 

__global__ void inplace_cvmul(uint64_t* __restrict__ inout,
			      const uint64_t __restrict__ c,
			      const int n,
			      const size_t nthr,
			      const uint64_t mod,
			      DBGS *dbg) {
  int id = blockIdx.x * nthr + threadIdx.x;
  if (id < n){
    uint64_t omegaFactor;

#ifdef NO_MOD_FUNC
    uint64_t w_hi(0), w_lo(0), q(0);
    w_lo = inout[id] * c;
    w_hi =  __umul64hi(inout[id], c);
#ifdef DEBUG_KERNEL
    dbg->wlo = w_lo;
    dbg->whi = w_hi;	
#endif
    divmod128by64(w_hi, w_lo, mod, q, omegaFactor);
#else
    omegaFactor = modmul(inout[id], c, mod);
#endif
#ifdef DEBUG_KERNEL
    dbg->omegaFactor = omegaFactor;
#endif
    inout[id] = omegaFactor;
  }
}

//////////////////////////////////////////////////////////////////////////////
// bitreverse vector in to vector out
// in vector of uint64_t
// out is output vector
// logn is log2(n)
// n length of vectors
// nthr total # threads
// dbg pointer to array of debug structures. 

__global__ void bitrev_reorder(uint64_t* __restrict__ out,
			       const uint64_t* __restrict__ in,
			       int logn,
			       int n,
			       size_t nthr) {
  int id = blockIdx.x * nthr + threadIdx.x;
  if (id < n)
    {
      out[__brev(id) >> (32 - logn)] = in[id];
    }
  
}

/////////////////////////////////////////////////////////////
// premultiply vector in by vec table vec
// Reorders output into vector out by bit-reversing the indexes.
// combines inplace_vmul<<<>>> and bitrev_reorder<<>> in one call
__global__ void inplace_vmul_bitrev(uint64_t* __restrict__ out,
				    const uint64_t* __restrict__ in,
				    const uint64_t* __restrict__ vec,
				    const int logn,
				    const int n,
				    const size_t nthr,
				    const uint64_t mod,
				    DBGS *dbg) {
  int id = blockIdx.x * nthr + threadIdx.x;
  if (id < n){
    uint64_t omegaFactor;

#ifdef NO_MOD_FUNC
    uint64_t w_hi(0), w_lo(0), q(0);
    w_lo = in[id] * vec[id];
    w_hi =  __umul64hi(in[id], vec[id]);
#ifdef DEBUG_KERNEL
    dbg->wlo = w_lo;
    dbg->whi = w_hi;	
#endif
    divmod128by64(w_hi, w_lo, mod, q, omegaFactor);
#else
    omegaFactor = modmul(in[id], vec[id], mod);
#endif
#ifdef DEBUG_KERNEL
    dbg->omegaFactor = omegaFactor;
#endif
    out[__brev(id) >> (32 - logn)] = omegaFactor;

  }

}


////////////////////////////////////////////
// Innermost  part of NTT double loop. Contains one  butterfly
// called by direclty by inplace_ntt<<<>>>()
// or in a loop in inplace_ntt_outer<<<>>>()

// vector r is the NTT vector partial result
// n is the vector length
// vector t usually contains (inverse)twiddle factors
// j,k,m are loop parameters of the algorithm
// logm, logn are log2 of m and n respectively
// mod uint64_t modulus
// dbg pointer to array of debugging arrays.

__device__ void inplace_ntt_inner(uint64_t* __restrict__ r,
				  const uint64_t* __restrict__ vec,
				  const int j,
				  const int k,
				  const int m, const int logm,
				  const int n, const int logn,
				  const uint64_t mod,
				  DBGS *dbg) {

  if (j + k + m / 2 < n) { 
    uint64_t t, t2, u, omegaFactor;
	
    unsigned int x = k<<(1+logn-logm);
    t= vec[x];

#ifdef DEBUG_KERNEL
    dbg[k].j = j;
    dbg[k].k = k;
    dbg[k].m = m;
    dbg[k].n = n;
    dbg[k].x = x;
    dbg[k].omega = t;
#endif
    u = r[j + k];

#ifdef DEBUG_KERNEL
    dbg[k].indexEven = j+k;
    dbg[k].indexOdd = j + k + m / 2;
    dbg[k].inEven = u;
    dbg[k].inOdd = r[j + k + m / 2];
#endif
    //t = ModMul(t, r[j + k + m / 2]);
#ifdef NO_MOD_FUNC
    uint64_t w_hi(0), w_lo(0);
    w_lo = t* r[j + k + m / 2];
    w_hi =  __umul64hi(t, r[j + k + m / 2]);
#ifdef DEBUG_KERNEL
    dbg[k].wlo = w_lo;
    dbg[k].whi = w_hi;	
#endif
    uint64_t q(0);
    divmod128by64(w_hi, w_lo, mod, q, omegaFactor);
#else
    omegaFactor = modmul(t, r[j + k + m / 2], mod);
#endif
#ifdef DEBUG_KERNEL
    dbg[k].omegaFactor = omegaFactor;
#endif
	
    //r[j + k] = ModAdd(u, t);
    t2 = u + omegaFactor;
    if (t2 >= mod )
      t2 -= mod;
    r[j + k] = t2;
#ifdef DEBUG_KERNEL
    dbg[k].butterflyPlus = t2;
#endif
	  
    //r[j + k + m / 2] = ModSub(u, t);
    if (u < omegaFactor)
      u += mod;
    u -= omegaFactor;
#ifdef DEBUG_KERNEL
    dbg[k].butterflyMinus = u;
#endif
    r[j + k + m / 2] = u;
  }
}

// Kernel call for Inner part of NTT double loop for small scope paralelism.
// calls one butterfly
// similar calling as inplace_ntt_inner except k is computed from grid location

__global__ void inplace_ntt(uint64_t* __restrict__ r,
			    const uint64_t* __restrict__ t,
			    const int j,
			    const int m, const int logm,
			    const int n, const int logn,
			    const size_t nthr,
			    const uint64_t mod,
			    DBGS *dbg) {

  int k = blockIdx.x * nthr + threadIdx.x;
  if (k < m/2) {
    inplace_ntt_inner(r, t, j, k, m, logm, n, logn, mod, dbg);
  }
}

// Kernel call for outer part of NTT double loop 
// for large scope paralelism.
// similar calling as inplace_ntt_inner except j is computed from grid location
// loops over k calling  m/2 butterflies

__global__ void inplace_ntt_outer(uint64_t* __restrict__ r,
				  const uint64_t* __restrict__ t,
				  const int m, const int logm,
				  const int n, const int logn,
				  const size_t nthr,
				  const uint64_t mod,
				  DBGS *dbg) {
  int j = (blockIdx.x * nthr + threadIdx.x) * m;
  
  for (int k = 0; k < m / 2; k++) {
    inplace_ntt_inner(r, t, j, k, m, logm, n, logn,  mod, dbg);
  }
}

//CRT Initializes the  Chinese Remainder Transformation based on in-place
// decimation in time Fermat TT/NTT/FFT
// allocates cuda and cpu memory 
// twiddle vector of uint64_t twiddles forward
// n length of vectors
// modulus: uint64_t modulus for all operations

void CRTsetup(vector<NTTMEM> gvec) {
  for (uint twr = 0; twr< gvec.size(); twr++){
	NTTMEM *gp = &gvec[twr];

#if DEBUG_KERNEL  
	//allocate gpu memory 
	CUDA_CHECK(cudaMallocManaged(&gp->dbg, sizeof(DBGS)*(n/2))); //gpu
#else
	gp->dbg = NULL;
#endif
	//prefetch twiddles and inverse twiddles
	CUDA_CHECK(cudaStreamAttachMemAsync(NULL, gp->twiddle, 0, cudaMemAttachGlobal));
	CUDA_CHECK(cudaStreamAttachMemAsync(NULL, gp->itwiddle, 0, cudaMemAttachGlobal));
  }
}

//CRT performs the forward Chinese Remainder Transformation based on in-place
// decimation in time Fermat TT/NTT/FFT
// fields of NTTMEM
// fwd_buffer: input vector of uint64_t
// result output vector of uint64_t
// twiddle vector of uint64_t twiddles forward
// n length of vectors
// modulus: uint64_t modulus for all operations
// w.threads: power of two # threads to use
// w.balance: power of two: stage at which we shift from single inplace_ntt_outer
//          kernel calls to loop over inplace_ntt_inner kernel calls 

void CRTfwd(vector<NTTMEM>gvec, GPUwisdom &w) {
  
  DEBUG_FLAG(false);
  bool kernel_error(false);
  DEBUGEXP(gvec.size());
  for (auto tix = 0; tix < gvec.size(); tix++){
	DEBUGEXP(tix);
	NTTMEM *gp = &gvec[tix];

	if (dbg_flag) print_nttmem(gp);
	DEBUGEXP(w.threads);
	DEBUGEXP(w.balance);
	// prefetch data to GPU
	CUDA_CHECK(cudaStreamAttachMemAsync(gp->SID, gp->fwd_buffer,
										0, cudaMemAttachGlobal));
  }
  for (auto tix = 0; tix < gvec.size(); tix++){
	DEBUGEXP(tix);
	NTTMEM *gp = &gvec[tix];
	if (gp->n == 00){
	  cout <<"Error, n = 0"<<endl;
	  exit(-1);
	}
	int logn = log2(gp->n);
	if (dbg_flag) {
	  cout <<"input tower "<<tix<<":"<<endl;
	  for (auto ix = 0; ix< gp->n; ix++){
		cout << gp->fwd_buffer[ix]<<endl;		
	  }
	}
	// premultiplication by twiddle and bit reversal
	DEBUG("  premultiply bitrev start");

	inplace_vmul_bitrev<<<ceil(gp->n / w.threads), w.threads, 0, gp->SID>>>(gp->result, gp->fwd_buffer, gp->twiddle, logn, gp->n, w.threads, gp->modulus, gp->dbg);
	debug_result(dbg_flag, *gp);
	//DEBUG("  premultiply bitrev start");

	// Iterative NTT (with loop paralelism balancing)
	uint out_count = 0;
	uint in_count = 0;
	for (int i = 1; i <= logn; i++) {// i is the stage number. 
	  int m = 1 << i;
	  int logm = i;
	  if (gp->n/m >= w.balance) {
		//one kernel call, that then internally calls m/2 butterflies 
		DEBUG("inplace_ntt_outer<<< "<< ceil((float)gp->n / m / w.threads)
			  <<", "<< w.threads<< ">>>"<<"m: "<<m<<" n: "<< gp->n);

		inplace_ntt_outer<<<ceil((float)gp->n / m / w.threads), w.threads, 0, gp->SID>>>
		  (gp->result, gp->twiddle, m, logm, gp->n, logn, w.threads, gp->modulus, gp->dbg);
		//CUDA_ERR_CHECK(dbg_flag);
	  
		out_count++;
	  } else {
		for (int j = 0; j < gp->n; j += m) {
		  // each call represents one block of butterflies that
		  // incrememnt by 1 input stride and repeat by repeats
		  float repeats = m / 2;
		  DEBUG("repeats "<< repeats<< " inplace_ntt<<< "<< ceil(repeats / w.threads)
				<<", "<< w.threads<< ">>>"<<"m: "<<m<<" n: "<<gp->n<<" j:"<<j);
		  inplace_ntt<<<ceil(repeats / w.threads), w.threads, 0, gp->SID>>>
			(gp->result, gp->twiddle, j, m,logm, gp->n,logn, w.threads, gp->modulus, gp->dbg);
		  in_count++;
		  //CUDA_ERR_CHECK(dbg_flag);
		  //debug_reps(false, *gp);

		} //for j
	  
	  } //else w.balance
	}
	//kernel_error |= CUDA_ERR_CHECK(dbg_flag);
	//cout<<" o:"<<out_count<<",i:"<<in_count<<" ";
	
  } //for tix
  DEBUG("done with launches");

  if (kernel_error) {
	cout <<" ERROR "<<endl;
	throw "BAD KERNEL";
  } else {
	for (auto tix = 0; tix < gvec.size(); tix++){
	  DEBUG("attaching to tower "<<tix<<" stream "<< gvec[tix].SID );
	  //prefetch r to cpu and sync
	  CUDA_CHECK(cudaStreamAttachMemAsync(gvec[tix].SID, gvec[tix].result, 0, cudaMemAttachHost));
	}	
  }

  for (auto tix = 0; tix < gvec.size(); tix++){
	DEBUG("synching to tower "<<tix<<" stream "<< gvec[tix].SID );
	// Synchronize
	CUDA_CHECK(cudaStreamSynchronize(gvec[tix].SID));
  }
  DEBUG("returning from CRTfwd()");

}

//CRT tearsdown the forward Chinese Remainder Transformation based on in-place
// decimation in time Fermat TT/NTT/FFT
// deallocates cuda and cpu memory 

void CRTteardown(vector<NTTMEM> gvec) {
  for (auto tix = 0; tix < gvec.size(); tix++){
	NTTMEM* gp = &gvec[tix];
	//free the allocated memory
	CUDA_CHECK(cudaFree(gp->result));
	CUDA_CHECK(cudaFree(gp->twiddle));  
	CUDA_CHECK(cudaFree(gp->itwiddle));  
	CUDA_CHECK(cudaFree(gp->fwd_buffer));
	CUDA_CHECK(cudaFree(gp->inv_buffer));
	
#if DEBUG_KERNEL
	CUDA_CHECK(cudaFree(gp->dbg));
#endif
  }
}

// ICRT Runs Inverse Chinese Remainder Transformation based on in-place
// decimation in time Fermat TT/NTT/FFT
// decimation in time Fermat TT/NTT/FFT
// fields of NTTMEM
// inv_buffer: input vector of uint64_t
// result output vector of uint64_t
// itwiddle vector of uint64_t twiddles inverse
// n length of vectors
// modulus: uint64_t modulus for all operations
// inverse: uint64_t modinverse(n) 
// w.threads: power of two # threads to use
// w.balance: power of two: stage at which we shift from single inplace_ntt_oute
//          kernel calls to loop over inplace_ntt_inner kernel calls 
void CRTinv(vector<NTTMEM> &gvec, GPUwisdom &w) {
			
  DEBUG_FLAG(false);
  bool kernel_error(false);
  for (auto tix = 0; tix < gvec.size(); tix++){
	NTTMEM *gp = &gvec[tix];
	
	// prefetch data to GPU
	CUDA_CHECK(cudaStreamAttachMemAsync(gp->SID, gp->inv_buffer,
										0, cudaMemAttachGlobal));
  }
  for (auto tix = 0; tix < gvec.size(); tix++){
	NTTMEM *gp = &gvec[tix];

  
	//loaded data is in inv_buffer
	// bitreverse d into r
	int logn = log2(gp->n);
	DEBUGEXP(logn);
	bitrev_reorder<<<ceil(gp->n / w.threads), w.threads, 0, gp->SID>>>
	  (gp->result, gp->inv_buffer, logn, gp->n, w.threads);
	//debug_result(dbg_flag, *gp);
  
	// Iterative NTT (with loop paralelism balancing)
	uint out_count = 0;
	uint in_count = 0;
	for (int i = 1; i <= logn; i++) {// i is the stage number. 
	  int m = 1 << i;
	  int logm = i;
	  if (gp->n/m >= w.balance) {
		//one kernel call, that then internally calls m/2 butterflies 
		DEBUG("inplace_ntt_outer<<< "<< ceil((float)gp->n / m / w.threads)
			  <<", "<< w.threads<< ">>>"<<"m: "<<m<<" n: "<<gp->n);
		inplace_ntt_outer<<<ceil((float)gp->n / m / w.threads), w.threads, 0, gp->SID>>>
		  (gp->result, gp->itwiddle, m, logm, gp->n, logn, w.threads, gp->modulus, gp->dbg);
		//CUDA_ERR_CHECK(dbg_flag);
	  
		out_count++;
	  } else {
		for (int j = 0; j < gp->n; j += m) {
		  // each call represents one block of butterflies that
		  // incrememnt by 1 input stride and repeat by repeats
		  float repeats = m / 2;
		  DEBUG("repeats "<< repeats<< " inplace_ntt<<< "<< ceil(repeats / w.threads)
				<<", "<< w.threads<< ">>>"<<"m: "<<m<<" n: "<<gp->n<<" j:"<<j);
		  inplace_ntt<<<ceil(repeats / w.threads), w.threads, 0, gp->SID>>>
			(gp->result, gp->itwiddle, j, m, logm, gp->n, logn, w.threads, gp->modulus, gp->dbg);
		  in_count++;
		  //CUDA_ERR_CHECK(dbg_flag);
		  //debug_reps(dbg_flag, *gp);
		} //for j

	  } //else balance
	}
	//kernel_error |= CUDA_ERR_CHECK(dbg_flag);
	//cout<<" o:"<<out_count<<",i:"<<in_count<<" ";

	//postmultiply by inverse
	inplace_cvmul<<<ceil(gp->n / w.threads), w.threads, 0, gp->SID>>>
	  (gp->result, gp->inverse, gp->n, w.threads, gp->modulus, gp->dbg);
  
	// postmultiplication by twiddle from r to r
	DEBUG("  postmultiply start");
	inplace_vmul<<<ceil(gp->n / w.threads), w.threads, 0, gp->SID>>>
	  (gp->result, gp->itwiddle, gp->n, w.threads, gp->modulus, gp->dbg);
	DEBUG("  postmultiply end");

  } //for tix
  if (kernel_error) {
    cout <<" ERROR "<<endl;
    throw "BAD KERNEL";
  } else {
	for (auto tix = 0; tix < gvec.size(); tix++){

	  //prefetch r to cpu and sync
	  CUDA_CHECK(cudaStreamAttachMemAsync(gvec[tix].SID, gvec[tix].result, 0, cudaMemAttachHost));
	}
  }
  for (auto tix = 0; tix < gvec.size(); tix++){
    // Synchronize
    CUDA_CHECK(cudaStreamSynchronize(gvec[tix].SID));
  }

}


//function to compute and time the CRT
// buffer input vector
// result output vector
// g argument and storage structures,
// w wisdom
// returns execution time in microseconds. 

uint64_t compute_CRT(vector<NTTMEM> &gvec, GPUwisdom &w) {
  // Start the stopwatch
  auto start = chrono::high_resolution_clock::now();
  
  // Run CRT algorithm with loaded data
  CRTfwd(gvec, w);
  
  // Log the elapsed time
  auto finish = chrono::high_resolution_clock::now();
  auto microseconds = chrono::duration_cast<std::chrono::microseconds>(finish-start);
  
  //return the average time for all towers
  return (microseconds.count()/gvec.size());
}

//function to compute and time the ICRT
// buffer input vector
// result output vector
// g argument and storage structures,
// w wisdom
// returns execution time in microseconds. 

uint64_t compute_ICRT(vector<NTTMEM> gvec, GPUwisdom &w) {

  // Start the stopwatch
  auto start = chrono::high_resolution_clock::now();
  
  // Run ICRT algorithm with loaded data
  CRTinv(gvec, w);
  
  // Log the elapsed time
  auto finish = chrono::high_resolution_clock::now();
  auto microseconds = chrono::duration_cast<std::chrono::microseconds>(finish-start);

  //return the average time for all towers
  return (microseconds.count()/gvec.size());
}


void clear_inv_buffers(bool cache_overwrt, vector<NTTMEM> gvec){			  
  if (cache_overwrt){
	//overwrite the input buffer to flush the cache
	for (size_t tix = 0; tix < gvec.size(); tix++){
	  NTTMEM *gp = &gvec[tix];
	  auto n = gp->n;
	  vector<uint64_t> temp;
	  for (size_t i = 0; i < n; i++){ //copy buffer
		temp.push_back(gp->inv_buffer[i]);
	  }
	  for (size_t i = 0; i < n; i++){ //copy buffer
		gp->inv_buffer[i] = 0;
	  }
	  for (size_t i = 0; i < n; i++){ //copy buffer
		gp->inv_buffer[i]=temp[i];
	  }
	}
  }

}  



void clear_fwd_buffers(bool cache_overwrt, vector<NTTMEM> gvec){			  
  if (cache_overwrt){
	//overwrite the input buffer to flush the cache
	for (size_t tix = 0; tix < gvec.size(); tix++){
	  NTTMEM *gp = &gvec[tix];
	  auto n = gp->n;
	  vector<uint64_t> temp;
	  for (size_t i = 0; i < n; i++){ //copy buffer
		temp.push_back(gp->fwd_buffer[i]);
	  }
	  for (size_t i = 0; i < n; i++){ //copy buffer
		gp->fwd_buffer[i] = 0;
	  }
	  for (size_t i = 0; i < n; i++){ //copy buffer
		gp->fwd_buffer[i]=temp[i];
	  }
	}
  }

}  


//main program parses input arguments
  
int main(int argc, char** argv) {
  DEBUG_FLAG(false);
  srand (time(NULL));
  
  // manage the command line args
  int opt; //option from command line parsing

  //default values of inputs
  unsigned int n_repeat(1);
  unsigned int n_towers(1); 
  unsigned int n_selected(8);
  unsigned int n_balance_select(0);
  unsigned int n_threads_select(0);
  string folder_select="";
  string out_fname="";
  bool verbose = false;
  bool cache_overwrt = false;

  string usage_string =
    string("run ntt-cuda demo with settings (default value show in parenthesis):\n")+
    string("-b balance factor (loop over balance) power of two (limited to 1024)\n")+
    string("-c number of executions to average over (1)\n")+
    string("-n ring length (8-1024) powers of two\n")+
	string("-o [output filename]\n")+
    string("-p number of threads (loop over threads) power of two <= 1024 (512 on TX2), if zero loop over 4 to max in powers of two\n")+
    string("-t number of tower elements to do (1)\n")+
    string("-v verbose flag (false)\n")+
    string("-x force cache overwrite on input  (false)\n")+
    string("\nh prints this message\n");
  
  while ((opt = getopt(argc, argv, "b:c:t:f:n:o:p:vxh")) != -1) {
    switch (opt)
      {
      case 'b':
        n_balance_select = atoi(optarg);
	break;
      case 'c':
        n_repeat = atoi(optarg);
	break;
      case 't':
        n_towers = atoi(optarg);
	break;
      case 'f':
        folder_select = optarg;
		if (folder_select.empty()) {
		  cerr << "Please enter a data folder using -f"<<endl;
		  exit(-1);
		}
	break;
      case 'n':
        n_selected = atoi(optarg);
	break;
      case 'o':
        out_fname = optarg;
	break;
      case 'p':
        n_threads_select = atoi(optarg);
	break;
      case 'x':
        cache_overwrt = true;
	break;
      case 'v':
        verbose = true;
	break;
		
      case 'h':
      default: /* '?' */
	cout<<usage_string<<endl;
	exit(0);
      }
  }

  if (verbose) {
    cout << "===========BENCHMARKING FOR CRT_CUDA ===============: " << endl;
  }
  
  // Initialize CUDA
  CUDA_CHECK(cudaSetDeviceFlags(cudaDeviceScheduleSpin));
  CUDA_CHECK(cudaFree(0)); //initializes the cuda context 


  vector<string> gpu_names;
  gpu_names = GPU_Properties();
  unsigned int maxthreadsperblock = 1024;
  if (gpu_names[0] == "NVIDIA Tegra X2"){
    maxthreadsperblock = 512;  //for some reason 1024 fails
  }
  cout<<"---------------------------------------------------"<<endl;
  cout << "Detected "<<gpu_names[0]<<", using max of "<<maxthreadsperblock<<" threads per block"<<endl;

  if (folder_select=="") {
    cerr <<"no folder selected, exiting!"<<endl;
    exit(-1);
  }
  cout<<"---------------------------------------------------"<<endl;
  // Print out a header
  if (verbose) {
    cout << "folder: "<<folder_select<<" NTT size: "<<n_selected<<endl;
  }

  vector<NTTMEM> gvec; //storage for gpu data
  gvec.resize(n_towers); //storage for n towers. 


    
  size_t data_size = n_selected * sizeof(uint64_t);
  for (auto tower_ix=0; tower_ix< n_towers; tower_ix++){
	NTTMEM *gp = &gvec[tower_ix];
	//create and attach the stream for this tower
	CUDA_CHECK(cudaStreamCreate(&gp->SID));
	
	CUDA_CHECK(cudaMallocManaged(&gp->result, data_size)); //gpu
	CUDA_CHECK(cudaMallocManaged(&gp->fwd_buffer, data_size, cudaMemAttachHost)); //cpu
	CUDA_CHECK(cudaMallocManaged(&gp->inv_buffer, data_size, cudaMemAttachHost)); //cpu  
	gp->modulus = 0;
	gp->inverse = 0;
	
	CUDA_CHECK(cudaMallocManaged(&gp->twiddle, data_size)); //gpu
	CUDA_CHECK(cudaMallocManaged(&gp->itwiddle, data_size)); //gpu  

  }
  
  //storage for modulus and inversemodulus
  
  //result filenames for verification
  string fwd_resultfname("");
  string inv_resultfname("");
  
  
  //scan the selected folder for the run desired, and load all variables from files.  
  scan_and_load_all_towers(folder_select, n_selected, n_towers, verbose,
						   gvec,
						   fwd_resultfname, inv_resultfname);


  if (dbg_flag){
	for (auto tix = 0; tix < gvec.size(); tix++){
	  NTTMEM *gp = &gvec[tix];
	  cout <<"forward input tower "<<tix<<":"<<endl;
	  for (auto ix = 0; ix< gp->n; ix++){
		cout << gp->fwd_buffer[ix]<<endl;		
	  }
	}
	for (auto tix = 0; tix < gvec.size(); tix++){
	  NTTMEM *gp = &gvec[tix];
	  cout <<"inverse input tower "<<tix<<":"<<endl;
	  for (auto ix = 0; ix< gp->n; ix++){
		cout << gp->inv_buffer[ix]<<endl;		
	  }
	}

  }
  

  DEBUGEXP(n_selected);
  if (dbg_flag) print_nttmem_vec(gvec);
  
  ofstream out_file;
  ofstream fastest_out_file;
  bool output_flag(false);
  if (out_fname.size()!= 0) {
	output_flag= true;
	out_file.open(out_fname);
	fastest_out_file.open("fast"+out_fname);
	//write out headers
	out_file<<"n, towers, inverse_flag, ave_time_usec, sd_time_usec, th, bal, nvalid"<< endl;
	fastest_out_file<<"n, towers, inverse_flag, ave_time_usec, sd_time_usec, th, bal, nvalid"<< endl;
  }
									  
  for (uint64_t loop_count = 0; loop_count<2; loop_count++){
	//loop through twice first time with inverse_flag false, then true
	bool fail_flag; 
	uint64_t time_usec;
	double accume_time_usec;
	double accume_time_sq_usec;
	double sd_time_usec;
	double ave_time_usec;
	  
	fastest_struct fastest;
	fastest.ave_time = UINT64_MAX;
	fastest.sd_time = 0.0;

	CRTsetup(gvec);

	bool inverse_flag = (loop_count == 1UL); 
	try { //big try block to catch bad kernel calls
	  int maxthreads = min(n_selected, maxthreadsperblock);
	  int start_th;
	  int end_th;
	  if (n_threads_select == 0){
		start_th = 4; //check this should be 1. 
		end_th = maxthreads;
	  }else{
		start_th = n_threads_select;
		end_th = n_threads_select;
	  }
	  GPUwisdom w; 

	  for (int th = start_th; th <= end_th; th <<= 1) {
		//for (int th = 4; th <= 4; th <<= 1) {
		if (th == 0) th = 1;
			  
		int start_bal;
		int end_bal;
		if (n_balance_select == 0){
		  start_bal = 4;  //never found anything better below this 
		  end_bal = n_selected/2;
		}else{
		  start_bal = n_balance_select;
		  end_bal = n_balance_select;
		}
		if (end_bal> 1024) {
		  end_bal = 1024; //never found anything better beyond this.
		}
		for (int bal = start_bal; bal <= end_bal; bal <<= 1) { //best so far
		  //for (int bal = 2; bal <= n / 2; bal <<= 1) { //original
		  //for (int bal = 4; bal <=  4; bal <<= 1) { //specific test
		  w.balance = bal;
		  w.threads = th;

		  accume_time_sq_usec = 0.0;
		  accume_time_usec = 0.0;

		  uint nvalid = 0;
		  for (int r = 0; r < n_repeat; r++) {
			if (inverse_flag) {
			  clear_inv_buffers(cache_overwrt, gvec);
			  time_usec = compute_ICRT(gvec, w);
			  if (verbose) {
				cout<<"ICRT: n:"<<n_selected << " ntowers: "<<n_towers
					<<" th:"<<th<<" bal:"<<bal<<" "
					<< "time_usec:" << time_usec;
			  }
			  //todo move file io out of compare results. 
			  fail_flag = compare_all_tower_results(inv_resultfname.c_str(),
										  gvec, verbose);
			}else{
			  clear_fwd_buffers(cache_overwrt, gvec);
			  time_usec = compute_CRT(gvec, w);
			  if (verbose) {
				cout<<"CRT: n:"<<n_selected << " ntowers: "<<n_towers
					<<" th:"<<th<<" bal:"<<bal<<" "
					<< "time_usec:" << time_usec;
			  }
			  fail_flag = compare_all_tower_results(fwd_resultfname.c_str(),
										  gvec, verbose);
			} //inverse flag

			// compare the computed data			  
			if (fail_flag){
			  if (verbose) cout <<" NOT verified"<<endl;
			} else {
			  if (verbose) cout <<" verified"<<endl;
			  nvalid++;
			  //divide the averages by the number of towers done
			  accume_time_usec += (double)time_usec;
			  //cout << (double)time_usec<<endl;
			  accume_time_sq_usec += (double)time_usec*(double)time_usec;;
			} //fail_flag
		  } //n_repeat;

		  //CRTteardown(g);
		
		  //compute statistics
		  double variance;
		  
		  
		  
		  ave_time_usec = accume_time_usec/(double)nvalid;
		  variance = accume_time_sq_usec/(double)nvalid -
			(ave_time_usec*ave_time_usec);
		  if (nvalid>1) {
			// compute Bessel's correction for unbiased estimate
			variance = ((double)nvalid/(double)(nvalid-1))*variance;
		  }
		  sd_time_usec = sqrt(variance);
			
		  cout <<"*** n: "<<n_selected
			   <<" tow: "<<n_towers
			   <<" th:"<<th
			   <<" bal:"<<bal
			   <<" "<<(inverse_flag?"ICRT":"CRT")
			   <<" Average time usec: "<<ave_time_usec
			   <<" +-"<< sd_time_usec <<" usec"
			   << " "<<nvalid<<" valid runs"<< endl;
		  if (output_flag){
			out_file
			  << n_selected <<","
			  << n_towers <<","
			  <<inverse_flag<<","
			  <<ave_time_usec<<","
			  << sd_time_usec <<","
			  <<th<<","<<bal<<","
			  <<nvalid<< endl;
		  }

		  if (fastest.ave_time > ave_time_usec) { //we have a new fastest configuration
			fastest.ave_time = ave_time_usec;
			fastest.sd_time = sd_time_usec;
			fastest.n = n_selected;
			fastest.towers = n_towers;
			fastest.th = th;
			fastest.bal = bal;
			fastest.nvalid = nvalid;
		  }
		} //balance
	  }//thread
		
	} catch (const char* msg) {
	  cout << msg<<endl;
	}
	cout<<"### n:"<<fastest.n<<" tow: "<<fastest.towers<<(inverse_flag?" ICRT":" CRT")
		<<" Fastest averaged time usec:"<<fastest.ave_time
		<<" +-"<<fastest.sd_time <<" usec"
		<<" th:"<<fastest.th<<" bal:"<<fastest.bal<<" "
		<< fastest.nvalid<<" valid runs"<< endl;

	if (output_flag){
	  fastest_out_file
	  <<fastest.n<<","
	  <<fastest.towers<<","
	  <<inverse_flag<<","
	  <<fastest.ave_time<<","
	  <<fastest.sd_time<<","
	  <<fastest.th<<","
	  <<fastest.bal<<","
	  <<fastest.nvalid<<endl;
	}
  } // loop_count


  if (output_flag){
	out_file.close();
	fastest_out_file.close();
  }
  return 0;
}

#endif //has_cuda
