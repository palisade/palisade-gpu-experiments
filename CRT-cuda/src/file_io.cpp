
//this file gathers together all file io functins for CRT.

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstring>
#include <string>
#include <debug.h>
#include <exception>
#include <file_io.h>

using namespace std;

//////////////////////////////////////////
// scans input directory for desired run files, then loads them
//TODO remove result
void scan_and_load_run(string folder_select, unsigned int n_selected, unsigned int tower_select,
					   bool verbose,
					   vector<uint64_t> &fwd_buffer, vector<uint64_t> &inv_buffer,
					   vector<uint64_t> &twiddle, vector<uint64_t> &itwiddle, uint64_t* result,
					   uint64_t &modulus, uint64_t &inverse,
					   string &fwd_resultfname, string &inv_resultfname,  unsigned int &n){

  DEBUG_FLAG(false);
  
  bool done(false);
  // Read the folder
  DIR* dirp = opendir(folder_select.c_str());
  struct dirent *epdf;

  // Scan over all files in the folder
  while (!done &&
		 ((epdf = readdir(dirp)) != NULL)) {
    size_t len = strlen(epdf->d_name);
    //DEBUG("examining "<<epdf->d_name);
    // Pick only .dat files
    if (!done
		&& (strcmp(epdf->d_name,".") != 0 && strcmp(epdf->d_name,"..") != 0
			&& strcmp(&epdf->d_name[len-3], "dat") == 0)) {
	  
      //look for the coefficient file.
      string input_name("");
      input_name = "coef";
	  
      if (!done &&
		  (strstr(epdf->d_name, input_name.c_str()) != NULL)) {
		//this is a valid coefficient  file, we will run CRT/ICRT  on this data
		stringstream fname(epdf->d_name);
		DEBUG("parsing "<<fname.str());
		
		string nstr;
		DEBUG(nstr);
		
		n = atoi(&epdf->d_name[4]); //parse string after "coef" to get size n
		DEBUG("n= "<<n);
		if(n != n_selected) {
		  continue; //skip to next file
		}
		// Read the 
		DEBUG(" running ");
		

		//build fwd_input file name from directory name and current file name
		string fwd_in_fname = string(folder_select) +"/"+ fname.str();

		
		if (verbose) {
		  cout<<"fwd_input file: "<<fwd_in_fname<<" tower: "<<tower_select<<endl;
		}
		//read in the correct tower and modulus
		read_input_file(fwd_in_fname.c_str(), fwd_buffer, tower_select, n, &modulus);
		//make the inverse input file name (swap "coef" with "eval"
		string inv_in_fname = fwd_in_fname;
		inv_in_fname.replace(inv_in_fname.find(input_name), input_name.length(),"eval");

		if (verbose) {
		  cout<<"inv_input file: "<<inv_in_fname<<" tower: "<<tower_select<<endl;
		}
		
		uint64_t test_modulus(0);
		//read in the correct tower and modulus
		read_input_file(inv_in_fname.c_str(), inv_buffer, tower_select, n, &test_modulus);
		if (modulus != test_modulus) {
		  cerr <<"cannot find eval with correct modulus!"<<endl;
		  cerr <<"need "<< modulus << "found "<< test_modulus<<endl;
		  exit(-1);
		}

		//build twiddle file name from directory name and current file name
		string twiddlefname("");
		string itwiddlefname("");
		
		itwiddlefname = string(folder_select) +"/irou"+to_string(n)+"-"
		  +to_string(modulus)+".dat";

		twiddlefname = string(folder_select) +"/frou"+to_string(n)+"-"
		  +to_string(modulus)+".dat";

		if (verbose) {
		  cout<<"twiddle: "<<twiddlefname<<endl;
		  cout<<"inverse twiddle: "<<itwiddlefname<<endl;
		}
		read_twiddle_ans_file(twiddlefname.c_str(), twiddle, n, modulus);
		read_twiddle_ans_file(itwiddlefname.c_str(), itwiddle, n, modulus);
		
		string inversefname("");

		//look for the inverse file.
		inversefname = string(folder_select) +"/inv"+to_string(n)+"-"
		  +to_string(modulus)+".dat";	
		DEBUGEXP(inversefname);
		if (verbose) {
		  cout<<"inverse file name: "<<inversefname<<endl;
		}
		read_inverse_file(inversefname.c_str(), inverse, n, modulus);
		
		//look for the result in a coefficient or eval file.
		inv_resultfname = string(folder_select) +"/coef"+to_string(n)+".dat";
		fwd_resultfname = string(folder_select) +"/eval"+to_string(n)+".dat";


		DEBUGEXP(fwd_resultfname);
		DEBUGEXP(inv_resultfname);
		if (verbose) {
		  cout<<"fwd result check: "<<fwd_resultfname<<endl;
		  cout<<"inv result check: "<<inv_resultfname<<endl;

		}
		done = true;
      } // look for coef file
    } // pick only *.dat files
  } //while scan over all files in folder
  closedir(dirp);
}


//////////////////////////////////////////
// scans input directory for desired run files, then loads them
//TODO remove result
void scan_and_load_run32(string folder_select,
						 unsigned int n_selected,
						 unsigned int tower_select,
						 bool verbose,
						 vector<uint32_t> &fwd_buffer, vector<uint32_t> &inv_buffer,
						 vector<uint32_t> &twiddle, vector<uint32_t> &itwiddle,
						 uint32_t* result,
						 uint32_t &modulus, uint32_t &inverse,
						 string &fwd_resultfname, string &inv_resultfname,  unsigned int &n){
  
  DEBUG_FLAG(false);
   
  bool done(false);
  // Read the folder
  DIR* dirp = opendir(folder_select.c_str());
  struct dirent *epdf;

  // Scan over all files in the folder
  while (!done &&
		 ((epdf = readdir(dirp)) != NULL)) {
    size_t len = strlen(epdf->d_name);
    //DEBUG("examining "<<epdf->d_name);
    // Pick only .dat files
    if (!done
		&& (strcmp(epdf->d_name,".") != 0 && strcmp(epdf->d_name,"..") != 0
			&& strcmp(&epdf->d_name[len-3], "dat") == 0)) {
	  
      //look for the coefficient file.
      string input_name("");
      input_name = "coef";
	  
      if (!done &&
		  (strstr(epdf->d_name, input_name.c_str()) != NULL)) {
		//this is a valid coefficient  file, we will run CRT/ICRT  on this data
		stringstream fname(epdf->d_name);
		DEBUG("parsing "<<fname.str());
		
		string nstr;
		DEBUG(nstr);
		
		n = atoi(&epdf->d_name[4]); //parse string after "coef" to get size n
		DEBUG("n= "<<n);
		if(n != n_selected) {
		  continue; //skip to next file
		}
		// Read the 
		DEBUG(" running ");
		

		//build fwd_input file name from directory name and current file name
		string fwd_in_fname = string(folder_select) +"/"+ fname.str();

		
		if (verbose) {
		  cout<<"fwd_input file: "<<fwd_in_fname<<" tower: "<<tower_select<<endl;
		}
		//read in the correct tower and modulus

		DEBUG(" 1 ");  
		uint64_t tmp_modulus;
		vector<uint64_t> input_vec; // to read input	
		input_vec.resize(0);
		read_input_file(fwd_in_fname.c_str(), input_vec, tower_select, n, &tmp_modulus);
		modulus = tmp_modulus;
		DEBUG(" 1.5 ");  
		//copy it to the fwd_buffer
		for (size_t i = 0; i < input_vec.size(); i++){ //copy it over
		  fwd_buffer[i]=input_vec[i];
		}
		input_vec.resize(0);

		DEBUG(" 2 ");  		
		//make the inverse input file name (swap "coef" with "eval"
		string inv_in_fname = fwd_in_fname;
		inv_in_fname.replace(inv_in_fname.find(input_name), input_name.length(),"eval");

		if (verbose) {
		  cout<<"inv_input file: "<<inv_in_fname<<" tower: "<<tower_select<<endl;
		}
		
		uint64_t test_modulus(0);
		//read in the correct tower and modulus
		read_input_file(inv_in_fname.c_str(), input_vec, tower_select, n, &test_modulus);
		if (modulus != test_modulus) {
		  cerr <<"cannot find eval with correct modulus!"<<endl;
		  cerr <<"need "<< modulus << "found "<< test_modulus<<endl;
		  exit(-1);
		}
		//copy it to the inv_buffer
		for (size_t i = 0; i < input_vec.size(); i++){ //copy it over
		  inv_buffer[i]=input_vec[i];
		}
		input_vec.resize(0);
		DEBUG(" 3 ");  		
		//build twiddle file name from directory name and current file name
		string twiddlefname("");
		string itwiddlefname("");
		
		itwiddlefname = string(folder_select) +"/irou"+to_string(n)+"-"
		  +to_string(modulus)+".dat";

		twiddlefname = string(folder_select) +"/frou"+to_string(n)+"-"
		  +to_string(modulus)+".dat";

		if (verbose) {
		  cout<<"twiddle: "<<twiddlefname<<endl;
		  cout<<"inverse twiddle: "<<itwiddlefname<<endl;
		}
		read_twiddle_ans_file(twiddlefname.c_str(), input_vec, n, tmp_modulus);
		
		//copy it to the twiddle_buffer
		for (size_t i = 0; i < input_vec.size(); i++){ //copy it over
		  twiddle[i]=input_vec[i];
		}
		input_vec.resize(0);
		read_twiddle_ans_file(itwiddlefname.c_str(), input_vec, n, tmp_modulus);
		//copy it to the itwiddle_buffer
		for (size_t i = 0; i < input_vec.size(); i++){ //copy it over
		  itwiddle[i]=input_vec[i];
		}
		input_vec.resize(0);		
		string inversefname("");

		//look for the inverse file.
		inversefname = string(folder_select) +"/inv"+to_string(n)+"-"
		  +to_string(modulus)+".dat";	
		DEBUGEXP(inversefname);
		if (verbose) {
		  cout<<"inverse file name: "<<inversefname<<endl;
		}
		uint64_t tmp_inverse;
		read_inverse_file(inversefname.c_str(), tmp_inverse, n, tmp_modulus);
		inverse = tmp_inverse;
				
		DEBUG(" 4 ");  		
		//look for the result in a coefficient or eval file.
		inv_resultfname = string(folder_select) +"/coef"+to_string(n)+".dat";
		fwd_resultfname = string(folder_select) +"/eval"+to_string(n)+".dat";


		DEBUGEXP(fwd_resultfname);
		DEBUGEXP(inv_resultfname);
		if (verbose) {
		  cout<<"fwd result check: "<<fwd_resultfname<<endl;
		  cout<<"inv result check: "<<inv_resultfname<<endl;

		}
		done = true;
      } // look for coef file
    } // pick only *.dat files
  } //while scan over all files in folder
  closedir(dirp);
}

//////////////////////////////////////////
// scans input directory for desired run files, then loads them

void scan_and_load_all_towers(string folder_select, 
							  unsigned int n_selected,
							  unsigned int n_towers,
							  bool verbose,
							  vector<NTTMEM> &gvec,
							  std::string &fwd_resultfname,
							  std::string &inv_resultfname
							  )
{
  
  DEBUG_FLAG(false);
  bool done(false);

  // vector storage for reading in input
  vector<uint64_t> input_vec; // to read input	
  uint n; // saves n that is read in
  // Read the folder
  DIR* dirp = opendir(folder_select.c_str());
  struct dirent *epdf;

  // Scan over all files in the folder
  while (!done &&
		 ((epdf = readdir(dirp)) != NULL)) {
    size_t len = strlen(epdf->d_name);
    //DEBUG("examining "<<epdf->d_name);
    // Pick only .dat files
    if (!done
		&& (strcmp(epdf->d_name,".") != 0 && strcmp(epdf->d_name,"..") != 0
			&& strcmp(&epdf->d_name[len-3], "dat") == 0)) {
	  
      //look for the coefficient file.
      string input_name("");
      input_name = "coef";
	  
      if (!done &&
		  (strstr(epdf->d_name, input_name.c_str()) != NULL)) {
		//this is a valid coefficient  file, we will run CRT/ICRT  on this data
		stringstream fname(epdf->d_name);
		DEBUG("parsing "<<fname.str());
		
		string nstr;
		DEBUG(nstr);
		
		n = atoi(&epdf->d_name[4]); //parse string after "coef" to get size n
		DEBUG("n= "<<n);
		if(n != n_selected) {
		  continue; //skip to next file
		}
		// Read the 
		DEBUG(" running ");

		//build fwd_input file name from directory name and current file name
		string fwd_in_fname = string(folder_select) +"/"+ fname.str();

		
		if (verbose) {
		  cout<<"fwd_input file: "<<fwd_in_fname<<endl;
		}

		for (uint twr = 0; twr< n_towers; twr++){
		  //save n
		  gvec[twr].n = n;

		  DEBUG("reading input tower "<<twr);
		  //read in the correct tower and modulus
		  read_input_file(fwd_in_fname.c_str(), input_vec, twr, n,
						  &gvec[twr].modulus);
		  
		  //copy it to the fwd_buffer
		  for (size_t i = 0; i < input_vec.size(); i++){ //copy it over
			gvec[twr].fwd_buffer[i]=input_vec[i];
		  }
		}

		//make the inverse input file name (swap "coef" with "eval"
		string inv_in_fname = fwd_in_fname;
		inv_in_fname.replace(inv_in_fname.find(input_name), input_name.length(),"eval");

		if (verbose) {
		  cout<<"inv_input file: "<<inv_in_fname<<endl;
		}
		
		for (uint twr = 0; twr< n_towers; twr++){
		  DEBUG("reading inverse input tower "<<twr);
		  uint64_t test_modulus(0);

		  //read in the correct tower and modulus
		  read_input_file(inv_in_fname.c_str(), input_vec, twr, n, &test_modulus);
		  if (gvec[twr].modulus != test_modulus) {
			cerr <<"cannot find eval with correct modulus!"<<endl;
			cerr <<"need "<< gvec[twr].modulus << "found "<< test_modulus<<endl;
			exit(-1);
		  }
		  
		  //copy it to the inv_buffer
		  for (size_t i = 0; i < input_vec.size(); i++){ //copy it over
			gvec[twr].inv_buffer[i]=input_vec[i];
		  }

		}

		for (uint twr = 0; twr< n_towers; twr++){

		  //build twiddle file name from directory name and current file name
		  string twiddlefname("");
		  string itwiddlefname("");
		  
		  itwiddlefname = string(folder_select) +"/irou"+to_string(n)+"-"
			+to_string(gvec[twr].modulus)+".dat";
		  
		  twiddlefname = string(folder_select) +"/frou"+to_string(n)+"-"
			+to_string(gvec[twr].modulus)+".dat";
		  
		  if (verbose) {
			cout<<"twiddle: "<<twiddlefname<<endl;
			cout<<"inverse twiddle: "<<itwiddlefname<<endl;
		  }
		  
		  DEBUG("reading twiddle input tower "<<twr);
		  input_vec.resize(0);
		  read_twiddle_ans_file(twiddlefname.c_str(), input_vec, n, gvec[twr].modulus);
		  //copy it to the twiddle_buffer
		  for (size_t i = 0; i < input_vec.size(); i++){ //copy it over
			gvec[twr].twiddle[i]=input_vec[i];
		  }
		  DEBUG("reading itwiddle input tower "<<twr);		  
		  input_vec.resize(0);
		  read_twiddle_ans_file(itwiddlefname.c_str(), input_vec, n, gvec[twr].modulus);
		  //copy it to the inversetwiddle_buffer
		  for (size_t i = 0; i < input_vec.size(); i++){ //copy it over
			gvec[twr].itwiddle[i]=input_vec[i];
		  }
		  DEBUG("done");
		}		

		for (uint twr = 0; twr< n_towers; twr++){

		  string inversefname("");
		  
		  //look for the inverse file.
		  inversefname = string(folder_select) +"/inv"+to_string(n)+"-"
			+to_string(gvec[twr].modulus)+".dat";	
		  DEBUGEXP(inversefname);
		  if (verbose) {
			cout<<"inverse file name: "<<inversefname<<endl;
		  }
		  
		  DEBUG("reading inverse tower "<<twr);
		  read_inverse_file(inversefname.c_str(), gvec[twr].inverse, n, gvec[twr].modulus);
		}		
		//look for the result in a coefficient or eval file.
		inv_resultfname = string(folder_select) +"/coef"+to_string(n)+".dat";
		fwd_resultfname = string(folder_select) +"/eval"+to_string(n)+".dat";

		DEBUGEXP(fwd_resultfname);
		DEBUGEXP(inv_resultfname);
		if (verbose) {
		  cout<<"fwd result check: "<<fwd_resultfname<<endl;
		  cout<<"inv result check: "<<inv_resultfname<<endl;

		}
		done = true;
      } // look for coef file
    } // pick only *.dat files
  } //while scan over all files in folder
  closedir(dirp);
}

//////////////////////////////////////////
// scans input directory for desired run files, then loads them

void scan_and_load_all_towers32(string folder_select, 
							  unsigned int n_selected,
							  unsigned int n_towers,
							  bool verbose,
							  vector<NTTMEM32> &gvec,
							  std::string &fwd_resultfname,
							  std::string &inv_resultfname
							  )
{
  
  DEBUG_FLAG(false);
  bool done(false);

  // vector storage for reading in input
  vector<uint64_t> input_vec; // to read input	
  uint n; // saves n that is read in
  // Read the folder
  DIR* dirp = opendir(folder_select.c_str());
  struct dirent *epdf;

  // Scan over all files in the folder
  while (!done &&
		 ((epdf = readdir(dirp)) != NULL)) {
    size_t len = strlen(epdf->d_name);
    //DEBUG("examining "<<epdf->d_name);
    // Pick only .dat files
    if (!done
		&& (strcmp(epdf->d_name,".") != 0 && strcmp(epdf->d_name,"..") != 0
			&& strcmp(&epdf->d_name[len-3], "dat") == 0)) {
	  
      //look for the coefficient file.
      string input_name("");
      input_name = "coef";
	  
      if (!done &&
		  (strstr(epdf->d_name, input_name.c_str()) != NULL)) {
		//this is a valid coefficient  file, we will run CRT/ICRT  on this data
		stringstream fname(epdf->d_name);
		DEBUG("parsing "<<fname.str());
		
		string nstr;
		DEBUG(nstr);
		
		n = atoi(&epdf->d_name[4]); //parse string after "coef" to get size n
		DEBUG("n= "<<n);
		if(n != n_selected) {
		  continue; //skip to next file
		}
		// Read the 
		DEBUG(" running ");

		//build fwd_input file name from directory name and current file name
		string fwd_in_fname = string(folder_select) +"/"+ fname.str();

		
		if (verbose) {
		  cout<<"fwd_input file: "<<fwd_in_fname<<endl;
		}

		for (uint twr = 0; twr< n_towers; twr++){
		  //save n
		  gvec[twr].n = n;

		  DEBUG("reading input tower "<<twr);
		  //read in the correct tower and modulus
		  uint64_t tmp_modulus;
		  read_input_file(fwd_in_fname.c_str(), input_vec, twr, n,
						  &tmp_modulus);
		  gvec[twr].modulus = tmp_modulus;

		  //copy it to the fwd_buffer
		  for (size_t i = 0; i < input_vec.size(); i++){ //copy it over
			gvec[twr].fwd_buffer[i]=input_vec[i];
		  }
		}

		//make the inverse input file name (swap "coef" with "eval"
		string inv_in_fname = fwd_in_fname;
		inv_in_fname.replace(inv_in_fname.find(input_name), input_name.length(),"eval");

		if (verbose) {
		  cout<<"inv_input file: "<<inv_in_fname<<endl;
		}
		
		for (uint twr = 0; twr< n_towers; twr++){
		  DEBUG("reading inverse input tower "<<twr);
		  uint64_t test_modulus(0);

		  //read in the correct tower and modulus
		  read_input_file(inv_in_fname.c_str(), input_vec, twr, n, &test_modulus);
		  if (gvec[twr].modulus != test_modulus) {
			cerr <<"cannot find eval with correct modulus!"<<endl;
			cerr <<"need "<< gvec[twr].modulus << "found "<< test_modulus<<endl;
			exit(-1);
		  }
		  
		  //copy it to the inv_buffer
		  for (size_t i = 0; i < input_vec.size(); i++){ //copy it over
			gvec[twr].inv_buffer[i]=input_vec[i];
		  }

		}

		for (uint twr = 0; twr< n_towers; twr++){

		  //build twiddle file name from directory name and current file name
		  string twiddlefname("");
		  string itwiddlefname("");
		  
		  itwiddlefname = string(folder_select) +"/irou"+to_string(n)+"-"
			+to_string(gvec[twr].modulus)+".dat";
		  
		  twiddlefname = string(folder_select) +"/frou"+to_string(n)+"-"
			+to_string(gvec[twr].modulus)+".dat";
		  
		  if (verbose) {
			cout<<"twiddle: "<<twiddlefname<<endl;
			cout<<"inverse twiddle: "<<itwiddlefname<<endl;
		  }
		  
		  DEBUG("reading twiddle input tower "<<twr);
		  input_vec.resize(0);

		  uint64_t tmp_modulus = gvec[twr].modulus;
		  read_twiddle_ans_file(twiddlefname.c_str(), input_vec, n, tmp_modulus);
		  //copy it to the twiddle_buffer
		  for (size_t i = 0; i < input_vec.size(); i++){ //copy it over
			gvec[twr].twiddle[i]=input_vec[i];
		  }
		  DEBUG("reading itwiddle input tower "<<twr);		  
		  input_vec.resize(0);
		  read_twiddle_ans_file(itwiddlefname.c_str(), input_vec, n, tmp_modulus);
		  //copy it to the inversetwiddle_buffer
		  for (size_t i = 0; i < input_vec.size(); i++){ //copy it over
			gvec[twr].itwiddle[i]=input_vec[i];
		  }
		  DEBUG("done");
		}		

		for (uint twr = 0; twr< n_towers; twr++){

		  string inversefname("");
		  
		  //look for the inverse file.
		  inversefname = string(folder_select) +"/inv"+to_string(n)+"-"
			+to_string(gvec[twr].modulus)+".dat";	
		  DEBUGEXP(inversefname);
		  if (verbose) {
			cout<<"inverse file name: "<<inversefname<<endl;
		  }
		  
		  DEBUG("reading inverse tower "<<twr);
		  uint64_t tmp_modulus = gvec[twr].modulus;
		  uint64_t tmp_inverse(0);
		  read_inverse_file(inversefname.c_str(), tmp_inverse, n, tmp_modulus);
		  gvec[twr].inverse = tmp_inverse;
		}		
		//look for the result in a coefficient or eval file.
		inv_resultfname = string(folder_select) +"/coef"+to_string(n)+".dat";
		fwd_resultfname = string(folder_select) +"/eval"+to_string(n)+".dat";

		DEBUGEXP(fwd_resultfname);
		DEBUGEXP(inv_resultfname);
		if (verbose) {
		  cout<<"fwd result check: "<<fwd_resultfname<<endl;
		  cout<<"inv result check: "<<inv_resultfname<<endl;

		}
		done = true;
      } // look for coef file
    } // pick only *.dat files
  } //while scan over all files in folder
  closedir(dirp);
}



//////////////////////////////////////////
// Reads input data for CRT from a file.

void read_input_file(const char* filename, vector<uint64_t>& in, unsigned int tower_select, unsigned int n, uint64_t *modp) {
  bool dbg_flag = false;
  ifstream file;
  unsigned int tower;

  //read in the coefficient file, and the twiddle file for the correct modulus
  string word; 
  in.resize(0); //set the vector to empty. 
  file.open(filename);
  if (file.is_open()) {
	DEBUG("reading file "<<filename);

	while (!file.eof()) {
	  // all "words" are whitespace separated.
	  getline(file, word, ' '); //read in the first word (tower number:)
	  DEBUG("top of loop");
	  DEBUGEXP(word);
	  size_t len = word.length(); // remove trailing colon
	  DEBUGEXP(len);
	  word.erase(len-1, len);
	  DEBUGEXP(word);
	  tower = stoul(word);
	  DEBUGEXP(tower);
	  
	  getline(file, word, ' '); //read in the second word (whitespace deliniated)
	  string format(word);
	  DEBUGEXP(format);
	  

	  for (size_t i = 0; i < n; i++) {
		uint64_t val;
		getline(file, word, ' ');
		if (i == 0) {		 //the first word begins with [, remove it
		  word.erase(0,1); 
		  DEBUG("parsing"<< word);
		}
		if (i == n-1) { 	 //the lastt word ends with ], remove it
		  size_t len = word.length();
		  word.erase(len-1, len);
		  DEBUG("parsing "<< word);
		}
		DEBUG("parsing "<< word);
		val = stoul(word);
		DEBUG(" to "<<val);
		if (tower == tower_select){
		  in.push_back(val);
		}
	  }

	  getline(file, word, ' '); //read in the first word (whitespace deliniated)
	  DEBUGEXP(word);
	  getline(file, word); //read in the second word (whitespace deliniated)
	  DEBUGEXP(word);
	  if (tower == tower_select){
		*modp = stoul(word); 
		break;
	  }
    }
  } else {
    cerr << "Can't open file " << filename << " for reading." << endl;
  }
  DEBUG("read in tower# "<<tower<<" "<< in.size()
		<< " samples, modulus "<<*modp);
  file.close();
}

/////////////////////////////////////////////////
// read in the inverse from a file
void read_inverse_file(const char* filename, uint64_t &inverse, size_t n, uint64_t modulus) {
  bool dbg_flag = false;
  ifstream file;

  //read in the inverse file for the correct modulus
  string word; 

  file.open(filename);
  if (file.is_open()) {
	DEBUG("reading inverse file "<<filename);

	// all "words" are whitespace separated.
	getline(file, word, ' '); //read in the first word (modulus:)
	getline(file, word); //read in the second word value of modulus
	DEBUGEXP(word);
	uint64_t input_modulus  = stoul(word);
	DEBUGEXP(input_modulus);
	if (input_modulus != modulus) {
	  cerr << "read bad modulus from file "<<filename<<" aborting!"<<endl;
	  exit(-1);
	}

	getline(file, word, ' '); //read in the word n:
	DEBUGEXP(word);
	getline(file, word); //read in the word value of n
	uint64_t input_n  = stoul(word);
	DEBUGEXP(input_n);
	if (input_n != n) {
	  cerr << "read bad n from file "<<filename<<" aborting!"<<endl;
	  exit(-1);
	}

	getline(file, word, ' '); //read in the next word inverse:
	DEBUGEXP(word);
	uint64_t val;
	getline(file, word); //read in the next word int value

	DEBUG("parsing "<< word);
	val = stoul(word);
	inverse = val;

  } else {
    cerr << "Can't open file " << filename << " for reading." << endl;
	exit(-1);
  }
  DEBUG("read in inverse "<<inverse << ", modulus "<<modulus);
  file.close();
}


/////////////////////////////////////////////////
// read in the twiddles from a file
void read_twiddle_ans_file(const char* filename, vector<uint64_t>& in, unsigned int n, uint64_t modulus) {
  bool dbg_flag = false;
  ifstream file;

  //read in the twiddle file for the correct modulus
  string word; 

  file.open(filename);
  if (file.is_open()) {
	DEBUG("reading twiddle file "<<filename);

	// all "words" are whitespace separated.
	getline(file, word, ' '); //read in the first word (modulus:)
	getline(file, word); //read in the second word value of modulus
	DEBUGEXP(word);
	uint64_t input_modulus  = stoul(word);
	DEBUGEXP(input_modulus);
	if (input_modulus != modulus) {
	  cerr << "read bad modulus from file "<<filename<<" aborting!"<<endl;
	  exit(-1);
	}

	getline(file, word, ' '); //read in the word n:
	DEBUGEXP(word);
	getline(file, word); //read in the word value of n
	uint64_t input_n  = stoul(word);
	DEBUGEXP(input_n);
	if (input_n != n) {
	  cerr << "read bad n from file "<<filename<<" aborting!"<<endl;
	  exit(-1);
	}

	getline(file, word); //read in the next word (rootOfUnityTable) or (answer:)


	for (size_t i = 0; i < n; i++) {
	  uint64_t val;
	  getline(file, word, ' ');
	  if (i == 0) {		 //the first word begins with [, remove it
		word.erase(0,1); 
		DEBUG("parsing "<< word);
	  }
	  if (i == n-1) { 	 //the lastt word ends with ], remove it
		size_t len = word.length();
		word.erase(len-1, len);
		DEBUG("parsing "<< word);
	  }
	  //DEBUG("parsing "<< word);
	  val = stoul(word);
	  //DEBUG(" to "<<val);
	  in.push_back(val);
    }
  } else {
    cerr << "Can't open file " << filename << " for reading." << endl;
	exit(-1);
  }
  DEBUG("read in twiddles "<<in.size() << " samples, modulus "<<modulus);
  file.close();
}

//////////////////////////////////////////////////////////////////////////
// compares the results with data from a result file
//

bool compare_results(const char* filename, uint64_t* result, unsigned int tower_select, uint64_t modulus, size_t n, bool verbose) {
  bool dbg_flag = false;
  //char* outfilename = new char[512];
  vector<uint64_t> answer; 
  vector<uint64_t> resvec;
  for (int i = 0; i< n; i++) {
	resvec.push_back(result[i]);
  }

  // Compose the output filename
  //strcpy(outfilename, filename);
  //strcat(outfilename, ".out");

  //DEBUGEXP(outfilename);
  // Create the file
  //ofstream outfile;
  //outfile.open (outfilename);
  //outfile.precision(4);
  DEBUGEXP(filename);
  
  uint64_t test_modulus;
  read_input_file(filename, answer, tower_select, n, &test_modulus);
  if (test_modulus!= modulus){
	cerr<<"result file bad modulus: "<<test_modulus<<" wanted: "<<modulus<<endl;
	exit(-1);
  }
  
  //std::sort(resvec.begin(), resvec.end());
  //std::sort(answer.begin(), answer.end());
  
  DEBUGEXP(n);
  // compare the data
  //outfile << "frequency, value" << endl;
  bool fail = false;
  for (int i = 0; i < n; i++) {
	if (resvec[i]!=answer[i]) {
	  fail |=true;
	  if (verbose) {
		cout<<"error result["<<i<<"]: "<<resvec[i]<<" != "<<answer[i]<<endl;
	  }
	  //outfile << result[i] << endl;
	}
  }
  //outfile.close();
  return fail;
}

//////////////////////////////////////////////////////////////////////////
// compares the results with data from a result file
//

bool compare_results32(const char* filename, uint32_t* result, unsigned int tower_select, uint32_t modulus, size_t n, bool verbose) {
  bool dbg_flag = false;
  //char* outfilename = new char[512];
  vector<uint64_t> answer; 
  vector<uint64_t> resvec;
  for (int i = 0; i< n; i++) {
	resvec.push_back(result[i]);
  }
  DEBUGEXP(filename);
  
  uint64_t test_modulus;
  read_input_file(filename, answer, tower_select, n, &test_modulus);
  if (test_modulus!= modulus){
	cerr<<"result file bad modulus: "<<test_modulus<<" wanted: "<<modulus<<endl;
	exit(-1);
  }
  
  DEBUGEXP(n);
  // compare the data
  bool fail = false;
  for (int i = 0; i < n; i++) {
	if (resvec[i]!=answer[i]) {
	  fail |=true;
	  if (verbose) {
		cout<<"error result["<<i<<"]: "<<resvec[i]<<" != "<<answer[i]<<endl;
	  }
	}
  }
  return fail;
}


//////////////////////////////////////////////////////////////////////////
// compares the results for all towers with data from a result file
//

bool compare_all_tower_results(const char* filename, std::vector<NTTMEM> &gvec, bool verbose) {

  bool result(false);
  for (auto tix = 0; tix < gvec.size(); tix++){
	auto gp = &gvec[tix];
	
	bool this_result = compare_results(filename, gp->result, tix, gp->modulus, gp->n, verbose);

	if (verbose) {
	  if (this_result) {
		cout << "Verify failure in tower ix "<< tix <<endl;
	  }else{
		cout << "Verify success in tower ix "<< tix <<endl;
	  }
	  result |= this_result;
	}
  }
  return (result);
}
  
bool compare_all_tower_results32(const char* filename, std::vector<NTTMEM32> &gvec, bool verbose) {

  bool result(false);
  for (auto tix = 0; tix < gvec.size(); tix++){
	auto gp = &gvec[tix];
	
	bool this_result = compare_results32(filename, gp->result, tix, gp->modulus, gp->n, verbose);

	if (verbose) {
	  if (this_result) {
		cout << "Verify failure in tower ix "<< tix <<endl;
	  }else{
		cout << "Verify success in tower ix "<< tix <<endl;
	  }
	  result |= this_result;
	}
  }
  return (result);
}
  
