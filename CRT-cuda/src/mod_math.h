// mod_math for crt

#ifndef MOD_MATH_H
#define MOD_MATH_H

#include <bits/stdc++.h> 
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>
#include <cstring>
#include <mod_math.h>

#include <debug.h>

#if 0 //128 bit mul from https://devtalk.nvidia.com/default/topic/642721/multiplication-methods/
//untested but could be a future optimization

    __device__ __forceinline__ 
    ulonglong2 umul64wide (unsigned long long int a, 
                           unsigned long long int b)
    {
        ulonglong2 res;
    #if __CUDA_ARCH__ >= 200
        asm ("{\n\t"
             ".reg .u32 r0, r1, r2, r3, alo, ahi, blo, bhi;\n\t"
             "mov.b64         {alo,ahi}, %2;   \n\t"
             "mov.b64         {blo,bhi}, %3;   \n\t"
             "mul.lo.u32      r0, alo, blo;    \n\t"
             "mul.hi.u32      r1, alo, blo;    \n\t"
             "mad.lo.cc.u32   r1, alo, bhi, r1;\n\t"
             "madc.hi.u32     r2, alo, bhi,  0;\n\t"
             "mad.lo.cc.u32   r1, ahi, blo, r1;\n\t"
             "madc.hi.cc.u32  r2, ahi, blo, r2;\n\t"
             "madc.hi.u32     r3, ahi, bhi,  0;\n\t"
             "mad.lo.cc.u32   r2, ahi, bhi, r2;\n\t"
             "addc.u32        r3, r3,  0;      \n\t"
             "mov.b64         %0, {r0,r1};     \n\t"  
             "mov.b64         %1, {r2,r3};     \n\t"
             "}"
             : "=l"(res.x), "=l"(res.y)
             : "l"(a), "l"(b));
    #else  /* __CUDA_ARCH__ >= 200 */
        res.y = __umul64hi (a, b);
        res.x = a * b;
    #endif /* __CUDA_ARCH__ >= 200 */
        return res;
    }

    __device__ __forceinline__  
    unsigned long long int my_umul64hi (unsigned long long int a, 
                                        unsigned long long int b)
    {
        ulonglong2 t;
        t = umul64wide (a, b);
        return t.y;
    }

    __device__ __forceinline__  
    unsigned long long int my_umul64lo (unsigned long long int a, 
                                        unsigned long long int b)
    {
        ulonglong2 t;
        t = umul64wide (a, b);
        return t.x;
    }
#endif
  // divide and conquer algorithm from /https://www.codeproject.com/Tips/785014/UInt-Division-Modulus
// divde the 128 bit {u1,u0} by v.
// quotient in q, remainder in r


__device__ __forceinline__ void divmod128by64(const uint64_t u1, const uint64_t u0, uint64_t v, uint64_t& q, uint64_t& r) {

  const uint64_t b = 1ll << 32;
  uint64_t un1, un0, vn1, vn0, q1, q0, un32, un21, un10, rhat, left, right;
  size_t s;

  s = __clzll(v); //cuda count leading zeros
  v <<= s;
  vn1 = v >> 32;
  vn0 = v & 0xffffffff;

  if (s > 0)
    {
	  un32 = (u1 << s) | (u0 >> (64 - s));
	  un10 = u0 << s;
    }
  else
    {
	  un32 = u1;
	  un10 = u0;
    }

  un1 = un10 >> 32;
  un0 = un10 & 0xffffffff;

  q1 = un32 / vn1;
  rhat = un32 % vn1;

  left = q1 * vn0;
  right = (rhat << 32) + un1;
 again1:
  if ((q1 >= b) || (left > right))
    {
	  --q1;
	  rhat += vn1;
	  if (rhat < b)
        {
		  left -= vn0;
		  right = (rhat << 32) | un1;
		  goto again1;
        }
    }

  un21 = (un32 << 32) + (un1 - (q1 * v));

  q0 = un21 / vn1;
  rhat = un21 % vn1;

  left = q0 * vn0;
  right = (rhat << 32) | un0;
 again2:
  if ((q0 >= b) || (left > right))
    {
	  --q0;
	  rhat += vn1;
	  if (rhat < b)
        {
		  left -= vn0;
		  right = (rhat << 32) | un0;
		  goto again2;
        }
    }

  r = ((un21 << 32) + (un0 - (q0 * v))) >> s;
  q = (q1 << 32) | q0;
}


//modulo multiplication using division.
__device__ __forceinline__  uint64_t modmul(uint64_t a, uint64_t b,
											uint64_t mod){
  uint64_t result, w_hi(0), w_lo(0), q(0);
  
  w_lo = a * b;
  w_hi =  __umul64hi(a, b);
  divmod128by64(w_hi, w_lo, mod, q, result);
  return result;
}	  


/////////////////////////////////////////////////////////////////
//  modulo multiplication using 32 bit inputs and cuda 64 bit mod
__device__ __forceinline__  uint32_t modmul32(uint32_t a, uint32_t b,
											uint32_t mod){
  uint32_t result;
  uint64_t x,y,m;
  x = a;
  y = b;
  m = mod;
  result = (x*y)%m;
  return result;

}	  

#endif //MOD_MATH_H
