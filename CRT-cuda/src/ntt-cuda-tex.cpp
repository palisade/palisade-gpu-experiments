//version 2 splitting CRT into setup, run , cleanup functions.
// this is a cuda version of the Palisade CRT based on the NTT
// this code was converted from a published cuda based FFT located at
// https://github.com/mmajko/FFT-cuda
// That was a student project looking focusing on implementing FFTs in CUDA
// his report in czech is located at
// https://github.com/mmajko/FFT-cuda/blob/master/docs/MI-PRC%20Seminar%20Project%20Report.ipynb

//TODO: add channel to allow for multi thread operation.
// note tegra has different memory construction than discrete gpu
// see https://docs.nvidia.com/cuda/cuda-for-tegra-appnote/index.html for issues with xavier

//investigating texture memory for twiddles.

// todo: investigate zero copy memory
// https://docs.nvidia.com/cuda/cuda-c-best-practices-guide/index.html#asynchronous-transfers-and-overlapping-transfers-with-computation

// massively recoded for CRT/NTT by Dave Cousins for the PALISADE project .

//#define NO_MOD_FUNC   //if defined it uses inline code otherwise
                        // it uses forcedinline functions on jetson tx2 there
                        // was no discernable difference in run time

//#define DEBUG_KERNEL //if defined, save debugging info from kernel

#if __has_include ("cuda.h") // if not on cuda system 
                             // does not trigger compile errors when made on
                             // non gpu system accidentally

#include <bits/stdc++.h> 
#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>
#include <vector>
#include <cmath>
#include <dirent.h>
#include <cstring>
#include <debug.h>
#include <getopt.h>
#include <gpu_support.h>
#include <gpu_properties.h>
#include <exception>
#include <file_io.h>
#include <dbg_crt.h>
#include <mod_math.h>

using namespace std;

//struct to save fastest time during trials. 
typedef struct {
  uint64_t n;
  uint64_t th;
  uint64_t bal;
  uint64_t time;
} fastest_struct;

typedef struct {
  size_t threads;
  size_t balance;
} GPUwisdom;

typedef struct {
  int n;						// vector length for this CRT
  uint64_t modulus = 0;			// modulus for this CRT
  uint64_t inverse = 0;			// inverse of cyclotomic order modulo
  uint64_t *r = NULL;			// reversed storage
  uint64_t *d = NULL;			// input storage
  uint64_t *t = NULL;			// twiddle storage
  uint64_t *it = NULL;			// inverse twiddle storage
  DBGS *dbg = NULL;
  DBGS *dbgcpu = NULL;  
} CRTGPUmem;

class GPUCRT{
  GPUCRT(){};
  
  void setup(){
  };
  void fwd(){
  };
  void inv(){
  };
  void teardown(){
  };

private:
  
  uint64_t *input; //cpu
  uint64_t *twiddle; //cpu
  uint64_t *itwiddle; //cpu
  size_t n;
  uint64_t modulus;
  uint64_t inverse;
  GPUwisdom wisdom;
};


//////////////////////////////////////////////////////////////////////
// inplace vector vector hadamard multiplication,
// used to premultiply d by twiddle table
// used to postmultiply d by inversetwiddle table
// d in place vector of uint64_t
// twiddle vector of uint64_t to mult by
// n length of vectors
// nthr total # threads
// mod uint64_t modulus
// dbg pointer to array of debug structures. 

__global__ void inplace_vmul(uint64_t* __restrict__ inout,
							 //const uint64_t* __restrict__ vec,
							 cudaTextureObject_t vecObj,
							 const int n,
							 const size_t nthr,
							 const uint64_t mod,
							 DBGS *dbg) {
  int id = blockIdx.x * nthr + threadIdx.x;
  if (id < n){
	uint64_t omegaFactor;

#ifdef NO_MOD_FUNC
	uint64_t w_hi(0), w_lo(0), q(0);
	uint64_t vec = tex1D<uint64_t>(vecObj, id);
    w_lo = inout[id] * vec;
	w_hi =  __umul64hi(inout[id], vec);
#ifdef DEBUG_KERNEL
	dbg->wlo = w_lo;
	dbg->whi = w_hi;	
#endif
	divmod128by64(w_hi, w_lo, mod, q, omegaFactor);
#else
	omegaFactor = modmul(inout[id], vec, mod);
#endif
#ifdef DEBUG_KERNEL
	dbg->omegaFactor = omegaFactor;
#endif
	inout[id] = omegaFactor;
  }
}

///////////////////////////////////////////////////////
// inplace vector constant multiply
//  vector uint64_t inout by constant uint64_t c
// inout in place vector of uint64_t
// c uint64_t constant to mult by
// n length of vectors
// nthr total # threads
// mod uint64_t modulus
// dbg pointer to array of debug structures. 

__global__ void inplace_cvmul(uint64_t* __restrict__ inout,
							  const uint64_t __restrict__ c,
							  const int n,
							  const size_t nthr,
							  const uint64_t mod,
							  DBGS *dbg) {
  int id = blockIdx.x * nthr + threadIdx.x;
  if (id < n){
	uint64_t omegaFactor;

#ifdef NO_MOD_FUNC
	uint64_t w_hi(0), w_lo(0), q(0);
    w_lo = inout[id] * c;
	w_hi =  __umul64hi(inout[id], c);
#ifdef DEBUG_KERNEL
	dbg->wlo = w_lo;
	dbg->whi = w_hi;	
#endif
	divmod128by64(w_hi, w_lo, mod, q, omegaFactor);
#else
	omegaFactor = modmul(inout[id], c, mod);
#endif
#ifdef DEBUG_KERNEL
	dbg->omegaFactor = omegaFactor;
#endif
	inout[id] = omegaFactor;
  }
}

//////////////////////////////////////////////////////////////////////////////
// bitreverse vector in to vector out
// in vector of uint64_t
// out is output vector
// logn is log2(n)
// n length of vectors
// nthr total # threads
// dbg pointer to array of debug structures. 

__global__ void bitrev_reorder(uint64_t* __restrict__ out,
							   const uint64_t* __restrict__ in,
							   int logn,
							   int n,
							   size_t nthr) {
  int id = blockIdx.x * nthr + threadIdx.x;
  if (id < n)
	{
	  out[__brev(id) >> (32 - logn)] = in[id];
	}
  
}

/////////////////////////////////////////////////////////////
// premultiply vector in by vec table vec
// Reorders output into vector out by bit-reversing the indexes.
// combines inplace_vmul<<<>>> and bitrev_reorder<<>> in one call
__global__ void inplace_vmul_bitrev(uint64_t* __restrict__ out,
									const uint64_t* __restrict__ in,
									//const uint64_t* __restrict__ vec,
									cudaTextureObject_t vecObj,
									const int logn,
									const int n,
									const size_t nthr,
									const uint64_t mod,
									DBGS *dbg) {
  int id = blockIdx.x * nthr + threadIdx.x;
  if (id < n){
	uint64_t omegaFactor;

#ifdef NO_MOD_FUNC
	uint64_t w_hi(0), w_lo(0), q(0);
	uint64_t vec = tex1D<uint64_t>(vecObj, id);

    w_lo = in[id] * vec;
	w_hi =  __umul64hi(in[id], vec);
#ifdef DEBUG_KERNEL
	dbg->wlo = w_lo;
	dbg->whi = w_hi;	
#endif
	divmod128by64(w_hi, w_lo, mod, q, omegaFactor);
#else
	omegaFactor = modmul(in[id], vec, mod);
#endif
#ifdef DEBUG_KERNEL
	dbg->omegaFactor = omegaFactor;
#endif
	out[__brev(id) >> (32 - logn)] = omegaFactor;
  }
}


////////////////////////////////////////////
// Innermost  part of NTT double loop. Contains one  butterfly
// called by direclty by inplace_ntt<<<>>>()
// or in a loop in inplace_ntt_outer<<<>>>()

// vector r is the NTT vector partial result
// n is the vector length
// vector t usually contains (inverse)twiddle factors
// j,k,m are loop parameters of the algorithm
// logm, logn are log2 of m and n respectively
// mod uint64_t modulus
// dbg pointer to array of debugging arrays.

__device__ void inplace_ntt_inner(uint64_t* __restrict__ r,
								  //const uint64_t* __restrict__ vec,
								  cudaTextureObject_t vecObj,			  
								  const int j,
								  const int k,
								  const int m, const int logm,
								  const int n, const int logn,
								  const uint64_t mod,
								  DBGS *dbg) {

  if (j + k + m / 2 < n) { 
    uint64_t t, t2, u, omegaFactor;
	
	unsigned int x = k<<(1+logn-logm);

	t = tex1D<uint64_t>(vecObj, x);

#ifdef DEBUG_KERNEL
	dbg[k].j = j;
	dbg[k].k = k;
	dbg[k].m = m;
	dbg[k].n = n;
	dbg[k].x = x;
	dbg[k].omega = t;
#endif
    u = r[j + k];

#ifdef DEBUG_KERNEL
	dbg[k].indexEven = j+k;
	dbg[k].indexOdd = j + k + m / 2;
	dbg[k].inEven = u;
	dbg[k].inOdd = r[j + k + m / 2];
#endif
    //t = ModMul(t, r[j + k + m / 2]);
#ifdef NO_MOD_FUNC
	uint64_t w_hi(0), w_lo(0);
	w_lo = t* r[j + k + m / 2];
    w_hi =  __umul64hi(t, r[j + k + m / 2]);
#ifdef DEBUG_KERNEL
	dbg[k].wlo = w_lo;
	dbg[k].whi = w_hi;	
#endif
	uint64_t q(0);
	divmod128by64(w_hi, w_lo, mod, q, omegaFactor);
#else
	omegaFactor = modmul(t, r[j + k + m / 2], mod);
#endif
#ifdef DEBUG_KERNEL
	dbg[k].omegaFactor = omegaFactor;
#endif
	
    //r[j + k] = ModAdd(u, t);
	t2 = u + omegaFactor;
	if (t2 >= mod )
	  t2 -= mod;
	r[j + k] = t2;
#ifdef DEBUG_KERNEL
	  dbg[k].butterflyPlus = t2;
#endif
	  
    //r[j + k + m / 2] = ModSub(u, t);
	if (u < omegaFactor)
	  u += mod;
	u -= omegaFactor;
#ifdef DEBUG_KERNEL
	  dbg[k].butterflyMinus = u;
#endif
	r[j + k + m / 2] = u;
  }
}

// Kernel call for Inner part of NTT double loop for small scope paralelism.
// calls one butterfly
// similar calling as inplace_ntt_inner except k is computed from grid location

__global__ void inplace_ntt(uint64_t* __restrict__ r,
							const uint64_t* __restrict__ t,
							const int j,
							const int m, const int logm,
							const int n, const int logn,
							const size_t nthr,
							const uint64_t mod,
							DBGS *dbg) {

  int k = blockIdx.x * nthr + threadIdx.x;
  if (k < m/2) {
	inplace_ntt_inner(r, t, j, k, m, logm, n, logn, mod, dbg);
  }
}

// Kernel call for outer part of NTT double loop 
// for large scope paralelism.
// similar calling as inplace_ntt_inner except j is computed from grid location
// loops over k calling  m/2 butterflies

__global__ void inplace_ntt_outer(uint64_t* __restrict__ r,
								  const uint64_t* __restrict__ t,
								  const int m, const int logm,
								  const int n, const int logn,
								  const size_t nthr,
								  const uint64_t mod,
								  DBGS *dbg) {
  int j = (blockIdx.x * nthr + threadIdx.x) * m;
  
  for (int k = 0; k < m / 2; k++) {
    inplace_ntt_inner(r, t, j, k, m, logm, n, logn,  mod, dbg);
  }
}

//CRT Initializes the forward Chinese Remainder Transformation based on in-place
// decimation in time Fermat TT/NTT/FFT
// allocates cuda and cpu memory 
// twiddle vector of uint64_t twiddles forward
// n length of vectors
// modulus: uint64_t modulus for all operations

void CRTsetup(const uint64_t* __restrict__ twiddle,
			  const uint64_t* __restrict__ invtwiddle,
			  const size_t n,
			  const uint64_t modulus,
			  const uint64_t inverse,
			  CRTGPUmem &g) {
  
  bool dbg_flag = false;
  size_t data_size = n * sizeof(uint64_t);

  //debug block
  // DBGS *dbgcpu  __attribute__((unused)); //unused  prevents compiler warnings
                                         // when not debugging the kernel
  //save mod and n in gpu memory structure for unique reference
  g.n = n;
  g.modulus = modulus;
  g.inverse = inverse;

#if DEBUG_KERNEL  
  g.dbgcpu = (DBGS *)malloc(sizeof(DBGS)*(n/2));
#else
  g.dbgcpu = NULL;
#endif
  DEBUGEXP(g.dbgcpu);
  
  // Malloc data store on GPU
#if DEBUG_KERNEL  
  CUDA_CHECK(cudaMalloc((void**)&(g.dbg), sizeof(DBGS)*(n/2)));
#else
  g.dbg = NULL;
#endif

  
  CUDA_CHECK(cudaMalloc((void**)&(g.r), data_size));
  CUDA_CHECK(cudaMalloc((void**)&(g.d), data_size));

  //twiddles get allocated differently
  cudaChannelFormatDesc channelDesc =cudaCreateChannelDesc(64, 0, 0, 0,cudaChannelFormatKindUnsigned);
  
  cudaArray* cuTwiddleArray;
  cudaMallocArray(&cuTwiddleArray, &channelDesc, n, 0);
  
  struct cudaResourceDesc resDesc;
  memset(&resDesc, 0, sizeof(resDesc));
  resDesc.resType = cudaResourceTypeArray;
  resDesc.res.array.array = cuArray;

  STOPPED HERE
	
  
  struct cudaTextureDesc texDesc;
  memset(&texDesc, 0, sizeof(texDesc));
  texDesc.addressMode[0]   = cudaAddressModeWrap;
  texDesc.addressMode[1]   = cudaAddressModeWrap;
  texDesc.filterMode       = cudaFilterModeLinear;
  texDesc.readMode         = cudaReadModeElementType;
  texDesc.normalizedCoords = 1;

        // Create texture object
        cudaTextureObject_t texObj = 0;
        cudaCreateTextureObject(&texObj, &resDesc, &texDesc, NULL);

#if 0  
  CUDA_CHECK(cudaMalloc((void**)&(g.t), data_size));
  CUDA_CHECK(cudaMalloc((void**)&(g.it), data_size));
#endif
  // Copy data to GPU
  CUDA_CHECK(cudaMemcpy(g.t, twiddle, data_size, cudaMemcpyHostToDevice));
  CUDA_CHECK(cudaMemcpy(g.it, invtwiddle, data_size, cudaMemcpyHostToDevice));
}


//CRT performs the forward Chinese Remainder Transformation based on in-place
// decimation in time Fermat TT/NTT/FFT

// data: input vector of uint64_t
// result output vector of uint64_t
// w.threads: power of two # threads to use
// w.balance: power of two: stage at which we shift from single inplace_ntt_outer
//          kernel calls to loop over inplace_ntt_inner kernel calls 
// g.twiddle vector of uint64_t twiddles forward
// g.n length of vectors
// g.modulus: uint64_t modulus for all operations


void CRTfwd(const uint64_t* __restrict__ data,
			uint64_t* __restrict__ result,
			CRTGPUmem &g,
			GPUwisdom &w) {
  
  bool dbg_flag = false;
  int n = g.n;
  size_t data_size = n * sizeof(uint64_t);

  // Copy data to GPU
  CUDA_CHECK(cudaMemcpy(g.d, data, data_size, cudaMemcpyHostToDevice));
  
  int logn = log2(n);
  DEBUGEXP(logn);
#if 0
  // premultiplication by twiddle and bit reversal
  DEBUG("  premultiply bitrev start");
  inplace_vmul_bitrev<<<ceil(n / w.threads), w.threads>>>(g.r, g.d, g.t, logn, n, w.threads, g.modulus, g.dbg);
  if (dbg_flag) {
	// Synchronize
	CUDA_ERR_CHECK(true);
	CUDA_CHECK(cudaDeviceSynchronize());
	CUDA_CHECK(cudaMemcpy(result, g.r, data_size, cudaMemcpyDeviceToHost));
	DEBUG("reversed data : ");
	for (auto ix = 0; ix< n; ix++){ DEBUG(result[ix]); }
	DEBUG("  bitreverse done");
  }
#endif  
  // Iterative NTT (with loop paralelism balancing)
  uint out_count = 0;
  uint in_count = 0;
  for (int i = 1; i <= logn; i++) {// i is the stage number. 
    int m = 1 << i;
	int logm = i;
    if (n/m >= w.balance) {
	  //one kernel call, that then internally calls m/2 butterflies 
	  dbg_flag = false;
	  DEBUG("inplace_ntt_outer<<< "<< ceil((float)n / m / w.threads)
			<<", "<< w.threads<< ">>>"<<"m: "<<m<<" n: "<<n);
	  dbg_flag = false;
      inplace_ntt_outer<<<ceil((float)n / m / w.threads), w.threads>>>(g.r, g.t, m, logm, n, logn, w.threads, g.modulus, g.dbg);
	  
	  CUDA_ERR_CHECK(dbg_flag);
	  
	  out_count++;
    } else {
      for (int j = 0; j < n; j += m) {
		// each call represents one block of butterflies that
		// incrememnt by 1 input stride and repeat by repeats
        float repeats = m / 2;
		dbg_flag = false;
		DEBUG("repeats "<< repeats<< " inplace_ntt<<< "<< ceil(repeats / w.threads)
			  <<", "<< w.threads<< ">>>"<<"m: "<<m<<" n: "<<n<<" j:"<<j);
        inplace_ntt<<<ceil(repeats / w.threads), w.threads>>>(g.r, g.t, j, m,logm, n,logn, w.threads, g.modulus, g.dbg);
		in_count++;
		CUDA_ERR_CHECK(dbg_flag);
		if (dbg_flag) {
#if DEBUG_KERNEL
		  CUDA_CHECK(cudaDeviceSynchronize());
		  CUDA_CHECK(cudaMemcpy(g.dbgcpu, g.dbg, sizeof(DBGS)*(n/2), cudaMemcpyDeviceToHost));
		  for (int rep = 0;rep<(repeats/w.threads); rep++){
			PrintDBGS(g.dbgcpu, rep, dbg_flag, g.modulus);
		  } //for rep
#endif
		} // if dbg_flag
		dbg_flag = false;
	  } //for j

    } //else w.balance
  }
  bool kernel_error = CUDA_ERR_CHECK(dbg_flag);
  cout<<" o:"<<out_count<<",i:"<<in_count<<" ";

  if (kernel_error) {
	cout <<" ERROR "<<endl;
	throw "BAD KERNEL";
  } else {
	// Copy data from GPU & free the memory blocks
	CUDA_CHECK(cudaMemcpy(result, g.r, data_size, cudaMemcpyDeviceToHost));
  }
}


//CRT tearsdown the forward Chinese Remainder Transformation based on in-place
// decimation in time Fermat TT/NTT/FFT
// deallocates cuda and cpu memory 

void CRTteardown(CRTGPUmem &g) {
  
  bool dbg_flag = false;
  DEBUGEXP(g.dbgcpu);
  
  if (g.dbgcpu)
	free(g.dbgcpu);
  
	  
  //free the allocated GPU memory
  CUDA_CHECK(cudaFree(g.r));
  CUDA_CHECK(cudaFree(g.t));  
  CUDA_CHECK(cudaFree(g.it));  
  CUDA_CHECK(cudaFree(g.d));
  
#if DEBUG_KERNEL
  CUDA_CHECK(cudaFree(g.dbg));
#endif

}

// ICRT Runs Inverse Chinese Remainder Transformation based on in-place
// decimation in time Fermat TT/NTT/FFT
// data: input vector of uint64_t
// result output vector of uint64_t
// w.threads: power of two # threads to use
// w.balance: power of two: stage at which we shift from inplace_ntt_outer
//          kernel calls to loop over inplace_ntt_inner kernel calls 
// g.it  vector of uint64_t twiddles inverse
// g.n length of vectors
// g.modulus: uint64_t modulus for all operations
// g.inverse: uint64_t inverse of cyclotomic order modulo the above modulus 
void CRTinv(const uint64_t* __restrict__ data,
			uint64_t* __restrict__ result,
			CRTGPUmem &g,
			GPUwisdom &w) {

  bool dbg_flag = false;
  int n = g.n;
  size_t data_size = n * sizeof(uint64_t);

  // Copy data to GPU
  CUDA_CHECK(cudaMemcpy(g.d, data, data_size, cudaMemcpyHostToDevice));

  //loaded data is in d
  // bitreverse d into r
  int logn = log2(n);
  DEBUGEXP(logn);
  bitrev_reorder<<<ceil(n / w.threads), w.threads>>>(g.r, g.d, logn, g.n, w.threads);
 if (dbg_flag) {
	// Synchronize
	CUDA_ERR_CHECK(true);
	CUDA_CHECK(cudaDeviceSynchronize());
	CUDA_CHECK(cudaMemcpy(result, g.r, data_size, cudaMemcpyDeviceToHost));
	DEBUG("reversed data : ");
	for (auto ix = 0; ix< n; ix++){ DEBUG(result[ix]); }
	DEBUG("  bitreverse done");
  }

  // Iterative NTT (with loop paralelism balancing)
  uint out_count = 0;
  uint in_count = 0;
  for (int i = 1; i <= logn; i++) {// i is the stage number. 
    int m = 1 << i;
	int logm = i;
    if (n/m >= w.balance) {
	  //one kernel call, that then internally calls m/2 butterflies 
	  dbg_flag = false;
	  DEBUG("inplace_ntt_outer<<< "<< ceil((float)n / m / w.threads)
			<<", "<< w.threads<< ">>>"<<"m: "<<m<<" n: "<<n);
	  dbg_flag = false;
      inplace_ntt_outer<<<ceil((float)n / m / w.threads), w.threads>>>(g.r, g.it, m, logm, n, logn, w.threads, g.modulus, g.dbg);
	  
	  CUDA_ERR_CHECK(dbg_flag);
	  
	  out_count++;
    } else {
      for (int j = 0; j < n; j += m) {
		// each call represents one block of butterflies that
		// incrememnt by 1 input stride and repeat by repeats
        float repeats = m / 2;
		dbg_flag = false;
		DEBUG("repeats "<< repeats<< " inplace_ntt<<< "<< ceil(repeats / w.threads)
			  <<", "<< w.threads<< ">>>"<<"m: "<<m<<" n: "<<n<<" j:"<<j);
        inplace_ntt<<<ceil(repeats / w.threads), w.threads>>>(g.r, g.it, j, m, logm, n, logn, w.threads, g.modulus, g.dbg);
		in_count++;
		CUDA_ERR_CHECK(dbg_flag);
		if (dbg_flag) {
#if DEBUG_KERNEL
		  CUDA_CHECK(cudaDeviceSynchronize());
		  CUDA_CHECK(cudaMemcpy(g.dbgcpu, g.dbg, sizeof(DBGS)*(n/2), cudaMemcpyDeviceToHost));
		  for (int rep = 0;rep<(repeats/w.threads); rep++){
			PrintDBGS(g.dbgcpu, rep, dbg_flag, g.modulus);
		  } //for rep
#endif
		} // if dbg_flag
		dbg_flag = false;
	  } //for j

    } //else balance
  }
  bool kernel_error = CUDA_ERR_CHECK(dbg_flag);
  cout<<" o:"<<out_count<<",i:"<<in_count<<" ";

  //postmultiply by inverse
  inplace_cvmul<<<ceil(n / w.threads), w.threads>>>(g.r, g.inverse, g.n, w.threads, g.modulus, g.dbg);
  // postmultiplication by twiddle from r to r
  DEBUG("  postmultiply start");
  inplace_vmul<<<ceil(n / w.threads), w.threads>>>(g.r, g.it, g.n, w.threads, g.modulus, g.dbg);
  DEBUG("  postmultiply end");
  
  if (kernel_error) {
	cout <<" ERROR "<<endl;
	throw "BAD KERNEL";
  } else {
	// Copy data from GPU & free the memory blocks
	CUDA_CHECK(cudaMemcpy(result, g.r, data_size, cudaMemcpyDeviceToHost));
  }
}


//function to compute and time the CRT
// buffer input vector
// result output vector
// g argument and storage structures,
// w wisdom
// returns execution time in microseconds. 

uint64_t compute_CRT(vector<uint64_t>& buffer,
					 uint64_t *result,
					 CRTGPUmem &g,
					 GPUwisdom &w) {
  // Start the stopwatch
  auto start = chrono::high_resolution_clock::now();
  
  // Run CRT algorithm with loaded data
  CRTfwd(&buffer[0], result, g,w);
  
  // Log the elapsed time
  auto finish = chrono::high_resolution_clock::now();
  auto microseconds = chrono::duration_cast<std::chrono::microseconds>(finish-start);

  return microseconds.count();
}

//function to compute and time the ICRT
// buffer input vector
// result output vector
// g argument and storage structures,
// w wisdom
// returns execution time in microseconds. 

uint64_t compute_ICRT(vector<uint64_t>& buffer, 
					  uint64_t *result, 
					 CRTGPUmem &g,
					 GPUwisdom &w) {

  // Start the stopwatch
  auto start = chrono::high_resolution_clock::now();
  
  // Run ICRT algorithm with loaded data
  CRTinv(&buffer[0], result, g, w);
  
  // Log the elapsed time
  auto finish = chrono::high_resolution_clock::now();
  auto microseconds = chrono::duration_cast<std::chrono::microseconds>(finish-start);

  return microseconds.count();
}

//main program parses input arguments

int main(int argc, char** argv) {
  srand (time(NULL));
  bool dbg_flag = false;
  
  // manage the command line args
  int opt; //option from command line parsing

  //default values of inputs
  unsigned int n_repeat(1);
  unsigned int tower_select(0); //this select the tower 
  unsigned int n_selected(8);
  unsigned int n_balance_select(0);
  unsigned int n_threads_select(0);
  string folder_select="";
  bool verbose = false;

  string usage_string =
    string("run ntt-cuda demo with settings (default value show in parenthesis):\n")+
    string("-b balance factor (loop over balance) power of two\n")+
    string("-c number of executions to average over (1)\n")+
    string("-e tower element selector (0)\n")+
    string("-n ring length (8-1024) powers of two\n")+
    string("-t number of threads (loop over threads) power of two <= 1024 (512 on TX2)\n")+
	string("-v verbose flag (false)\n")+
    string("\nh prints this message\n");
  
  while ((opt = getopt(argc, argv, "b:c:e:f:n:t:vh")) != -1) {
    switch (opt)
      {
      case 'b':
        n_balance_select = atoi(optarg);
		break;
      case 'c':
        n_repeat = atoi(optarg);
		break;
      case 'e':
        tower_select = atoi(optarg);
		break;
      case 'f':
        folder_select = optarg;
		break;
      case 'n':
        n_selected = atoi(optarg);
		break;
      case 't':
        n_threads_select = atoi(optarg);
		break;
      case 'v':
        verbose = true;
		break;
		
      case 'h':
      default: /* '?' */
		cout<<usage_string<<endl;
		exit(0);
      }
  }

  if (verbose) {
	cout << "===========BENCHMARKING FOR CRT_CUDA ===============: " << endl;
  }
  
  // Initialize CUDA
  CUDA_CHECK(cudaFree(0));

  vector<string> gpu_names;
  gpu_names = GPU_Properties();
  unsigned int maxthreadsperblock = 1024;
  if (gpu_names[0] == "NVIDIA Tegra X2"){
	maxthreadsperblock = 512;  //for some reason 1024 fails
  }
  cout << "Detected "<<gpu_names[0]<<", using max of "<<maxthreadsperblock<<" threads per block"<<endl;
  if (folder_select=="") {
	cerr <<"no folder selected, exiting!"<<endl;
	exit(-1);
  }
  // Print out the CSV header
  if (verbose) {
	cout << "folder: "<<folder_select<<" NTT size: "<<n_selected<<endl;
  }

  // Read the folder
  DIR* dirp = opendir(folder_select.c_str());
  struct dirent *epdf;

  // Scan over all files in the folder
  while ((epdf = readdir(dirp)) != NULL) {
    size_t len = strlen(epdf->d_name);
    DEBUG("examining "<<epdf->d_name);
    // Pick only .dat files
    if (strcmp(epdf->d_name,".") != 0 && strcmp(epdf->d_name,"..") != 0
        && strcmp(&epdf->d_name[len-3], "dat") == 0) {

	  //look for the coefficient file.
	  string input_name("");
	  input_name = "coef";

	  if (strstr(epdf->d_name, input_name.c_str()) != NULL) {
		//this is a valid coefficient  file, we will run CRT/ICRT  on this data
		stringstream fname(epdf->d_name);
		DEBUG("parsing "<<fname.str());

		string nstr;
		DEBUG(nstr);
		unsigned int n;

		n = atoi(&epdf->d_name[4]); //parse string after "coef" to get size n
		DEBUG("n= "<<n);
		if(n != n_selected) {
		  continue; //skip to next file
		}
		// Read the 
		DEBUG(" running ");
  
		// Read the fwd_input and twiddle file
		vector<uint64_t> fwd_buffer; //storage for fwd_input
		vector<uint64_t> inv_buffer; //storage for fwd_input
		vector<uint64_t> twiddle; //storage for twiddle
		vector<uint64_t> itwiddle; //storage for twiddle
		uint64_t* result; //storage for output

		size_t data_size = n * sizeof(uint64_t);
		result = (uint64_t*)malloc(data_size);

		//build fwd_input file name from directory name and current file name
		string fwd_in_fname = string(folder_select) +"/"+ fname.str();

		
		uint64_t modulus(0);
		uint64_t inverse(0);
		if (verbose) {
		  cout<<"fwd_input file: "<<fwd_in_fname<<" tower: "<<tower_select<<endl;
		}
		//read in the correct tower and modulus
		read_input_file(fwd_in_fname.c_str(), fwd_buffer, tower_select, n, &modulus);
		//make the inverse input file name (swap "coef" with "eval"
		string inv_in_fname = fwd_in_fname;
		inv_in_fname.replace(inv_in_fname.find(input_name), input_name.length(),"eval");

		if (verbose) {
		  cout<<"inv_input file: "<<inv_in_fname<<" tower: "<<tower_select<<endl;
		}
		
		uint64_t test_modulus(0);
		//read in the correct tower and modulus
		read_input_file(inv_in_fname.c_str(), inv_buffer, tower_select, n, &test_modulus);
		if (modulus != test_modulus) {
		  cerr <<"cannot find eval with correct modulus!"<<endl;
		  cerr <<"need "<< modulus << "found "<< test_modulus<<endl;
		  exit(-1);
		}

		//build twiddle file name from directory name and current file name
		string twiddlefname("");
		string itwiddlefname("");
		
		itwiddlefname = string(folder_select) +"/irou"+to_string(n)+"-"
		  +to_string(modulus)+".dat";

		twiddlefname = string(folder_select) +"/frou"+to_string(n)+"-"
		  +to_string(modulus)+".dat";

		if (verbose) {
		  cout<<"twiddle: "<<twiddlefname<<endl;
		  cout<<"inverse twiddle: "<<itwiddlefname<<endl;
		}
		read_twiddle_ans_file(twiddlefname.c_str(), twiddle, n, modulus);
		read_twiddle_ans_file(itwiddlefname.c_str(), itwiddle, n, modulus);
		
		string inversefname("");

		//look for the inverse file.
		inversefname = string(folder_select) +"/inv"+to_string(n)+"-"
		  +to_string(modulus)+".dat";	
		DEBUGEXP(inversefname);
		if (verbose) {
		  cout<<"inverse file name: "<<inversefname<<endl;
		}
		read_inverse_file(inversefname.c_str(), inverse, n, modulus);
		
		//look for the result in a coefficient or eval file.
		string fwd_resultfname("");
		string inv_resultfname("");

		inv_resultfname = string(folder_select) +"/coef"+to_string(n)+".dat";
		fwd_resultfname = string(folder_select) +"/eval"+to_string(n)+".dat";


		DEBUGEXP(fwd_resultfname);
		DEBUGEXP(inv_resultfname);
		if (verbose) {
		  cout<<"fwd result check: "<<fwd_resultfname<<endl;
		  cout<<"inv result check: "<<inv_resultfname<<endl;

		}
		bool fail_flag; 
		uint64_t time_usec;
		double avetime_usec;
		fastest_struct fastest;
		fastest.time = UINT64_MAX;


		for (uint64_t loop_count = 0; loop_count<2; loop_count++){
		  //loop through twice first time with inverse_flag false, then true
		  bool inverse_flag = (loop_count == 1UL); 
		  try { //big try block to catch bad kernel calls
			int maxthreads = min(n,maxthreadsperblock);
			int start_th;
			int end_th;
			if (n_threads_select == 0){
			  start_th = 0; //check this should be 1. 
			  end_th = maxthreads;
			}else{
			  start_th = n_threads_select;
			  end_th = n_threads_select;
			}
			GPUwisdom w; 
			CRTGPUmem g; //storage for gpu data

			for (int th = start_th; th <= end_th; th <<= 1) {
			  //for (int th = 4; th <= 4; th <<= 1) {
			  if (th == 0) th = 1;
			  
			  int start_bal;
			  int end_bal;
			  if (n_balance_select == 0){
				start_bal = 1;
				end_bal = n/2;
			  }else{
				start_bal = n_balance_select;
				end_bal = n_balance_select;
			  }
			  for (int bal = start_bal; bal <= end_bal; bal <<= 1) { //best so far
				//for (int bal = 2; bal <= n / 2; bal <<= 1) { //original
				//for (int bal = 4; bal <=  4; bal <<= 1) { //specific test
				w.balance = bal;
				w.threads = th;

				avetime_usec = 0.0;
				uint nvalid = 0;
				CRTsetup(twiddle.data(), itwiddle.data(),
						 n, modulus, inverse, g);

				for (int r = 0; r < n_repeat; r++) {
				  if (inverse_flag) {
					cout<<"ICRT: n:"<<n<< " tower:"<<tower_select
						<<" th:"<<th<<" bal:"<<bal<<" ";
					time_usec = compute_ICRT(inv_buffer, result, g, w);
					cout << "time_usec:" << time_usec;
					//todo move file io out of compare results. 
					fail_flag = compare_results(inv_resultfname.c_str(),
												result, tower_select, modulus, n,
												verbose);
				  }else{
					cout<<"CRT: n:"<<n<< " tower:"<<tower_select
						<<" th:"<<th<<" bal:"<<bal<<" ";
					time_usec = compute_CRT(fwd_buffer, result, g, w);

					cout << "time_usec:" << time_usec;
					fail_flag = compare_results(fwd_resultfname.c_str(),
												result, tower_select, modulus, n,
												verbose);
				  } //inverse flag
				  // compare the computed data			  

				  if (fail_flag){
					cout <<" NOT verified"<<endl;
				  } else {
					cout <<" verified"<<endl;
					nvalid++;
					avetime_usec += (double)time_usec;
				  } //fail_flag
				} //n_repeat;

				CRTteardown(g);
			  
				avetime_usec /= (double)nvalid;
				cout <<"*** "<<(inverse_flag?"ICRT":"CRT")
					 <<" Average time usec: "<<avetime_usec<<endl;
				if (fastest.time > avetime_usec) { //we have a new fastest configuration
				  fastest.time = avetime_usec;
				  fastest.n = n;
				  fastest.th = th;
				  fastest.bal = bal;
				}
			  } //balance
			}//thread
			
		  } catch (const char* msg) {
			cout << msg<<endl;
		  }
		  cout<<"### "<<(inverse_flag?"ICRT":"CRT")
			  <<" Fastest averaged time usec:"<<fastest.time<<" n:"<<fastest.n
			  <<" th:"<<fastest.th<<" bal:"<<fastest.bal<<endl;
		} // loop_count
	  } // look for coef file
    } // pick only *.dat files
  } //while scan over all files in folder
  closedir(dirp);
  
  return 0;
}

#endif //has_cuda
