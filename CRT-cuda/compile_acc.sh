#!/bin/sh

echo "Compiling src/fft-acc.cu"
g++ -fopenacc -O2 src/fft-acc.cpp -std=c++11 -o bin/fft-acc

echo "Compiled."
