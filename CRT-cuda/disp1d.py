#!/usr/bin/env python3
#Script to plot outputs of doruns_gpu
# call like this:
#   ./disp.py foo
# to plot foo and fastfoo

from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
import numpy as np
import sys
import csv

#================================
#function to read in the values from a time file
def readTimeFiles(filename):
    n = []
    inverse_flag = []
    ave_time_usec = []
    sd_time_usec = []
    th = []
    bal = []
    nvalid = []
    colnames = []
    
    with open(filename) as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        first_row = True
        for row in csvReader:
            if (first_row):
                colnames = row
                first_row = False
            else:
                n.append(int(row[0]))
                inverse_flag.append(bool(int(row[1])))
                ave_time_usec.append(float(row[2]))
                sd_time_usec.append(float(row[3]))
                th.append(int(row[4]))
                bal.append(int(row[5]))
                nvalid.append(int(row[6]))

    return     colnames, n, inverse_flag, ave_time_usec, sd_time_usec, th, bal, nvalid
#================================

if len(sys.argv) < 2:
    print('Wrong count of arguments.')
    sys.exit()

timedatafile = sys.argv[1]
fastestdatafile = "fast"+timedatafile
surfplot = False

#load in the runtimes from the file
colnames, n, inverse_flag, ave_time_usec, sd_time_usec, th, bal, nvalid = readTimeFiles(timedatafile)
#load in the fastest run time
fcolnames, fn, finverse_flag, fave_time_usec, fsd_time_usec, fth, fbal, fnvalid = readTimeFiles(fastestdatafile)

#select some data for plots

#compute the max dimensions (th vs bal)
tind = np.unique(th); #gather all th values
nt = tind.shape[0]    #determine dimension
bind = np.unique(bal) #do same for bal values
nb = bind.shape[0]

if (nb != 1) :
    print('there is more than  one balance entry, use disp2d.py to display data')
    

time = np.zeros((nt, 1)) #construct array of times
    
for i in np.arange(len(n)):
    type(inverse_flag[i])
    if not inverse_flag[i]: #this is a fwd plot
        itup = np.nonzero(tind==th[i]) #find approprate indicies
        ix = itup[0][0] #converts to indicies. ugly
        time[ix] = ave_time_usec[i] #save this value

#convert the indicies to log2
tind = np.log2(tind)

#and data to log10
time = np.log10(time)

#make a 1D plotting axis
tplot = tind

# this is True for surface plot
surfplot= False

#with plt.xkcd():

ix = 0 #forward fastest plot

fig,ax = plt.subplots()
ax.plot(tind, time)

#plot a white + at the location of the fastest run
ftix = np.log2(fth[ix]) #get the indicies

#ax.plot([ftix], [fbix], '+', color='white')

# label plot
fig.suptitle('Log10 run time for n = '+str(n[ix]))
ax.set_title('Fastest Run Time th:'+str(fth[ix])+' bal:'+str(fbal[ix])+
             " is "+str(fave_time_usec[ix]) +'\u00b1'+ '{:.0f}'.format(fsd_time_usec[ix]) +' uS' )
ax.set_xlabel('log2 # threads')
ax.set_ylabel('log 10 runtime (mSec)')

plt.show()
    
